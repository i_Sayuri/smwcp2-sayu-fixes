@echo off

cp SMWCP2.smc SMWCP2_!5.smc
asar asm/objectool04.asm SMWCP2.smc
asar asm/MultiMidwayPoints.asm SMWCP2.smc
asar asm/clear.asm SMWCP2.smc
asar asm/sram_plus.asm SMWCP2.smc
asar asm/asar_patch.asm SMWCP2.smc
asar asm/BlimpDuckpatch.asm SMWCP2.smc
asar asm/DisableFireCape.asm SMWCP2.smc
asar asm/kill_old.asm SMWCP2.smc
asar asm/lakitu_limit.asm SMWCP2.smc
asar asm/DKC2_bar.asm SMWCP2.smc
asar asm/Fixall.asm SMWCP2.smc
asar asm/genericuploader.asm SMWCP2.smc
asar asm/gravity.asm SMWCP2.smc
asar asm/HammerBroNMSTLFix.asm SMWCP2.smc
asar asm/HDMAfix.asm SMWCP2.smc
asar asm/kill_irq.asm SMWCP2.smc
asar asm/LakituFixSpecial.asm SMWCP2.smc
REM asar asm/Layer3ExGFX.asm SMWCP2.smc
asar asm/lineguideactfix.asm SMWCP2.smc
asar asm/MarioGFXDMA.asm SMWCP2.smc
asar asm/mode7patch.asm SMWCP2.smc
REM asar asm/NMSTL.asm SMWCP2.smc
asar asm/nnmstl.asm SMWCP2.smc
asar asm/NoteWallFix.asm SMWCP2.smc
REM asar asm/NoVineLives.asm SMWCP2.smc
asar asm/objectClipping.asm SMWCP2.smc
asar asm/owborder.asm SMWCP2.smc
asar asm/owcounter.asm SMWCP2.smc
asar asm/OWMusic.asm SMWCP2.smc
asar asm/scrollreset.asm SMWCP2.smc
asar asm/sfx_echo.asm SMWCP2.smc
asar asm/SMB3Powerdown.asm SMWCP2.smc
asar asm/Windowing.asm SMWCP2.smc
asar asm/spritemessagebox.asm SMWCP2.smc
asar asm/REMAP.asm SMWCP2.smc
asar asm/jukebox_nmi.asm SMWCP2.smc
asar asm/messages.asm SMWCP2.smc

asar asm/tweaker.asm SMWCP2.smc
asar asm/Yoshi.asm SMWCP2.smc
asar asm/autosave.asm SMWCP2.smc
asar asm/PresentsScreen.asm SMWCP2.smc
asar amk/deathfix.asm SMWCP2.smc
asar asm/IRQHack.asm SMWCP2.smc
asar asm/title.asm SMWCP2.smc
asar asm/capeclimb.asm SMWCP2.smc
asar asm/thwomp_fix.asm SMWCP2.smc
asar asm/offscreen_walk.asm SMWCP2.smc
asar asm/bounceunrestrictor.asm SMWCP2.smc

@pause
