HEADER
LOROM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; HEY LIGHTVAYNE,
; Some of the checkpoint entrances are set to water levels with secondary entrances. I mentioned 
; in the tables below which levels will need 'em, so to make sure that those respective levels are
; working properly, add INC $85 into those respective sublevels through levelINIT.asm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Defines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; coin-stuff
incsrc CoinDefines.asm
;;


	!RAM_MIDWAY	= !CheckpointRAM	; holds which midway point to use (96 bytes, one for each level)


ORG $05D8B7					;was $05D8BC. Changed per Alcaro
	autoclean JML MAIN		;Point to new routine
;	BRA $01 : NOP			;Removed per Alcaro

ORG $05D9DE
	autoclean JML SECONDARIES2	; additional code for secondary exits

ORG $048F74
	autoclean JML RESET_MIDWAY	; hijack the code that resets the midway point

org $00A195			; Added per Alcaro V
	LDX #$5F
	LDA #$00
	-
	STA !RAM_MIDWAY,x
	DEX
	BPL -
	RTS 			; ^ to here
;ORG $00A19A					;Removed Per Alcaro
;	autoclean JML MIDWAY_INIT		; hijack the OW initialization to reset the table

ORG $05DAA3
	autoclean JML NO_YOSHI		; make secondary exits compatible with no yoshi intros


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;New main code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


freecode



!MIDWAY_NUMBER	= $0003	; max number of midway points per level
			; using more vastly increases the size of the table
			; if you changed it to, for example, 0003
			; the tables for EVERY level would look similar to this:
			; dw $0001 : dw $0001 : dw $0001
			; the maximum of midway points would be 00FF (256 decimal)


LEVEL_TABLES:
;levels 0-F
dw $0000 : dw $0000 : dw $0000				; 
dw $0001 : dw $0001 : dw $0000				; place the level number which you want to use here
dw $0002 : dw $0002 : dw $0000				; 
dw $0003 : dw $0003 : dw $0000				; IE: $0140 will use level 140's midway entrance
dw $0004 : dw $0004 : dw $0000				; $00FF will use level 0FF's midway entrance
dw $0005 : dw $0005 : dw $0000				; $0100 will use level 100's
dw $0006 : dw $0006 : dw $0000				; ect
dw $0007 : dw $0007 : dw $0000				; setting the highest nybble (making it 1000+ )
dw $0008 : dw $0008 : dw $0000
dw $005C : dw $101B : dw $101C	; Reclaimed Refinery
dw $000A : dw $000A : dw $0000				; will use a secondary entrance instead
dw $006A : dw $1190 : dw $006D	; Blazing Brush		; IE: $1003 will use secondary entrance 003
dw $000C : dw $000C : dw $0000				; $1138 will use secondary entrance 138
dw $000D : dw $000D : dw $0000				; $1000 will use secondary entrance 000
dw $000E : dw $000E : dw $0000				; ect
dw $000F : dw $000F : dw $0000
;levels 10-1F
dw $0010 : dw $0010 : dw $0000
dw $0047 : dw $0048 : dw $0000	; Briar Mire
dw $0012 : dw $0012 : dw $0000
dw $0013 : dw $0013 : dw $0000
dw $1014 : dw $0014 : dw $0000	; Onsen Overhang
dw $0015 : dw $0015 : dw $0000
dw $0016 : dw $0016 : dw $0000
dw $1191 : dw $1192 : dw $0000	; Shenmi Frica-Sea (Level EA's water level setting 
								; needs to be enabled through LevelASM.)
dw $0018 : dw $00F1 : dw $1193	; Tourou Temple
dw $0019 : dw $009B : dw $0000	; Tropical Turnout
dw $1194 : dw $1195 : dw $0000	; Beach Ball Brawl
dw $0058 : dw $001B : dw $0000	; Hydrostatic Halt
dw $001C : dw $001C : dw $0000
dw $1082 : dw $001D : dw $0000	; Crocodile Crossing
dw $1035 : dw $001E : dw $0000	; Brutal Beach Bash
dw $00EF : dw $1196 : dw $0000	; Stone Sea Sanctuary (Level EE's water level setting
				; needs to be enabled through LevelASM.)
;levels 20-24
dw $0020 : dw $0039 : dw $0000	; Sunset Split
dw $0021 : dw $0021 : dw $0000
dw $014D : dw $0022 : dw $0000	; Scorching Sepulcher
dw $0023 : dw $0023 : dw $0000
dw $0024 : dw $0024 : dw $0000
;levels 101-10F
dw $0089 : dw $0101 : dw $0000 	; Crumbling Catacombs
dw $01A2 : dw $01A8 : dw $019E 	; Digsite Dangers
dw $0103 : dw $0146 : dw $0000 	; Opulent Oasis
dw $0104 : dw $0174 : dw $0000	; Jewel Jubilation
dw $0060 : dw $0105 : dw $0000 	; Ice-Floe Inferno
dw $0106 : dw $0106 : dw $0000 
dw $0107 : dw $0107 : dw $0000 
dw $0108 : dw $0108 : dw $0000 
dw $0109 : dw $0109 : dw $0000 
dw $010A : dw $010A : dw $0000 
dw $010B : dw $010B : dw $0000 
dw $010C : dw $010C : dw $0000 
dw $010D : dw $010D : dw $0000 
dw $010E : dw $010E : dw $0000 	; Barathrum Baritis
dw $00AA : dw $010F : dw $0000 	; Terrifying Timbers
;levels 110-11F
dw $0090 : dw $0110 : dw $0000	; Carnival Caper
dw $0111 : dw $0111 : dw $0000
dw $00FF : dw $00FC : dw $0000	; Coaster Commotion
dw $11E7 : dw $0113 : dw $0000	; Balloon Bonanza
dw $10C6 : dw $0114 : dw $0000	; Playground Bound
dw $017C : dw $017D : dw $11A0	; Fantastic Elastic (Needs Secondary for Checkpoint #3)
dw $0095 : dw $0116 : dw $0000	; Tropopause Trail
dw $00C9 : dw $1197 : dw $0000	; Dynamic Disarray
dw $0031 : dw $0118 : dw $0000	; Aerotastic Assault
dw $1198 : dw $0119 : dw $0000	; Radiatus Ruins (Needs Secondary for Checkpoint #1)
dw $011A : dw $011A : dw $0000
dw $0179 : dw $011B : dw $0000	; Cumulus Chapel
dw $011C : dw $011C : dw $0000
dw $1017 : dw $011D : dw $0000	; Dangling Danger
dw $1199 : dw $011E : dw $0167	; Gliding Garrison
dw $011F : dw $0189 : dw $0000	; Stratus Skyworks
;levels 120-12F
dw $0120 : dw $0120 : dw $0000
dw $0164 : dw $0165 : dw $0167	; Stormy Stronghold
dw $0122 : dw $0122 : dw $0000
dw $10A6 : dw $0123 : dw $0000	; Crystalline Citadel
dw $0124 : dw $0124 : dw $0000
dw $0125 : dw $002A : dw $0000	; Frost-Fried Frenzy (Palette 4 Shenanigans)
dw $01E6 : dw $0126 : dw $0000	; Frostflow Freezer (Level 2E crashes on entry, cannot fully test.)
dw $0127 : dw $0127 : dw $0000
dw $1159 : dw $0128 : dw $0000	; Smoldering Shrine
dw $00F8 : dw $0129 : dw $0000	; Chilly Colors
dw $112A : dw $112C : dw $0098	; Pyro-Cryo Castle
dw $012B : dw $01DD : dw $0000	; Rusted Retribution
dw $012C : dw $012C : dw $0000
dw $012D : dw $012D : dw $0000	; Petroleum Passage (Did not touch because of length/complex reasons.)
dw $012E : dw $012E : dw $0000	; Musical Mechanisms (Level is currently broken, cannot test.)
dw $005F : dw $012F : dw $0000	; Synchronized Sector
;levels 130-13B
dw $0162 : dw $0130 : dw $0000	; Myrrky Mainline
dw $0131 : dw $0184 : dw $0185	; Perilous Paths
dw $0132 : dw $119C : dw $0000	; Nexus Norvegicus (Didn't touch Checkpoint #3 because incomplete.)
dw $1009 : dw $0133 : dw $0000	; Reverse Universe
dw $0134 : dw $0134 : dw $0000
dw $0135 : dw $0135 : dw $0000
dw $119D : dw $119E : dw $119F	; EREHPS EHT RAEF (May need secondaries instead.)
dw $0137 : dw $0137 : dw $0000
dw $0138 : dw $0138 : dw $0000
dw $0139 : dw $0139 : dw $0000	; Bedazzling Bastion (Needs level 1B1 reinserted.)
dw $0153 : dw $0150 : dw $0000	; Asteroid Antics
dw $013B : dw $013B : dw $0000	; Finale
;--------
;;;; Code used by smwc-coins
;--------

Coincode:
PHA
PHY
PHX
PHP
SEP #$30
LDX $13BF
LDA $1EA2,x
BIT #$40
BNE Coincheck
BIT #$80
BEQ +
	jmp  EndCoins
+
LDA $13BF
CMP #$01
BEQ Coincheck
JSL Reset_RAM
LDA #$00
STA.l !TempCounter
STA.l !TempCounter+1
STA.l !TempCounter+2
STA.l !TempCounter+4

Coincheck:
LDA.l !TempCounter+1
LSR #3
STA.l !TempCounter+3
LDA.l !TempCounter+1
AND #$38
ORA.l !TempCounter+3
STA.l !TempCounter+1

LDA $13BF
AND #$07
TAX
LDA.l bitTable,x
STA.l !TempCounter+3

LDA $13BF
LSR
LSR
LSR
TAX

LDA !ramspace,x
AND.l !TempCounter+3
BEQ Sec
LDA.l !TempCounter+1
ORA #$01
STA.l !TempCounter+1

LDA.l !TempCounter+2
ORA #$01
STA.l !TempCounter+2

Sec:
LDA.l !ramspace+$0B,x
AND.l !TempCounter+3
BEQ Thi

LDA.l !TempCounter+1
ORA #$02
STA.l !TempCounter+1

LDA.l !TempCounter+2
ORA #$02
STA.l !TempCounter+2

Thi:
LDA.l !ramspace+$16,x
AND !TempCounter+3
BEQ EndCoins

LDA.l !TempCounter+1
ORA #$04
STA.l !TempCounter+1

LDA.l !TempCounter+2
ORA #$04
STA.l !TempCounter+2

BRA EndCoins

;Was backwards
bitTable:
db $80,$40,$20,$10,$08,$04,$02,$01
;db $01, $02, $04, $08, $10, $20, $40, $80
EndCoins:
PLP
PLX
PLY
PLA
RTL
;------
;;;;
;------


MAIN:
	REP #$30	; 16-bit A X Y
	LDA $0E		; (this is stolen from levelasm)
	STA $010B	;
	PHY		; push y
	SEP #$30	; 8 bit A X Y
	LDA $141A	; skip if not the opening level
	BNE RETURN
	LDA $1B93	; prevent infinite loop if using a secondary exit
	BNE RETURN
	;;
	JSL Coincode ; run the coins' code
	;;
	LDY $13BF	; get translevel number
	LDA $1EA2,y	; get level settings
	AND #$40	; check midway point flag
	BEQ RETURN	; return if not set
	PHB		;	
	PHK		; wrapper
	PLB		;
	TYA		; stick translevel number in A
	REP #$30	; 16 bit A X Y
	AND #$00FF	; clear high byte of A
	ASL		; x 2
	STA $0E		; store to scratch
	PHX		; push x
	TYX		; stick y in x temporarily
	LDA !RAM_MIDWAY,x	;\ get midway point number to use
	AND #$00FF		; |
	ASL			;/
	PLX		; pull x
	LDY #$0000	; 
	CLC
LOOP:
	ADC $0E		; multiply it by the number of midway points in use per level
	INY
	CPY #!MIDWAY_NUMBER
	BCC LOOP
	TAY		; stick in y
	LDA LEVEL_TABLES,y	; get new level number
	CMP #$1000		; check secondary exit flag
	BCS SECONDARIES		; use secondary exit
	AND #$01FF		; failsafe
	STA $0E			; store level number
	STA $010B
	SEP #$30	; 8 bit A X Y
	PLB		; get bank back
RETURN:
	REP #$30	; 16 bit A X Y
	PLY		; pull y
	LDA $0E
	JML $85D8BC	; return. Was 05D8C3. changed again from $85D8BB because uberasm

SECONDARIES:
	AND #$0FFF	; clear secondary exit flag here
	ORA #$0600	; these bits have to be set
	SEP #$30	; 8 bit A X Y
	STA $19B8	; store secondary exit number (low)
	XBA		; flip high and low byte of A
	STA $19D8	; store secondary exit number (high and properties)
	STZ $95		; set these to 0
	STZ $97
	PLB		; get bank back
	REP #$30	; 16 bit A X Y
	PLY		; pull y back
	SEP #$30	; 8 bit A X Y ; Removed per Alcaro ; Readded because COPs
	JML $85D7B3	; return and load level at a secondary exit position

SECONDARIES2:
	LDX $1B93	; check if using a secondary exit for this
	BNE RETURN2	; if so, skip the code that sets mario's x position to the midway entrance
	STA $13CF	; restore old code
	LDA $02		; restore old code
	JML $85D9E3	; return

RETURN2:
	JML $85D9EC	; return without setting mario's x to the midway entrance

RESET_MIDWAY:
	STA $1EA2,x		; restore old code
	INC $13D9		;
	LDA !RAM_MIDWAY,x
	AND #$FF00		; reset midway point number too
	STA !RAM_MIDWAY,x
	JML $848F7A		; return


	
;MIDWAY_INIT:
;	LDA #$00			;Removes lines below
;	STA !RAM_MIDWAY,x	;and added this EDIT: Removed this...
;	JML $80A1A3			;per Alcao
	
;	LDA $1F49,x		; restore old code
;	STA $1EA2,x		;
;	LDA #$00		; reset this table
;	STA !RAM_MIDWAY,x
;	JML $80A1A0		; return

Reset_RAM:
	LDX #97
	LDA #$00
	resetloop:
		STA.l !CheckpointRAM-1,x
		DEX
		BNE resetloop
	LDX #97
	midwayloop:
		LDA $1EA2-1,x
		AND #$BF
		STA $1EA2-1,x
		DEX
		BNE midwayloop
	RTL 


NO_YOSHI:
	STZ $1B93	; reset this (prevents glitch with no yoshi intros and secondary entrances)
	LDA $05D78A,x	; restore old code
	JML $85DAA7	; return



MainEnd:

print freespaceuse