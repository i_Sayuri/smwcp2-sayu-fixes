import re

def addresstostring(address, endianness = "little"):
    return "".join([hex(ord(x))[2:] if len(hex(ord(x))[2:]) == 2 else "0"+hex(ord(x))[2:] for x in (address[::-1] if endianness == "little" else address)])

def get_bits(value, size, offset):
    mask = 0b1
    for i in xrange(1, size):
        mask <<= 1
        mask += 1
    return (value&(mask<<offset))>>offset

def read_bytes(data, address, count, little_endian=True):
    out = data[address:address+count]
    if little_endian:
        out = out[::-1]
    out = map(ord, out)
    shifter = 0
    out_num = 0
    for n in out[::-1]:
        out_num += n<<shifter
        shifter += 8
    return out_num

def LoRomtoPC(address,header=False):
    return (((address & 0x7f0000) >> 1) | (address & 0x7fff)) + header*0x200

def HiRomtoPC(address,header=False):
    return (address & 0x3fffff) + header*0x200

def PCtoLoRom(address,header=False):
    address = address-(header*0x200)
    return ((address & 0x7F8000) << 1) + 0x8000 + (address & 0x7FFF)

def PCtoHiRom(address,header=False):
    return 0xC00000 + ((address - header*0x200)&0x3FFFFF)

class FreeSpace(object):
    def __init__(self,start,length):
        self.start = start
        self.LoRom = PCtoLoRom(start)
        self.HiRom = PCtoHiRom(start)
        self.length = length
    def UpdateAddress(self,length):
        self.length -= length
        self.start += length
        self.LoRom = PCtoLoRom(self.start)
        self.HiRom = PCtoHiRom(self.start)
    def __str__(self):
        return "<FreeSpace object, start= {0} , length= {1} >".format(hex(self.start),hex(self.length))

def FindFreespace(data,size=1,zeros=False):
    matches = re.finditer("STAR(....)",data,re.S)
    count, RATs, addresses, lastRAT = 0,[],[],(0,0)
    for match in matches:
        length = int(addresstostring(match.groups()[0][0:2]),16)
        if length^0xFFFF == int(addresstostring(match.groups()[0][2:]),16) and \
        match.start() + 8 + length >= lastRAT[1]:
            RATs.append((match.start(),match.start()+8+length))
            lastRAT = (match.start(),match.start()+8+length)
    if zeros:
        for RAT in zip([(0,0)]+RATs,RATs+[(len(data),len(data))]):
            if RAT[0][1]+1 < RAT[1][0]:
                matches=re.finditer("".join(["\x00" for x in xrange(size)])+"\x00*",data[RAT[0][1]+1:RAT[1][0]])
                for match in matches:
                    freespace = FreeSpace(RAT[0][1]+1+match.start(),match.end()-match.start())
                    addresses.append(freespace)               
    else:
        for RAT in zip([(0,0)]+RATs,RATs+[(len(data),len(data))]):
            if RAT[0][1]+1 < RAT[1][0]:
                freespace = FreeSpace(RAT[0][1]+1, RAT[1][0]-RAT[0][1])
                if freespace.length >= size:  
                    addresses.append(freespace)
    return addresses

def FindFreespaceByBanks(data,size=1,zeros=False):
    freespaces = FindFreespace(data,size,zeros)
    bankfreespaces = [[] for x in xrange(len(data)//0x8000)]
    for freespace in freespaces:
        banknum = freespace.start//0x8000
        posinbank = freespace.start%0x8000
        
        if posinbank + freespace.length > 0x8000:
            tempfreespaces = []
            length = freespace.length
            newfreespace = FreeSpace(freespace.start,0x8000-posinbank)
            bankfreespaces[banknum].append(newfreespace)
            tempfreespaces.append(newfreespace)
            length -= 0x8000-posinbank
            banknum += 1
            while length > 0x8000:
                newfreespace = FreeSpace(0x8000*banknum,0x8000)
                bankfreespaces[banknum].append(newfreespace)
                tempfreespaces.append(newfreespace)
                length -= 0x8000
                banknum += 1
            
            newfreespace = FreeSpace(0x8000*banknum,length)
            bankfreespaces[banknum].append(newfreespace)
            tempfreespaces.append(newfreespace)

            
        else:
            bankfreespaces[banknum].append(freespace)
    return bankfreespaces        

def GetFirstFreespace(banks,length, update = True):
    for freespaces in banks:
        for freespace in freespaces:
            if freespace.length >= length:
                tmpfreespace = FreeSpace(freespace.start,freespace.length)
                if update:
                    freespace.UpdateAddress(length)
                return tmpfreespace
    return None

