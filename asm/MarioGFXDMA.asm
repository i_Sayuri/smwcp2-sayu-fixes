header
lorom
!VRAMLoc = $60C0	;Ignore the first and last number. The middle two numbers are the
			;tile number where you want the graphics loaded to in SP1. Must be 2
			;tiles in a row (Unless you like stuff being overwritten)

!Tile1 = $0C		;Tile where first extended tile is loaded to. Must be same as
			;middle two numbers in !VRAMLoc

!Tile2 = $0D		;Tile where second extended tile is loaded to. Must be directly
			;after !Tile1

org $00A300
autoclean JML BEGINDMA

!i = !

!VRAMLoc = $6000|(!Tile1<<4)
assert !Tile1 >= 0, "!{i}Tile1 has an invalid value"
assert !Tile1 < 256, "!{i}Tile1 has an invalid value"
assert !Tile2 == !Tile1+1, "!{i}Tile2 has an invalid value"


freedata ; this one doesn't change the data bank register, so it uses the RAM mirrors from another bank, so I might as well toss it into banks 40+
BEGINDMA:
REP #$20
LDX #$04
LDY $0D84
BEQ CODE_00A328

;;
;Mario's Palette
;;

LDY #$86
STY $2121
LDA #$2200
STA $4320
LDA $0D82
STA $4322
LDY #$00
STY $4324
LDA #$0014
STA $4325
STX $420B

CODE_00A328:

LDY #$80
STY $2115
LDA #$1801
STA $4320

SEP #$20
LDA $0100
CMP #$14
BEQ ContinUpload
CMP #$07
BEQ ContinUpload
JMP StopUpload

ContinUpload:
REP #$20

;;
;Random Cape Tile
;;

LDA #$67F0
STA $2116
LDA $0D99
STA $4322
LDY #$7E
STY $4324
LDA #$0020
STA $4325
STX $420B

;;
;Mario's 8x8 tiles
;;

LDA #!VRAMLoc
STA $2116

SEP #$20
LDA $13E0
ASL
TAX
REP #$20

PHX
LDX $19
STX $4202
LDX #$02
STX $4203
NOP #4
LDX $4216
STX $00
PLX
TXA
LDX $00
CLC
ADC.l DMAGFXtable,x
REP #$10
TAX
LDA.w #BEGINXTND
CLC
ADC.l DMAGFXbigtables,x
SEP #$10
STA $4322
LDY.b #BEGINXTND>>16
STY $4324
LDA #$0040
STA $4325
LDY #$04
STY $420B
JMP SkipMiddle

StopUpload:
REP #$20

;;
;Mario's top tiles
;;

SkipMiddle:
LDA #$6000
STA $2116
LDX #$00
.loop
LDA $0D85,x
STA $4322
LDY #$7E
STY $4324
LDA #$0040
STA $4325
LDY #$04
STY $420B
INX #2
CPX $0D84
BCC .loop

;;
;Mario's bottom tiles
;;

LDA #$6100
STA $2116
LDX #$00
.loop2
LDA $0D8F,x
STA $4322
LDY #$7E
STY $4324
LDA #$0040
STA $4325
LDY #$04
STY $420B
INX #2
CPX $0D84
BCC .loop2
SEP #$20

JML $80A38F


DMAGFXtable:			;Don't mess with this unless you know what you're doing
dw $0000,$008C,$0118,$008C

DMAGFXbigtables:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Basically, each tile takes up 20 bytes. Since we are uploading 2 tiles, it's 40 bytes
;This table shows which frames use which tiles (The tiles are in ExtendGFX.bin)

;Small Mario

dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;1
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;2
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;3
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;4
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;5
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;6
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;7
dw $0000,$0000,$0000,$0000,$0000			;8
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;9
dw $0000						;10

;Big/Fire Mario:

dw $0000,$0000,$0000,$0000,$0040,$0040,$0040,$0000	;1
dw $0000,$0000,$0000,$0000,$0080,$0000,$0000,$0000	;2
dw $0000,$00C0,$00C0,$00C0,$0000,$0000,$0100,$0100	;3
dw $0140,$0140,$0180,$0180,$0000,$0000,$0200,$0000	;4
dw $0000,$0000,$0000,$01C0,$0000,$0000,$0000,$0000	;5
dw $0240,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;6
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;7
dw $0000,$0000,$0000,$0000,$0000			;8
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;9
dw $0000						;10

;Cape Mario:

dw $0000,$0000,$0000,$0000,$0040,$0040,$0040,$0000	;1
dw $0000,$0000,$0000,$0000,$0080,$0000,$0000,$0000	;2
dw $0000,$00C0,$00C0,$00C0,$0000,$0000,$0100,$0100	;3
dw $0140,$0140,$0180,$0180,$0000,$0000,$0200,$0000	;4
dw $0000,$0000,$0000,$01C0,$0000,$0000,$0000,$0000	;5
dw $0240,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;6
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;7
dw $0000,$0000,$0000,$0000,$0000			;8
dw $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000	;9
dw $0000						;10

BEGINXTND:
incbin ExtendGFX.bin



org $00DF1A
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00

db $00,$00,$00,$00,$00,$00,$28,$00
db $00

db $00,$00,$00,$00,$04,$04,$04,$00
db $00,$00,$00,$00,$04,$00,$00,$00
db $00,$04,$04,$04,$00,$00,$04,$04
db $04,$04,$04,$04,$00,$00,$04,$00
db $00,$00,$00,$04,$00,$00,$00,$00
db $04,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00

db $00,$00,$00,$00,$04,$04,$04,$00
db $00,$00,$00,$00,$04,$00,$00,$00
db $00,$04,$04,$04,$00,$00,$04,$04
db $04,$04,$04,$04,$00,$00,$04,$00
db $00,$00,$00,$04,$00,$00,$00,$00
db $04,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00

org $00DFDA
db $00,$02,$80,$80		;[00-03]
db $00,$02,!Tile1,!Tile2	;[04-07]
db $00,$00,$00,$00		;[08-0B]
db $00,$00,$00,$00		;[0C-0F]
db $00,$00,$00,$00		;[10-13]
db $00,$00,$00,$00		;[14-17]
db $00,$00,$00,$00		;[18-1B]
db $00,$00,$00,$00		;[1C-1F]
db $00,$00,$00,$00		;[20-23]
db $00,$00,$00,$00		;[24-27]
db $00,$02,$02,$80		;[28-2B]
db $04				;[2C]
db $7F				;[2D]
db $4A,$5B,$4B,$5A		;[2E-31]