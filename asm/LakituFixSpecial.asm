; Depends on specially modified No More Sprite Tile Limits patch, which in turn depends on specially modified UberASM

		!SpriteTilesReservedEnabled	= 1
		!RAM_SpriteTilesReserved	= $18CA
		;!SpriteTilesReservedEnabledDyn	= 1	;must be enabled anyway for the patch to work.
		!RAM_SpriteTilesReservedDyn	= $18CB

org $01E8D2
autoclean	JML LakituCloudFix

org $02E6EC
		JML LakituPoleFix
		NOP

org $01E945
		BRA +
		NOP #3
+

org $019F74
	jml HeldItemFix
;LDA.w $13DD
;bne CODE_019F83
;org $019F83
;CODE_019F83:

freecode

; care about this again $7E18C2: Player is inside Lakitu cloud flag.


LakituCloudFix:
	LDA $1692
	CMP #$10
	BNE .CarryOn
	
	LDA.w !RAM_SpriteTilesReservedDyn
if !SpriteTilesReservedEnabled
	CLC : ADC.w !RAM_SpriteTilesReserved
endif
	ASL #2
	EOR.b #$FF
	CLC : ADC.b #$FC+1
	STA $0D
	SEC
	SBC.b #$04
	STA $0C
	STA $15EA,x
	INC.w !RAM_SpriteTilesReservedDyn
	INC.w !RAM_SpriteTilesReservedDyn
	LDA #$00
	JML $81E8E2


.CarryOn
	LDA #$F8
	STA $0C
	STA $15EA,x	;Added
	JML $81E8D6
	
LakituPoleFix:
	LDA $1692
	CMP #$10
	BNE .CarryOn
	LDA $15EA,x
	CLC : ADC.b #$10
	STA $15EA,x
	TAY
	JML $82E6F1
	
.CarryOn
	LDA.b #$38
	STA.w $15EA,x
	JML $82E6F1

HeldItemFix:

	LDA.w $13DD				;$019F74	|
	BNE .reserve				;$019F77	|
	LDA.w $1419				;$019F79	|
	BNE .reserve				;$019F7C	|
	LDA.w $1499				;$019F7E	|
	BEQ .CarryOn				;$019F81	|
	.reserve
	LDA $1692
	CMP #$10
	BNE .CarryOn
	lda.b $18C2
	bne .CarryOn
	lda $9E,x
	cmp.b #$2F
	bne +
	lda.b #$00
	sta $15EA,x
	bra .CarryOn
	+
	lda.b #$04
	sta $15EA,x

.CarryOn
	JML $819F86