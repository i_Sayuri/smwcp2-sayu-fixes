!barkerX = $E4,x
!barkerY = $D8,x
!barkerState = !actorVar3,x
!barkerTimer = !actorVar4,x


SetSpeakerToBarkerDuck:
	LDX !barkerIndex
	STX !currentSpeaker
	RTL

BarkerDuckCreate:
	REP #$20
	LDA.w #BarkerINIT
	STA $00
	LDX.b #BarkerINIT>>16
	STX $02
	
	LDA.w #BarkerMAIN
	STA $03
	LDX.b #BarkerMAIN>>16
	STX $05
	
	LDA.w #BarkerNMI
	STA $06
	LDX.b #BarkerNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !barkerIndex
	RTL

BarkerDelete:
	LDX !barkerIndex
	JSL DeleteActor
	RTL

BarkerINIT:
	LDA #$00
	STA !barkerSpinningCane
	STA !barkerTimer
	STA !barkerState
	
	JSL BarkerSetToNormal
	
	;LDA #$01
	;STA !barkerCaneState
	
	LDA #$A8
	STA !barkerX
	LDA #$30
	STA !barkerY
	
	LDA #$01
	STA !barkerDrawGoomba
	RTL
	
BarkerPointToMario:
	LDA #$A8 : STA !barkerTopLeftTile
	LDA #$AA : STA !barkerTopRightTile
	LDA #$AC : STA !barkerBottomLeftTile
	LDA #$A2 : STA !barkerBottomRightTile
	LDA #$48 : STA !barkerExtraCaneTile
	STZ !barkerSpinningCane
	;LDA #$01
	;STA !barkerArmState
	;LDA #$00
	;STA !barkerCaneState
	RTL
	
BarkerArmsUp:
	LDA #$40 : STA !barkerTopLeftTile
	LDA #$42 : STA !barkerTopRightTile
	LDA #$26 : STA !barkerBottomLeftTile
	LDA #$28 : STA !barkerBottomRightTile
	LDA #$00 : STA !barkerExtraCaneTile
	
	;LDA #$02
	;STA !barkerArmState
	;LDA #$03
	;STA !barkerCaneState
	RTL
	
BarkerSkywardShout:
	LDA #$84 : STA !barkerTopLeftTile
	LDA #$86 : STA !barkerTopRightTile
	LDA #$A4 : STA !barkerBottomLeftTile
	LDA #$A6 : STA !barkerBottomRightTile
	LDA #$00 : STA !barkerExtraCaneTile
	STZ !barkerSpinningCane
	RTL
	
;BarkerArmsDown:
;	STZ !barkerArmState
;	LDA #$01
;	STA !barkerCaneState
;	RTL
	
BarkerSpinCane:
	LDA #$01
	STA !barkerSpinningCane
	STZ !barkerExtraCaneTile
	RTL
	
;BarkerStopSpinningCane:
;	STZ !barkerSpinningCane
;	RTL
	
BarkerEraseGoomba:
	STZ !barkerDrawGoomba
	RTL
	
BarkerSetHeadFacingBack:
	LDA #$4B : STA !barkerTopLeftTile
	LDA #$2B : STA !barkerTopRightTile
	LDA #$A0 : STA !barkerBottomLeftTile
	LDA #$A2 : STA !barkerBottomRightTile
	LDA #$00 : STA !barkerExtraCaneTile
	;LDA #$01
	;STA !barkerHeadFacingBack
	RTL
	
BarkerSetRunningHeadFacingBack:
	LDA #$01
	STA !barkerLookBackRunning
	RTL

;BarkerSetHeadFacingNormal:
;	STZ !barkerHeadFacingBack
;	RTL
	
BarkerDrawScaredLuigi:
	LDA #$01
	STA !barkerLuigiDrawState
	RTL
	
BarkerDrawHangingLuigi:
	LDA #$02
	STA !barkerLuigiDrawState
	RTL
	
BarkerJumpToLuigi:
	LDX !barkerIndex
	LDA #$E8
	STA $B6,x
	LDA #$ED
	STA $AA,x
	LDA #$01
	STA !barkerState
	RTL
	
BarkerRunOffScreen:
	LDX !barkerIndex
	LDA #$02
	STA !barkerState
	;LDA #$03
	;STA !barkerArmState
	RTL
	
BarkerSetXPos:
	LDX !barkerIndex
	STA !barkerX
	RTL
	
;BarkerNormal:
;	LDX !barkerIndex
;	LDA #$00
;	STA !barkerState
;	STA !barkerTimer
;	RTL
	
;BarkerRunOff:
;	LDX !barkerIndex
;	LDA #$02
;	STA !barkerState
;	STA !barkerTimer
;	RTL

;BarkerRunRight:
;	LDX !barkerIndex
;	LDA #$08
;	STA $B6,x
;	RTL
	
;BarkerJump:	
;	LDX !barkerIndex
;	LDA #$E0
;	STA $AA,x
;	RTL
	

BarkerSetToNormal:
	LDA #$80 : STA !barkerTopLeftTile
	LDA #$82 : STA !barkerTopRightTile
	LDA #$A0 : STA !barkerBottomLeftTile
	LDA #$A2 : STA !barkerBottomRightTile
	LDA #$00 : STA !barkerExtraCaneTile
	STZ !barkerSpinningCane
	
	LDX !barkerIndex
	STZ $AA,x
	STZ $B6,x
	LDA #$00
	STA !barkerState
	RTL
	
BarkerSetToRunningInPlace:
	LDX !barkerIndex
	LDA #$03
	STA !barkerState
	STZ !barkerLookBackRunning
	RTL
	
BarkerStartScrolling:
	LDA #$01
	STA !barkerScrollScreen
	RTL

BarkerStopScrolling:
	STZ !barkerScrollScreen
	RTL

BarkerStateTable:
dw BarkerNormal, BarkerJumping, BarkerRunningOffScreen, BarkerRunningInPlace

BarkerMAIN:
	PHK
	PLB
	
	LDA !barkerScrollScreen
	BEQ +
	LDA $1A
	CLC : ADC #$06
	STA $1A
	
	LDA $1E
	CLC : ADC #$03
	STA $1E
+
	
	LDA #$00
	STA $09
	LDA !barkerState
	CMP #$02
	BEQ .bob
	CMP #$03
	BEQ .bob
	BRA .noBob
.bob
	LDA $13
	AND #$02
	BNE .noBob
	INC $09
.noBob
	
;	LDA !barkerSpinningCane
;	BEQ +
;	LDA $13
;	AND #$1C
;	LSR
;	LSR
;	STA !barkerCaneState
;	+
	
	PHX				; Jump to the Barker's current routine.
	LDA !barkerState		;
	ASL				;
	TAX				;
	LDA BarkerStateTable,x		;
	STA $00				;
	LDA BarkerStateTable+1,x	;
	STA $01				;
	PLX				;
	PEA.w .ret1-1			;
	JMP [$0000]			;
.ret1	
	
	RTL
	
BarkerNormal:
	JSR BarkerDraw
	RTS
	
BarkerJumping:
;	LDA #$01
;	STA !barkerFeetState
;	LDA $AA,x
;	BMI +
;	LDA #$02
;	STA !barkerFeetState
;+

	LDA #$01
	STA $15DC,x
	JSL $01802A			; Update sprite position if it's falling.
	
	LDA !barkerY
	CMP #$40
	BCC +
	JSL BarkerSetToNormal
	+
	
	
	JSR BarkerDraw
	

	RTS
	
BarkerRunningOffScreen:
	
	LDA !barkerX
	CMP #$10
	BCS +
	JSL BarkerSetToNormal
	+
	
	LDA !barkerX
	CLC
	ADC #$05
	STA !barkerX
	
BarkerRunningInPlace:
	;LDA #$03
	;STA !barkerFeetState
	
	LDA !barkerLookBackRunning
	BNE +
	LDA #$08 : STA !barkerTopLeftTile
	LDA #$0A : STA !barkerTopRightTile
	BRA ++
+
	LDA #$EA : STA !barkerTopLeftTile
	LDA #$EE : STA !barkerTopRightTile
++
	LDA $13 : AND #$02 : ASL : CLC : ADC #$68 : STA !barkerBottomLeftTile
	LDA $13 : AND #$02 : ASL : CLC : ADC #$6A : STA !barkerBottomRightTile
	
	LDA #$40
	STA !barkerXFlip
	JSR BarkerDraw
	
	RTS
	
BarkerGetLeftSpeechTileOffset:
	LDA !textIsAppearing
	BEQ .noSpeech
	
	LDA $13
	AND #$08
	BEQ .noSpeech

	LDA !barkerTopLeftTile
	CMP #$08 : BNE + : LDA #$04 : RTS
+	CMP #$40 : BNE + : LDA #$24 : RTS
+	CMP #$80 : BNE + : LDA #$08 : RTS
+	CMP #$84 : BNE + : LDA #$08 : RTS
+	CMP #$A8 : BNE + : LDA #$3E : RTS
+	CMP #$4B : BNE + : LDA #$97 : RTS
+	CMP #$C0 : BNE + : LDA #$02 : RTS
+	CMP #$C4 : BNE + : LDA #$02 : RTS
+	CMP #$C8 : BNE + : LDA #$02 : RTS
+	CMP #$CC : BNE + : LDA #$02 : RTS
	
+	
	
.noSpeech
	LDA #$00
	RTS
	
	
BarkerGetRightSpeechTileOffset:
	LDA !textIsAppearing
	BEQ .noSpeech
	
	LDA $13
	AND #$08
	BEQ .noSpeech

	LDA !barkerTopRightTile
	CMP #$0A : BNE + : LDA #$04 : RTS
+	CMP #$42 : BNE + : LDA #$24 : RTS
+	CMP #$82 : BNE + : LDA #$08 : RTS
+	CMP #$86 : BNE + : LDA #$08 : RTS
+	CMP #$AA : BNE + : LDA #$E0 : RTS
+	CMP #$2B : BNE + : LDA #$83 : RTS
+	CMP #$C2 : BNE + : LDA #$A4 : RTS
+	CMP #$C6 : BNE + : LDA #$A0 : RTS
+	CMP #$CA : BNE + : LDA #$9C : RTS
+	CMP #$CE : BNE + : LDA #$98 : RTS
	
+	
	
.noSpeech
	LDA #$00
	RTS

	
BarkerDraw:

	LDA !barkerSpinningCane
	BEQ +
	
	LDA $13 : AND #$0C : CLC : ADC #$C0 : STA !barkerTopLeftTile
	LDA #$82 : STA !barkerTopRightTile
	LDA $13 : AND #$0C : CLC : ADC #$E0 : STA !barkerBottomLeftTile
	LDA #$A2 : STA !barkerBottomRightTile
	+


	JSR BarkerDrawLuigi
	JSR BarkerDrawSelf	
	;JSR BarkerDrawArm	: BCS + : JSR BarkerGenericTileDraw : +
	;JSR BarkerDrawCane
	;JSR BarkerDrawFrontFoot	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawHead
	;JSR BarkerDrawBody	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawBackFoot	: BCC + : JSR BarkerGenericTileDraw : +
	
	JSR BarkerDrawGoomba

	
	;JSR BarkerDrawBackFoot	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawBody	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawHead	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawFrontFoot	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawArm	: JSR BarkerGenericTileDraw
	;JSR BarkerDrawCane
	;JSR BarkerDrawLuigi	: JSR BarkerGenericTileDraw	; Only used when running
	
	RTS
	
BarkerGenericTileDraw:
	PHA
	LDA #$3A
	ORA !barkerXFlip
	STA $03
	
	PLA
	JSL DrawTile
	RTS
	
BarkerDrawSelf:
	JSR BarkerGetLeftSpeechTileOffset
	STA $0C
	JSR BarkerGetRightSpeechTileOffset
	STA $0D
	
	LDA !barkerXFlip
	BEQ +
	JMP .barkerLeft
+
	
	
	LDA !barkerX : STA $00
	LDA !barkerY : CLC : ADC $09 : STA $01
	LDA !barkerTopLeftTile : CLC : ADC $0C : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : CLC : ADC #$10 : STA $00
	LDA !barkerY : CLC : ADC $09 : STA $01
	LDA !barkerTopRightTile : CLC : ADC $0D : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : STA $00
	LDA !barkerY : CLC : ADC $09 : CLC : ADC #$10 : STA $01
	LDA !barkerBottomLeftTile : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : CLC : ADC #$10 : STA $00
	LDA !barkerY : CLC : ADC $09 : CLC : ADC #$10 : STA $01
	LDA !barkerBottomRightTile : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : SEC : SBC #$10 : STA $00
	LDA !barkerY : CLC : ADC $09 : CLC : ADC #$10 : STA $01
	LDA !barkerExtraCaneTile : BEQ + : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
+
	RTS
	
.barkerLeft	
	LDA !barkerX : STA $00
	LDA !barkerY : CLC : ADC $09 : STA $01
	LDA !barkerTopRightTile : CLC : ADC $0C : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : CLC : ADC #$10 : STA $00
	LDA !barkerY : CLC : ADC $09 : STA $01
	LDA !barkerTopLeftTile : CLC : ADC $0D : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : STA $00
	LDA !barkerY : CLC : ADC $09 : CLC : ADC #$10 : STA $01
	LDA !barkerBottomRightTile : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : CLC : ADC #$10 : STA $00
	LDA !barkerY : CLC : ADC $09 : CLC : ADC #$10 : STA $01
	LDA !barkerBottomLeftTile : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
	
	LDA !barkerX : CLC : ADC #$20 : STA $00
	LDA !barkerY : CLC : ADC $09 : CLC : ADC #$10 : STA $01
	LDA !barkerExtraCaneTile : BEQ + : STA $02
	LDA #$02 : JSR BarkerGenericTileDraw
+
	RTS
	
	
	

;BarkerDrawFrontFoot:
;	LDA !barkerFeetState
;	BEQ .normal
;	CMP #$01
;	BEQ .normal
;	CMP #$02
;	BEQ .normal
;	CMP #$03
;	BEQ .running
;	RTS
;	
;.normal					; Just draw the normal feet.  Barker's always facing left here.  No front feet will be drawn.
;	LDA !barkerX
;	STA $00
;	
;	LDA !barkerY
;	CLC : ADC #$10 : CLC : ADC $09
;	STA $01
;
;	LDA #$42
;	STA $02
;	
;	LDA #$02
;	RTS
;	
;.jumpingUp
;	LDA !barkerX
;	CLC : ADC #$04
;	STA $00
;	
;	LDA !barkerY
;	CLC : ADC #$18 : CLC : ADC $09
;	STA $01
;	
;	LDA #$29
;	STA $02
;	
;	LDA #$00
;	RTS
;	
;.jumpingDown
;	LDA !barkerX
;	CLC : ADC #$04
;	STA $00
;	
;	LDA !barkerY
;	CLC : ADC #$18 : CLC : ADC $09
;	STA $01
;	
;	LDA #$28
;	STA $02
;	
;	LDA #$00
;	RTS
;	
;.running				; Barker's always facing right here
;	LDA !barkerX
;	;SEC : ADC #$04
;	STA $00
;	
;	LDA !barkerY
;	CLC : ADC #$10 : CLC : ADC $09
;	STA $01
;	
;	LDA $13
;	AND #$02
;	CLC
;	ADC #$0C
;	STA $02
;	
;	LDA #$02
;	RTS	
;	
;	
;	
;BarkerDrawBackFoot:
;	LDA !barkerFeetState
;	BEQ .normal
;	CMP #$01
;	BEQ .jumpingUp
;	CMP #$02
;	BEQ .jumpingDown
;	CMP #$03
;	BEQ .running
;	RTS
;	
;.normal
;.running
;	SEC
;	RTS
;	
;.jumpingUp
;.jumpingDown
;	JSR BarkerDrawFrontFoot
;	PHA
;	LDA $00
;	CLC : ADC #$01
;	STA $00
;	LDA $01
;	CLC : ADC #$01
;	STA $01
;	PLA
;	CLC
;	RTS
;
;	
;BarkerDrawBody:
;	LDA !barkerX
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$10 : CLC : ADC $09
;	STA $01
;	LDA #$40
;	STA $02
;	LDA #$02
;	RTS
	
;BarkerHeadOffset:
;db $00, $01
;BarkerHeadTile:
;db $08, $0A
;BarkerHeadHorizOffset:
;db $00, $03, $00, $FD
;	
;BarkerDrawHead:
;
;	LDA !barkerXFlip
;	LSR #5
;	CLC
;	ADC !barkerHeadFacingBack
;	TAY
;	LDA BarkerHeadHorizOffset,y
;	CLC
;	ADC !barkerX
;	STA $00
;	
;	
;	LDY #$00
;	LDA !textIsAppearing
;	BEQ +
;	LDA $13
;	AND #$04
;	LSR
;	LSR
;	TAY
;+
;	LDA !barkerY
;	CLC
;	ADC BarkerHeadOffset,y : CLC : ADC $09
;	STA $01
;	
;	LDA BarkerHeadTile,y
;	STA $02
;	
;	LDA #$3A
;	ORA !barkerXFlip
;	LDY !barkerHeadFacingBack
;	BEQ +
;	EOR #$40
;+
;	
;	STA $03
;	
;	LDA #$02
;	JSL DrawTile
;	
;	RTS
;	
;BarkerDrawArm:
;	LDA !barkerArmState
;	BEQ .normal
;	CMP #$01
;	BEQ .pointToMario
;	CMP #$02
;	BEQ .holdingUp
;	CMP #$03
;	BEQ .behind
;	CLC
;	RTS
;	
;.normal
;	LDA !barkerX
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$10 : CLC : ADC $09
;	STA $01
;	LDA #$48
;	STA $02
;	LDA #$02
;	CLC
;	RTS
;	
;.pointToMario
;	LDA !barkerX
;	DEC
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$10 : CLC : ADC $09
;	STA $01
;	LDA #$4A
;	STA $02
;	LDA #$02
;	CLC
;	RTS
;	
;.holdingUp
;	LDA !barkerX
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$08 : CLC : ADC $09
;	STA $01
;	LDA #$48
;	STA $02
;	LDA #$BA
;	STA $03
;	
;	LDA #$02
;	JSL DrawTile
;	SEC
;	RTS
;	
;.behind	
;	LDA !barkerX
;	DEC
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$11 : CLC : ADC $09
;	STA $01
;	LDA #$4A
;	STA $02
;	
;	LDA #$3A
;	STA $03
;	
;	LDA #$02
;	JSL DrawTile
;	
;	SEC
;	RTS
;	
;CaneFrameTable:
;db $68, $6A, $6C, $6E, $68, $6A, $6C, $6E
;CaneFlipTable:
;db $00, $00, $00, $00, $C0, $C0, $C0, $C0
;	
;BarkerDrawCane:
;	LDA !barkerArmState
;	BEQ .normal
;	CMP #$01
;	BEQ .pointToMario
;	CMP #$02
;	BEQ .holdingUp
;	CMP #$03
;	BEQ .behind
;	RTS
;
;.normal
;	LDA !barkerX
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$10 : CLC : ADC $09
;	STA $01
;	BRA .continue
;	
;.pointToMario
;	LDA !barkerX
;	SEC
;	SBC #$0B
;	STA $00
;	
;	LDA !barkerY
;	CLC : ADC #$0A : CLC : ADC $09
;	STA $01
;	BRA .continue
;	
;.holdingUp
;	LDA !barkerSpinningCane
;	BNE +
;	LDA !barkerX
;	CLC : ADC #$06
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$04 : CLC : ADC $09
;	STA $01
;	BRA .continue
;	
;	+
;	LDA !barkerX
;	CLC : ADC #$03
;	STA $00
;	LDA !barkerY
;	CLC : ADC #$05 : CLC : ADC $09
;	STA $01
;	
;.continue
;	PHX
;	LDX !barkerCaneState
;	LDA CaneFrameTable,x
;	STA $02
;	LDA CaneFlipTable,x
;	ORA #$34
;	STA $03
;	PLX
;	
;	LDA #$02
;	JSL DrawTile
;	RTS
;	
;
;.behind
;	LDA !barkerX
;	SEC
;	SBC #$0B
;	STA $00
;	
;	LDA !barkerY
;	CLC : ADC #$0B : CLC : ADC $09
;	STA $01
;	
;	LDA #$68
;	STA $02
;	
;	LDA #$B4
;	STA $03
;	
;	LDA #$02
;	JSL DrawTile
;	RTS
;	
;	
;
BarkerDrawLuigi:
	LDA !barkerLuigiDrawState
	BEQ .ret
	CMP #$01
	BEQ .scared
	CMP #$02
	BEQ .hanging
.ret
	RTS

.scared
	LDA $13
	AND #$01
	CLC : ADC #$83
	STA $00
	
	LDA #$50
	STA $01
	LDA #$8A
	STA $02
	LDA #$33
	STA $03
	
	LDA #$02
	JSL DrawTile
	RTS
	
.hanging
	LDA !barkerX
	;SEC
	;SBC #$24
	STA $00
	
	LDA !barkerY
	;CLC
	;ADC #$0A
	SEC
	SBC #$0C
	CLC
	ADC $09
	STA $01
	
	LDA #$60
	STA $02
	
	LDA #$32
	STA $03
	LDA #$02
	JSL DrawTile
	
	LDA #$62
	STA $02
	LDA $00
	CLC
	ADC #$10
	STA $00
	
	LDA #$02
	JSL DrawTile
	
	
	RTS

BarkerDrawGoomba:
	LDA !barkerDrawGoomba
	BEQ +
	LDA #$83
	STA $00
	LDA #$50
	STA $01
	LDA $13
	AND #$08
	LSR
	LSR
	CLC
	ADC #$44
	STA $02
	LDA #$34
	STA $03
	LDA #$02
	JSL DrawTile
+
	RTS

BarkerNMI:
RTL
