
freecode
AllCutsceneData2:

strings1:
dl CS1T1, CS1T2, CS1T3, CS1T4, CS1T5, CS1T6, CS1T7, CS1T8, CS1T9, CS1TA, CS1TB, CS1TC, CS1TD, CS1TE, CS1TF, CS1T10
dl CS1T11, CS1T12, CS1T13, CS1T14, CS1T15
strings2:
dl CS2T1
strings3:
dl CS3T1
strings4:
dl CS4T1
strings5:
dl CS5T1
strings6:
;dl CS6T1
strings7:
dl CS7T1
strings8:
dl CS8T1
strings9:
dl CS9T1
stringsA:
dl CSAT1
stringsB:
dl CSBT1
stringsC:
dl CSCT1
stringsD:
dl CSDT1
stringsE:
dl CSET1
stringsF:
dl CSFT1
strings10:
dl CS10T1

cutsceneStrings:
dl strings1, strings2, strings3, strings4, strings5, strings6, strings7, strings8, strings9, stringsA, stringsB, stringsC, stringsD, stringsE, stringsF, strings10

cleartable
table charTable.txt

cutsceneInits:
dl CSINIT1
dl CSINIT2
dl CSINIT3
dl CSINIT4
dl CSINIT5
dl CSINIT6
dl CSINIT7
dl CSINIT8
dl CSINIT9
dl CSINITA
dl CSINITB
dl CSINITC
dl CSINITD
dl CSINITE
dl CSINITF
dl CSINIT10


;CSINIT5:
CSINIT6:
;CSINITC:

;CSINIT9:


;CSINITE:
;CSINITF:
BRK


incsrc Cutscene1.asm	; Intro cutscene (some Doc Croc problems)
incsrc Cutscene2.asm	; Doc Croc introduces Mario to the world 1 factory (no problems)
incsrc Cutscene3.asm	; Truntec (after freezing, Mario "teleports" to the ground)
incsrc Cutscene4.asm	; Driad (no problems)
incsrc Cutscene5.asm	; Tanuki (no problems)
;incsrc Cutscene6.asm	; Jelectro (crash, intentional. Cutscene to be implemented in-game)
incsrc Cutscene7.asm	; Madam Mau (no problems)
incsrc Cutscene8.asm	; Barker Duck (no problems)
incsrc Cutscene9.asm	; Hindenbird (no problems)

freecode
AllCutsceneData3:

incsrc CutsceneA.asm	; Frank (Frank starts falling from too low, lands too low, Mario's jump should have a SFX)
incsrc CutsceneB.asm	; Doc Croc talks with Norveg about Mario (door sound effect?)
incsrc CutsceneC.asm	; Doc Croc talks to Mario about the world 8 factory (trap door open SFX?, Mario fall SFX)
incsrc CutsceneD.asm	; Doc Croc (pre-boss) (no problems)
incsrc CutsceneE.asm	; Bowser on the beach (no problems)
incsrc CutsceneF.asm	; Norveg (pre-boss)
incsrc Cutscene10.asm	; Deedle ball
