!FreeRamForMode2 = $7F9D00	;Change if it conflicts with other patches (about $40 bytes)
!levelnum = #$006E

header
lorom

org $0081D5
autoclean JSL main
NOP

org read3($00820A)	; Should be wherever $8209 JSLs to
PHP
JSL Hijack
NOP

freecode

main:
	REP #$20
	LDA $010B
	CMP !levelnum
	SEP #$20
	BEQ SpecialStuffs
NopeGoBack:
	lda $0100
	cmp #$02
	bcs +
	lda #$03
	sta $2105
	rtl
	+
	cmp #$02
	beq +
	LDA #$09
	STA $2105
	+
	RTL
                
SpecialStuffs:
	LDA $0100
	cmp #$0B
	beq +
	CMP #$13
	BCC NopeGoBack
	+
	LDA #$02
	STA $2105
	RTL

Hijack:
	lda $0100
	cmp #$0B
	beq ++
	cmp #$13
	bcc +
	++
	REP #$20
	LDA $010B
	CMP !levelnum
	SEP #$20
	BEQ DestructiveReturn
	+
	LDA $0100
	CMP #$01
	RTL
		
DestructiveReturn:
	REP #$20
	LDA !FreeRamForMode2+$40
	AND #$0FFF
	STA $1C
	SEP #$20

	LDA #$01
	STA $4300

	LDA #$18
	STA $4301

	LDA.b #!FreeRamForMode2+2
	STA $4302
	LDA.b #!FreeRamForMode2+2>>8
	STA $4303
	LDA.b #!FreeRamForMode2+2>>16
	STA $4304

	REP #$20
	LDA #$5000
	STA $2116

	LDA #$007E
	STA $4305
	SEP #$20

	LDA #$01	;And start
	STA $420B

	PLA
	PLA
	PLA
	PLP
	RTL
