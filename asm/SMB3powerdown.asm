; Perfect SMB 3 Powerdown
; Revised by ICB
;
; found in SMwiki, Submitted by aiyo


header
lorom
org $00F5F8
autoclean JML Main

freecode

Main:
LDA $19       	;\
CMP #$02      	; |If Mario has a flower or cape
BCS to_super  	; |then jump to Big Mario code.
STZ $19		; /set Mario to normal ($00)
LDA #$01	; \ Play Mario Shrink
STA $71		;/Animation
LDA #$29    	; |Time to play Animation
JML $80F61D	; Jump back

to_super:            	;\ 
LDA #$01      	; |Set Status
STA $19       	; /To Big
LDA #$0C	; \ Play another sound
STA $1DF9	; / that is closest to SMB3 sound
JSL $81C5AE	; \This is the 'get cape' animation, for the smoke puff
LDA #$11        	; /and we play it for a very short time
JML $80F61D	;| Jump Back

