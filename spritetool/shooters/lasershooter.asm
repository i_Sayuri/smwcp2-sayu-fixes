!ClusterSpriteNum = $0C

print "INIT ",pc
print "MAIN ",pc
PHB
PHK
PLB
JSR Lasershooter
PLB
RTL

Return:
RTS

Lasershooter:
LDA $17AB,x
BEQ Return
CMP #$11		;on shooter load, timer is always #$10 for some reason
BCC Return
STZ $17AB,x

;LDA $179B,x
;CMP $D1
;LDA $17A3,x
;SBC $D2
;BNE Return		;don't shoot if mario is on right side of shooter?

;LDA $96
;SEC
;SBC $178B,x
;LDA $97
;SBC $1793,x
;BMI Return		;don't shoot if mario is above shooter

SEP #$20

PHX
TXY
LDX #$13
-
LDA $1892,x
BEQ +
DEX
BPL -
PLX
RTS

+
LDA #$01
STA $1DF9
LDA #!ClusterSpriteNum
STA $1892,x

LDA $1783,y
AND #$40
STA $00
BEQ +
LDA #$01
STA $1E52,x
+

LDA $00
BNE +

LDA $179B,y
SEC
SBC #$08
STA $1E16,x
LDA $17A3,y
SBC #$00
STA $1E3E,x
BRA ++

+
LDA $179B,y
CLC
ADC #$08
STA $1E16,x
LDA $17A3,y
ADC #$00
STA $1E3E,x


++
LDA $178B,y
SEC
SBC #$08
STA $1E02,x
LDA $1793,y
SBC #$00
STA $1E2A,x

PLX

LDA #$01
STA $18B8

RTS
