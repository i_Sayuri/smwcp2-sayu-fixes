;SMB3 - Fire snake
;by smkdan
;uses 768 bytes worth of sprite tables as in the fire chomp

DIR: = $03
!PROPRAM = $04
!TEMP = $09
IND: = $0A
!INDINDEX = $0C

;general def
!JUMPCNT = $20		;32 frames between jumps
!JUMPCNT2 = $28
!JUMPYSPD = $D8		;what to set yspd to on jumps (originally $E0)
!JUMPYSPD2 = $F4
JUMPXSPD: db $09,$F7	;what to set xspd on jumps (originally $06, $FA)
JUMPXSPD2: db $16,$EA

;table def
!JUMPRAM = $1570	;jumping delay counter

PRINT "INIT ",pc
	JSR INITTABLE	;init extra sprite tables
	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE
	LDA #!JUMPCNT	;set delay count for jumping
	BRA STORE_JUMPCOUNT
LOW_TYPE:
	LDA #!JUMPCNT2
STORE_JUMPCOUNT:
	STA !JUMPRAM,x
      	RTL

PRINT_TWO: "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL

RETURN_I:
	RTS

Run:
	JSR SUB_OFF_SCREEN_X0
	JSL $20CA4F
	JSR GFX			;draw sprite

	LDA $14C8,x
	CMP #$08          	 
	BNE RETURN_I           
	LDA $9D		;locked sprites?
	BNE RETURN_I
	LDA $15D0,x	;yoshi eating?
	BNE RETURN_I

	JSL $01802A	;updated speed
	JSL $01A7DC	;mario interact

	JSR LOGPOSITIONS

	LDA $1588,x
	AND #$04	;on ground?
	BNE ONGROUND

;inair
	LDA $1588,x		; \ check if sprite is touching object side
	AND #%00000011		; /
	BEQ DONT_CHANGE_DIR	; don't change direction if not touching side
	LDA $157C,x		; \
	EOR #$01		;  | flip sprite direction
	STA $157C,x		; /
	
	LDY $157C,x	;index for xspd
	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE5
	LDA JUMPXSPD,y
	BRA STORE_XSPEED2
LOW_TYPE5:
	LDA JUMPXSPD2,y
STORE_XSPEED2:
	STA $B6,x	;new xspd
DONT_CHANGE_DIR:


	DEC $AA,x	;reduce regular gravity by decreasing speed
	DEC $AA,x	;...twice

	BRA SKIPJUMP
	
ONGROUND:
	STZ $B6,x	;no xspd when on ground

	DEC !JUMPRAM,x	;decrement jumping counter
	BNE SKIPJUMP	;ready to jump?

;jump
	JSR SUB_HORZ_POS	;face mario
	TYA
	STA $157C,x		;new direction

	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE2
	LDA #!JUMPCNT	;set jump counter back
	BRA STORE_JUMPCOUNT2
LOW_TYPE2:
	LDA #!JUMPCNT2
STORE_JUMPCOUNT2:
	STA !JUMPRAM,x

	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE3
	LDA #!JUMPYSPD	;set jump yspeed
	BRA STORE_YSPEED
LOW_TYPE3:
	LDA #!JUMPYSPD2
STORE_YSPEED:
	STA $AA,x

	LDY $157C,x	;index for xspd
	LDA $7FAB10,x
	AND #$04
	BNE LOW_TYPE4
	LDA JUMPXSPD,y
	BRA STORE_XSPEED
LOW_TYPE4:
	LDA JUMPXSPD2,y
STORE_XSPEED:
	STA $B6,x	;new xspd

SKIPJUMP:
;interaction
	LDA $E4,x	;xlow
	PHA
	LDA $14E0,x	;xhigh
	PHA
	LDA $D8,x	;ylow
	PHA
	LDA $14D4,x	;yhigh, all preserved
	PHA

	LDA #$00
	PHA
INTLOOP:
	PLA
	TAY
	INC A
	INC A
	PHA
	
	REP #$30	;16bit AXY
	TYA
	AND.w #$00FF	;wipe high
	TAY
	LDA $15E9	;load index
	AND.w #$00FF
	CLC
	ADC INDEXES,y	;load index again, ADD 15E9!
	TAX
	SEP #$20	;8bit A
	LDA $7F0000,x	;load byte from table
	SEP #$10	;8bitXY
	LDX $15E9	;sprite index
	STA $E4,x	;storex

	PLA
	TAY
	INC A
	INC A
	PHA
	REP #$30	;16bit AXY
	TYA
	AND.w #$00FF	;wipe high
	TAY
	LDA $15E9	;load index
	AND.w #$00FF
	CLC
	ADC INDEXES,y	;load index again, ADD 15E9!
	TAX
	SEP #$20	;8bit A
	LDA $7F0000,x	;load byte
	SEP #$10	;8bit XY
	LDX $15E9	;sprite index
	STA $D8,x	;storey 

	JSL $01A7DC	;mario interact, CAN ONLY RUN ONCE ARGH

	PLA
	PHA
	CMP #$0C	;done yet?
	BNE INTLOOP	;interaction again
	PLA

	PLA
	STA $14D4,x	;yhigh, all restored
	PLA
	STA $D8,x
	PLA
	STA $14E0,x
	PLA
	STA $E4,x
	
	RTS        
			
;=====

TILEMAP:		db $A8,$AA	;head tiles
TILEMAPB:	db $C8,$CA	;body tiles

INDEXES:	dw !XLO_10&$FFFF,!YLO_10&$FFFF	;INDEXES to use out of the sprite tables
	dw !XLO_21&$FFFF,!YLO_21&$FFFF
	dw !XLO_32&$FFFF,!YLO_32&$FFFF

GFX:
	LDA $14		;frame counter
	LSR A
	LSR A
	AND #$01	;flicker every 4th frame
	STA !TEMP

	LDA $15F6,x	;properties...
	STA !PROPRAM

;some sort of loop
	LDA $00
	STA $0300,y	;xpos
	LDA $01
	STA $0301,y	;ypos
	LDX !TEMP
	LDA TILEMAP,x
	STA $0302,y	;chr
	LDA !PROPRAM
	ORA $64
	STA $0303,y	;prop

	INY		;next tile
	INY
	INY
	INY	

;draw body parts, what a mess this is 8D
	STZ !INDINDEX	;reset indirect index
	
OAM_BODY:
	LDX !INDINDEX	;load index
	INC !INDINDEX	;advance index to next word
	INC !INDINDEX
	LDA $15E9	;load sprite index
	REP #$30	;AXY
	AND.w #$00FF	;wipe high
	CLC
	ADC INDEXES,x	;add sprite table index
	TAX		;into X
	SEP #$20	;8bit A again

	LDA $00		;????
	CLC
	ADC $7F0000,x	;load from table	
	LDX.w #$0000	;wipe x
	SEP #$10	;8bit XY
	LDX $15E9
	SEC
	SBC $E4,x	;sub xpos
	STA $0300,y	;xpos

	LDX !INDINDEX	;load index
	INC !INDINDEX	;advance index to next word
	INC !INDINDEX
	LDA $15E9	;load sprite index
	REP #$30	;AXY
	AND.w #$00FF	;wipehigh
	CLC
	ADC INDEXES,x	;add sprite table index
	TAX		;into X
	SEP #$20	;8bit A again	

	LDA $01		;???
	CLC
	ADC $7F0000,x	;load from table
	LDX.w #$0000	;wipe x
	SEP #$10	;8bit XY again
	LDX $15E9
	SEC
	SBC $D8,x	;sub ypos
	STA $0301,y	;ypos

	LDX !TEMP	;frame index
	LDA TILEMAPB,x
	STA $0302,y	;chr

	LDA !PROPRAM
	ORA $64
	STA $0303,y	;prop
	
	INY
	INY
	INY
	INY		;next tile
	LDA !INDINDEX	;test ind index
	CMP #$0C	;3*4
	BNE OAM_BODY

	LDX $15E9	;restore sprite index

	LDY #$02	;16x16
	LDA #$03	;4 tiles
	JSL $01B7B3	;reserve

	RTS
	
;=================
;BORROWED ROUTINES
;=================

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_HORZ_POS
; This routine determines which side of the sprite Mario is on.  It sets the Y register
; to the direction such that the sprite would face Mario
; It is ripped from $03B817
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_HORZ_POS:		LDY #$00
			LDA $94
			SEC
			SBC $E4,x
			STA $0F
			LDA $95
			SBC $14E0,x
			BPL SPR_L16
			INY
SPR_L16:			RTS

;these two are from the firechomp
INITTABLE:
	LDA $D8,x	;y;set positions to be all behind the head
      	STA !YLO_1,x
      	STA !YLO_2,x
      	STA !YLO_3,x
      	STA !YLO_4,x
      	STA !YLO_5,x
      	STA !YLO_6,x
      	STA !YLO_7,x
      	STA !YLO_8,x
      	STA !YLO_9,x
      	STA !YLO_10,x
      	STA !YLO_11,x
      	STA !YLO_12,x
      	STA !YLO_13,x
      	STA !YLO_14,x
      	STA !YLO_15,x
      	STA !YLO_16,x
      	STA !YLO_17,x
      	STA !YLO_18,x
      	STA !YLO_19,x
      	STA !YLO_20,x
      	STA !YLO_21,x
      	STA !YLO_22,x
      	STA !YLO_23,x
      	STA !YLO_24,x
      	STA !YLO_25,x
      	STA !YLO_26,x
      	STA !YLO_27,x
      	STA !YLO_28,x
      	STA !YLO_29,x
      	STA !YLO_30,x
      	STA !YLO_31,x
      	STA !YLO_32,x
      	LDA $E4,x	;x
      	STA !XLO_1,x
      	STA !XLO_2,x
      	STA !XLO_3,x
      	STA !XLO_4,x
      	STA !XLO_5,x
      	STA !XLO_6,x
      	STA !XLO_7,x
      	STA !XLO_8,x
      	STA !XLO_9,x
      	STA !XLO_10,x
      	STA !XLO_11,x
      	STA !XLO_12,x
      	STA !XLO_13,x
      	STA !XLO_14,x
      	STA !XLO_15,x
      	STA !XLO_16,x
      	STA !XLO_17,x
      	STA !XLO_18,x
      	STA !XLO_19,x
      	STA !XLO_20,x
      	STA !XLO_21,x
      	STA !XLO_22,x
      	STA !XLO_23,x
      	STA !XLO_24,x
      	STA !XLO_25,x
      	STA !XLO_26,x
      	STA !XLO_27,x
      	STA !XLO_28,x
      	STA !XLO_29,x
      	STA !XLO_30,x
      	STA !XLO_31,x
      	STA !XLO_32,x

	RTS

LOGPOSITIONS:
      	LDA !YLO_31,x
      	STA !YLO_32,x
      	LDA !YLO_30,x
      	STA !YLO_31,x
      	LDA !YLO_29,x
      	STA !YLO_30,x
      	LDA !YLO_28,x
      	STA !YLO_29,x
      	LDA !YLO_27,x
      	STA !YLO_28,x
      	LDA !YLO_26,x
      	STA !YLO_27,x
      	LDA !YLO_25,x
      	STA !YLO_26,x
      	LDA !YLO_24,x
      	STA !YLO_25,x
      	LDA !YLO_23,x
      	STA !YLO_24,x
      	LDA !YLO_22,x
      	STA !YLO_23,x
      	LDA !YLO_21,x
      	STA !YLO_22,x
      	LDA !YLO_20,x
      	STA !YLO_21,x
      	LDA !YLO_19,x
      	STA !YLO_20,x
      	LDA !YLO_18,x
      	STA !YLO_19,x
      	LDA !YLO_17,x
      	STA !YLO_18,x
      	LDA !YLO_16,x
      	STA !YLO_17,x
      	LDA !YLO_15,x
      	STA !YLO_16,x
      	LDA !YLO_14,x
      	STA !YLO_15,x
      	LDA !YLO_13,x
      	STA !YLO_14,x
      	LDA !YLO_12,x
      	STA !YLO_13,x
      	LDA !YLO_11,x
      	STA !YLO_12,x
      	LDA !YLO_10,x
      	STA !YLO_11,x
      	LDA !YLO_9,x
      	STA !YLO_10,x
      	LDA !YLO_8,x
      	STA !YLO_9,x
      	LDA !YLO_7,x
      	STA !YLO_8,x
      	LDA !YLO_6,x
      	STA !YLO_7,x
      	LDA !YLO_5,x
      	STA !YLO_6,x
      	LDA !YLO_4,x
      	STA !YLO_5,x
      	LDA !YLO_3,x
      	STA !YLO_4,x
      	LDA !YLO_2,x
      	STA !YLO_3,x
      	LDA !YLO_1,x
      	STA !YLO_2,x
      	LDA $D8,x
      	STA !YLO_1,x
      	LDA !XLO_31,x
      	STA !XLO_32,x
      	LDA !XLO_30,x
      	STA !XLO_31,x
      	LDA !XLO_29,x
      	STA !XLO_30,x
      	LDA !XLO_28,x
      	STA !XLO_29,x
      	LDA !XLO_27,x
      	STA !XLO_28,x
      	LDA !XLO_26,x
      	STA !XLO_27,x
      	LDA !XLO_25,x
      	STA !XLO_26,x
      	LDA !XLO_24,x
      	STA !XLO_25,x
      	LDA !XLO_23,x
      	STA !XLO_24,x
      	LDA !XLO_22,x
      	STA !XLO_23,x
      	LDA !XLO_21,x
      	STA !XLO_22,x
      	LDA !XLO_20,x
      	STA !XLO_21,x
      	LDA !XLO_19,x
      	STA !XLO_20,x
      	LDA !XLO_18,x
      	STA !XLO_19,x
      	LDA !XLO_17,x
      	STA !XLO_18,x
      	LDA !XLO_16,x
      	STA !XLO_17,x
      	LDA !XLO_15,x
      	STA !XLO_16,x
      	LDA !XLO_14,x
      	STA !XLO_15,x
      	LDA !XLO_13,x
      	STA !XLO_14,x
      	LDA !XLO_12,x
      	STA !XLO_13,x
      	LDA !XLO_11,x
      	STA !XLO_12,x
      	LDA !XLO_10,x
      	STA !XLO_11,x
      	LDA !XLO_9,x
      	STA !XLO_10,x
      	LDA !XLO_8,x
      	STA !XLO_9,x
      	LDA !XLO_7,x
      	STA !XLO_8,x
      	LDA !XLO_6,x
      	STA !XLO_7,x
      	LDA !XLO_5,x
      	STA !XLO_6,x
      	LDA !XLO_4,x
      	STA !XLO_5,x
      	LDA !XLO_3,x
      	STA !XLO_4,x
      	LDA !XLO_2,x
      	STA !XLO_3,x
      	LDA !XLO_1,x
      	STA !XLO_2,x
      	LDA $E4,x
      	STA !XLO_1,x

	RTS


	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA.w $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return

;definitions from firechomp, beats remaking it myself
;----------------------------------------------------

!FREESPACE = $7F8900

		!YLO_1 = !FREESPACE+0
		!YLO_2 = !FREESPACE+12
		!YLO_3 = !FREESPACE+24
		!YLO_4 = !FREESPACE+36
		!YLO_5 = !FREESPACE+48
		!YLO_6 = !FREESPACE+60
		!YLO_7 = !FREESPACE+72
		!YLO_8 = !FREESPACE+84
		!YLO_9 = !FREESPACE+96
		!YLO_10 = !FREESPACE+108
		!YLO_11 = !FREESPACE+120
		!YLO_12 = !FREESPACE+132
		!YLO_13 = !FREESPACE+144
		!YLO_14 = !FREESPACE+156
		!YLO_15 = !FREESPACE+168
		!YLO_16 = !FREESPACE+180
		!YLO_17 = !FREESPACE+192
		!YLO_18 = !FREESPACE+204
		!YLO_19 = !FREESPACE+216
		!YLO_20 = !FREESPACE+228
		!YLO_21 = !FREESPACE+240
		!YLO_22 = !FREESPACE+252
		!YLO_23 = !FREESPACE+264
		!YLO_24 = !FREESPACE+276
		!YLO_25 = !FREESPACE+288
		!YLO_26 = !FREESPACE+300
		!YLO_27 = !FREESPACE+312
		!YLO_28 = !FREESPACE+324
		!YLO_29 = !FREESPACE+336
		!YLO_30 = !FREESPACE+348
		!YLO_31 = !FREESPACE+360
		!YLO_32 = !FREESPACE+372

		!XLO_1 = !FREESPACE+384
		!XLO_2 = !FREESPACE+396
		!XLO_3 = !FREESPACE+408
		!XLO_4 = !FREESPACE+420
		!XLO_5 = !FREESPACE+432
		!XLO_6 = !FREESPACE+444
		!XLO_7 = !FREESPACE+456
		!XLO_8 = !FREESPACE+468
		!XLO_9 = !FREESPACE+480
		!XLO_10 = !FREESPACE+492
		!XLO_11 = !FREESPACE+504
		!XLO_12 = !FREESPACE+516
		!XLO_13 = !FREESPACE+528
		!XLO_14 = !FREESPACE+540
		!XLO_15 = !FREESPACE+552
		!XLO_16 = !FREESPACE+564
		!XLO_17 = !FREESPACE+576
		!XLO_18 = !FREESPACE+588
		!XLO_19 = !FREESPACE+600
		!XLO_20 = !FREESPACE+612
		!XLO_21 = !FREESPACE+624
		!XLO_22 = !FREESPACE+636
		!XLO_23 = !FREESPACE+648
		!XLO_24 = !FREESPACE+660
		!XLO_25 = !FREESPACE+672
		!XLO_26 = !FREESPACE+684
		!XLO_27 = !FREESPACE+696
		!XLO_28 = !FREESPACE+708
		!XLO_29 = !FREESPACE+720
		!XLO_30 = !FREESPACE+732
		!XLO_31 = !FREESPACE+744
		!XLO_32 = !FREESPACE+756