;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Sumo brother disassembly
; By nekoh
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    LDY #$00                
                    LDA $D1                   
                    SEC                       
                    SBC $E4,X       
                    STA $0F                   
                    LDA $D2                   
                    SBC.w $14E0,X     
                    BPL RETURN01AD41          
                    INY                             
                    TYA                       
                    STA $157C,X     
RETURN01AD41:       RTL                       


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                           
                    PRINT "MAIN ",pc
                    PHB                       
                    PHK                       
                    PLB                             
                    JSR PORCUPUFFER         
                    PLB                       
                    RTL                       ; Return 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PORCUPUFFACCEL:     db $01,$FF
PORCUPUFFMAXSPEED:  db $10,$F0

PORCUPUFFER:        JSR POCRUPUFFERGFX         
                    LDA $9D     
                    BNE RETURN038586          
                    LDA $14C8,X             
                    CMP #$08                
                    BNE RETURN038586          
                    JSR SUBOFFSCREEN0BNK3   
                    JSL $01803A  
                    JSR SUBHORZPOSBNK3      
                    TYA                       
                    STA $157C,X     
                    LDA $14     
                    AND #$03                
                    BNE CODE_03855E           
                    LDA $B6,X    ; \ Branch if at max speed 
                    CMP PORCUPUFFMAXSPEED,Y ;  | 
                    BEQ CODE_03855E           ; / 
                    CLC                       ; \ Otherwise, accelerate 
                    ADC PORCUPUFFACCEL,Y    ;  | 
                    STA $B6,X    ; / 
CODE_03855E:        LDA $B6,X    
                    PHA                       
                    LDA $17BD               
                    ASL                       
                    ASL                       
                    ASL                       
                    CLC                       
                    ADC $B6,X    
                    STA $B6,X    
                    JSL $018022   
                    PLA                       
                    STA $B6,X    
                    JSL $019138         
                    LDY #$04                
                    LDA.w $164A,X             
                    BEQ CODE_038580           
                    LDY #$FC                
CODE_038580:        STY $AA,X    
                    JSL $01801A   
RETURN038586:       RTS                       ; Return 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; PORCUPUFFER graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

POCRUPUFFERDISPX:   db $F8,$08,$F8,$08,$08,$F8,$08,$F8
POCRUPUFFERDISPY:   db $F8,$F8,$08,$08
POCRUPUFFERTILES:   db $86,$C0,$A6,$C2,$86,$C0,$A6,$8A
POCRUPUFFERGFXPROP: db $0D,$0D,$0D,$0D,$4D,$4D,$4D,$4D

POCRUPUFFERGFX:     JSL $20CA4F
                    LDA $14     
                    AND #$04                
                    STA $03                   
                    LDA $157C,X     
                    STA $02                   
                    PHX                       
                    LDX #$03                
CODE_0385B4:        LDA $01                   
                    CLC                       
                    ADC POCRUPUFFERDISPY,X  
                    STA $0301,Y         
                    PHX                       
                    LDA $02                   
                    BNE CODE_0385C6           
                    TXA                       
                    ORA #$04                
                    TAX                       
CODE_0385C6:        LDA $00                   
                    CLC                       
                    ADC POCRUPUFFERDISPX,X  
                    STA $0300,Y         
                    LDA POCRUPUFFERGFXPROP,X 
                    ORA $64                   
                    STA $0303,Y          
                    PLA                       
                    PHA                       
                    ORA $03                   
                    TAX                       
                    LDA POCRUPUFFERTILES,X  
                    STA $0302,Y          
                    PLX                       
                    INY                       
                    INY                       
                    INY                       
                    INY                       
                    DEX                       
                    BPL CODE_0385B4           
                    PLX                       
                    LDY #$02                
                    LDA #$03                
                    JSL $01B7B3      
                    RTS                       ; Return 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Sub Horizontal Position Bank 3
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUBHORZPOSBNK3:     LDY #$00                
                    LDA $94         
                    SEC                       
                    SBC $E4,X       
                    STA $0F                   
                    LDA $95       
                    SBC $14E0,X     
                    BPL RETURN03B828          
                    INY                       
RETURN03B828:       RTS                       ; Return 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Sprite Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DATA_03B83B:                      db $40,$B0
DATA_03B83D:                      db $01,$FF
DATA_03B83F:                      db $30,$C0,$A0,$80,$A0,$40,$60,$B0
DATA_03B847:                      db $01,$FF,$01,$FF,$01,$00,$01,$FF

SUBOFFSCREEN0BNK3:  STZ $03                     ; / 
                    JSR ISSPROFFSCREENBNK3      ; \ if sprite is not off screen, return 
                    BEQ RETURNONE               ; / 
                    LDA $5B                     ; \  vertical level 
                    AND #$01                    ;  | 
                    BNE VERTICALLEVELBNK3       ; / 
                    LDA $D8,X                   ; \ 
                    CLC                         ;  | 
                    ADC #$50                    ;  | if the sprite has gone off the bottom of the level... 
                    LDA $14D4,X                 ;  | 
                    ADC #$00                    ;  | 
                    CMP #$02                    ;  | 
                    BPL OFFSCRERASESPRBNK3      ; /    ...erase the sprite 
                    LDA $167A,X                 ; \ if "process offscreen" flag is set, return 
                    AND #$04                    ;  | 
                    BNE RETURNONE               ; / 
                    LDA $13      
                    AND #$01                
                    ORA $03                   
                    STA $01                   
                    TAY                       
                    LDA $1A    
                    CLC                       
                    ADC DATA_03B83F,Y       
                    ROL $00                   
                    CMP $E4,X       
                    PHP                       
                    LDA $1B    
                    LSR $00                   
                    ADC DATA_03B847,Y       
                    PLP                       
                    SBC $14E0,X     
                    STA $00                   
                    LSR $01                   
                    BCC CODE_03B8A8           
                    EOR #$80                
                    STA $00        
           
CODE_03B8A8:        LDA $00                   
                    BPL RETURNONE   
       
OFFSCRERASESPRBNK3: LDA $14C8,X                 ; \ If sprite status < 8, permanently erase sprite 
                    CMP #$08                    ;  | 
                    BCC OFFSCRKILLSPRBNK3       ; / 
                    LDY $161A,X                 ; \ Branch if should permanently erase sprite 
                    CPY #$FF                    ;  | 
                    BEQ OFFSCRKILLSPRBNK3       ; / 
                    LDA #$00                    ; \ Allow sprite to be reloaded by level loading routine 
                    STA $1938,Y                 ; / 
OFFSCRKILLSPRBNK3:  STZ $14C8,X 
            
RETURNONE:          RTS                         ; Return 

VERTICALLEVELBNK3:  LDA $167A,X                 ; \ If "process offscreen" flag is set, return 
                    AND #$04                    ;  | 
                    BNE RETURNONE               ; / 
                    LDA $13                     ; \ Return every other frame 
                    LSR                         ;  | 
                    BCS RETURNONE               ; / 
                    AND #$01                
                    STA $01                   
                    TAY                       
                    LDA $1C    
                    CLC                       
                    ADC DATA_03B83B,Y       
                    ROL $00                   
                    CMP $D8,X       
                    PHP                       
                    LDA $1D  
                    LSR $00                   
                    ADC DATA_03B83D,Y       
                    PLP                       
                    SBC $14D4,X     
                    STA $00                   
                    LDY $01                   
                    BEQ CODE_03B8F5           
                    EOR #$80                
                    STA $00  
                 
CODE_03B8F5:        LDA $00                   
                    BPL RETURNONE          
                    BMI OFFSCRERASESPRBNK3 
   
ISSPROFFSCREENBNK3: LDA $15A0,X                 ; \ If sprite is on screen, A = 0  
                    ORA $186C,X                 ;  | 
                    RTS                         ; / Return 

