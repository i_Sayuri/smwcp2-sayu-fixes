;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; P-Switch, by Davros.
;;
;; Description: A P-switch that will set the blue timer, silver timer, turn sprites into 
;; coins, stun sprites with an earthquake, spawn sprites and spawn blocks depending on it's 
;; palette.
;;
;; Uses first extra bit: YES
;;
;; Green P-Switch:
;; When the first extra bit is clear, it will spawn NORMAL sprites. When it's set, it will
;; spawn CUSTOM sprites.
;;
;; Gold P-Switch:
;; When the first extra bit is clear, it will spawn a question with a flower. When it's
;; set, it will spawn a question block with a feather.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extra Property Byte 1
;;    Bit 0 - don't carry sprite
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !BLUE_TIMER = $50              ; blue p-switch timer 
                    !GREY_TIMER = $50              ; grey p-switch timer

                    !QUAKE_TIMER = $20             ; shake GROUND timer
                    !QUAKE_TIMER2 = $20            ; shake GROUND timer

                    !SOUND_TO_GEN = $0B
                    !SOUND_TO_GEN2 = $09
		    !SOUND_TO_GEN3 = $02
		    !SOUND_TO_GEN4 = $10

		    !BLOCK_TILE1 = $011F           ; map16 value of the block in hex          
		    !BLOCK_TILE2 = $0120           ; map16 value of the block in hex           

		    !SPRITE_TO_GEN = $79           ; NORMAL sprite to spwan
                    !CUST_SPRITE_TO_GEN = $20      ; CUSTOM sprite to spawn

                    !EXTRA_BITS = $7FAB10
                    !EXTRA_PROP_1 = $7FAB28
                    !NEW_SPRITE_NUM = $7FAB9E

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
	            LDA $167A,x             ; \ don't hurt Mario when it touches the sprite 
	            STA $1528,x             ; / 
                    RTL                 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc                                    
                    PHB                     ; \
                    PHK                     ;  | main sprite function, just calls local subroutine
                    PLB                     ;  |
	            CMP #$09                ;  |
	            BCS HANDLE_STUNNED      ;  |
                    JSR SPRITE_CODE_START   ;  |
                    PLB                     ;  |
                    RTL                     ; /

HANDLE_STUNNED:      LDA $167A,x		    ; \ set to interact with Mario
	            AND #$7F                ;  |
	            STA $167A,x             ; /
	            
	            PHX                     ; \ set carried sprite graphics
                    JSR CARRIED_GFX         ;  |
	            PLX                     ; /
	            PLB
                    RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
         
         
JUSTSPATOUT:	    LDA #$08		    ; \ set NORMAL
		    STA $14C8,x		    ; / sprite mode
		    LDA #$08	            ; \ set timer for
		    STA $1540,x	            ; / "solidification"
                    RTS                     ; RETURN

SPRITE_CODE_START:   JSR SUB_GFX             ; graphics routine
                    LDA $14C8,x             ; \ if it's a shell
		    CMP #$09		    ;  | that means yoshi
		    BCS JUSTSPATOUT	    ; /  just spat it out
		    CMP #$0B		    ; \ if it's carried
		    BCS JUSTSPATOUT	    ; /  just spat it out
                    LDA $9D                 ; \ if sprites locked, RETURN
                    BNE RETURN              ; /

                    LDA $1558,x             ; \ if sprite not defeated (timer to show remains > 0)...
                    BEQ ALIVE               ; / ... go to ALIVE
                    STA $15D0,x             ; \ 
                    DEC A                   ;  } if sprite remains don't disappear next frame...
                    BNE RETURN              ; / ... RETURN
                    STZ $14C8,x             ; disappear the sprite
                    JSR SUB_SMOKE           ; smoke routine

                    LDA $15F6,x             ; \ if the sprite palette is...
                    CMP #$00                ;  | ...gold
                    BEQ SET_BLOCK           ; /
                    CMP #$0A                ; \ ...green
                    BEQ SET_SPRITE          ; /
                    BRA RETURN

SET_BLOCK:           JSR SPAWN_BLOCK         ; spawn block routine
                    BRA RETURN

SET_SPRITE:          JSR SPAWN_SPRITE        ; spawn sprite routine

RETURN:              RTS                     ; RETURN

ALIVE:               JSR SUB_OFF_SCREEN_X3   ; handle off screen situation
                    
                    LDA $1588,x             ; \ if sprite is in contact with an object...
                    AND #$03                ;  |
                    BEQ CEILING_CONTACT     ;  |
                    LDA $157C,x             ;  | flip the DIRECTION status
                    EOR #$01                ;  |
                    STA $157C,x             ; /
                    LDA $B6,x               ; \ flip speed
                    EOR #$FF                ;  |
                    STA $B6,x               ; /

CEILING_CONTACT:     LDA $1588,x             ; \ if sprite is in contact with the ceiling...
                    AND #$08                ;  |
                    BEQ GROUND              ; /
                    STZ $AA,x               ; no y speed
                    BRA SET_GROUND_SPEED

GROUND:              LDA $1588,x             ; \ if sprite is in contact with the GROUND...
                    AND #$04                ;  |
                    BEQ IN_AIR              ; /

SET_GROUND_SPEED:    JSR GROUND_SPEED        ; GROUND speed routine

IN_AIR:              JSL $01802A             ; update position based on speed values

	            LDA $1528,x             ; \ don't hurt Mario when it touches the sprite
	            STA $167A,x             ; /

                    JSL $01A7DC             ; check for Mario/sprite contact
                    BCC RETURN              ; RETURN if no contact
                    JSR SUB_VERT_POS        ; vertical Mario/sprite check routine
                    LDA $0E                 ; \ if Mario isn't above sprite, and there's vertical contact...
                    CMP #$E6                ;  | ... sprite wins
                    BPL SET_CARRIED         ; /
                    LDA $7D                 ; \  if Mario's y speed < 10 ...
                    CMP #$10                ;  } ... sprite will move Mario
                    BMI SET_CARRIED         ; /                       
                   
MARIO_WINS:          JSL $01AA33             ; set Mario speed
                    JSL $01AB99             ; display contact graphic
                    LDA #$20                ; \ ... time to show defeated sprite = $20
                    STA $1558,x             ; / 
		    LDA #!SOUND_TO_GEN       ; \ set sound effect
		    STA $1DF9               ; /

                    LDA $15F6,x             ; \ if the sprite palette is...
                    CMP #$02                ;  | ...gray
                    BEQ SET_MUSIC           ; /
                    CMP #$04                ; \ ...yellow
                    BEQ TURN_INTO_A_COIN    ; /
                    CMP #$06                ; \ ...blue
                    BEQ SET_MUSIC           ; /
                    CMP #$08                ; \ ...red
                    BEQ QUAKE               ; /
                    BRA RETURN2

SET_MUSIC:	    LDA $163E,x             ; \ if the timer is set...
		    BNE RETURN2             ; /
		    LDA $0DDA               ; \ if the music is playing...
		    BMI SET_TIMER           ; /
		    LDA #$0E                ; \ set P-switch music
		    STA $1DFB               ; /
SET_TIMER:	    LDA #$20                ; \ set timer
		    STA $163E,x             ; /
		    LDA #!QUAKE_TIMER        ; \ shake GROUND timer
		    STA $1887               ; /

                    LDA $15F6,x             ; \ if the sprite palette is...
                    CMP #$02                ;  | ...gray
                    BEQ SET_GREY_TIMER      ; /
                    CMP #$06                ; \ ...blue
                    BEQ SET_BLUE_TIMER      ; /
                    BRA RETURN2

SET_GREY_TIMER:      LDA #!GREY_TIMER         ; \ set grey P-switch timer
		    STA $14AE               ; /
		    JSL $02B9BD             ; change on screen sprites into silver coins routine
                    BRA RETURN2

SET_BLUE_TIMER:	    LDA #!BLUE_TIMER         ; \ set blue P-switch timer
		    STA $14AD               ; /
                    BRA RETURN2

TURN_INTO_A_COIN:    JSL $00FA80             ; change on screen sprites into a coin routine
                    BRA RETURN2

QUAKE:               LDA #!QUAKE_TIMER2       ; \ shake GROUND
                    STA $1887               ; /
                    LDA #!SOUND_TO_GEN2      ; \ play sound effect
                    STA $1DFC               ; /
                    JSL $0294C1             ; earthquake routine
RETURN2:             RTS                     ; RETURN

OFFSET:		    db $01,$00,$FF,$FF
		        
SET_CARRIED:         BIT $15		    ; \ don't pick up sprite if not pressing button
                    BVC NO_MARIO_X_SPEED    ; /
                    LDA $1470               ; \ if carrying an enemy...
                    ORA $187A               ; / ...or on Yoshi
                    BNE NO_MARIO_X_SPEED    ; don't carry
                    LDA !EXTRA_PROP_1,x      ; \ don't carry if the extra property 1 is set...
	            AND #$01                ;  |
	            BNE NO_MARIO_X_SPEED    ; / 
	            LDA #$0B		    ; \ sprite status = Carried
	            STA $14C8,x             ; /

NO_MARIO_X_SPEED:    STZ $7B                 ; no Mario x speed
		    JSR SUB_HORZ_POS        ; horizontal Mario/sprite check routine
		    TYA                     ;
		    ASL A                   ;
		    TAY                     ;
		    REP #$20                ; Accum (16 bit)
		    LDA $94                 ; \ set Mario's x position
		    CLC                     ;  |
		    ADC OFFSET,y            ;  |
		    STA $94                 ; /
		    SEP #$20                ; Accum (8 bit) 
	            RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GROUND speed routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


Y_SPEED_TABLE:       db $00,$00,$00,$F8,$F8,$F8,$F8,$F8
                    db $F8,$F7,$F6,$F5,$F4,$F3,$F2,$E8
                    db $E8,$E8,$E8,$00


GROUND_SPEED:        LDA $B6,x               ;
                    PHP                     ; push processor
                    BPL SKIP_FLIP           ;
                    EOR #$FF		    ; \ set A to -A
                    INC A                   ; /  
SKIP_FLIP:           LSR                     ;
                    PLP                     ; pull processor
                    BPL STORE_X_SPEED       ;
                    EOR #$FF		    ; \ set A to -A
                    INC A                   ; /  
STORE_X_SPEED:       STA $B6,x               ;
                    LDA $AA,x               ;
                    PHA                     ;
                    JSR SET_SOME_Y_SPEED    ; set y speed
                    PLA                     ;
                    LSR                     ;
                    LSR                     ;
                    TAY                     ;
                    LDA Y_SPEED_TABLE,y     ; y speed table
                    LDY $1588,x             ; \ if touching an object...
                    BMI SET_RETURN          ; /
                    STA $AA,x               ; store y speed
SET_RETURN:          RTS                     ; RETURN

SET_SOME_Y_SPEED:    LDA $1588,x             ; \ if touching an object...
                    BMI SET_THE_Y_SPEED     ; /
                    LDA #$00                ; \ sprite Y speed = #$00 or #$18
                    LDY $15B8,x             ;  | depending on 15B8,x ???
                    BEQ STORE_Y_SPEED	    ;  | 
SET_THE_Y_SPEED:     LDA #$18                ;  | 
STORE_Y_SPEED:       STA $AA,x		    ; / 
                    RTS			    ; RETURN 
	
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; display smoke effect
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SUB_SMOKE:           LDY #$03                ; \ find a free slot to display effect
FINDFREE:            LDA $17C0,y             ;  |
                    BEQ FOUNDONE            ;  |
                    DEY                     ;  |
                    BPL FINDFREE            ;  |
                    RTS                     ; / RETURN if no slots open

FOUNDONE:            LDA #$01                ; \ set effect graphic to smoke graphic
                    STA $17C0,y             ; /
                    LDA #$1B                ; \ set time to show smoke
                    STA $17CC,y             ; /
                    LDA $D8,x               ; \ smoke y position = generator y position
                    STA $17C4,y             ; /
                    LDA $E4,x               ; \ load generator x position and store it for later
                    STA $17C8,y             ; /
                    RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; spawn sprite routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SPAWN_SPRITE:        LDA !EXTRA_BITS,x        ; \ generate a CUSTOM sprite if the extra bit is set
                    AND #$04                ;  |
                    BNE CUSTOM              ; /

NORMAL:		    LDA #$08                ; \ set sprite status for new sprite
                    STA $14C8,x             ; /

                    LDA #!SPRITE_TO_GEN      ; \ sprite type
		    STA $9E,x               ; /
		    JSL $07F7D2             ; reset sprite PROPERTIES
                    BRA SHARED

CUSTOM:              LDA #$08                ; \ set sprite status for new sprite
                    STA $14C8,x             ; /

                    LDA #!CUST_SPRITE_TO_GEN ; \ sprite type
                    STA !NEW_SPRITE_NUM,x    ; /
                    JSL $07F7D2             ; reset sprite PROPERTIES
                    JSL $0187A7             ; get table values for CUSTOM sprite
                    LDA #$88                ; \ mark as initialized
                    STA !EXTRA_BITS,x        ; /
  
SHARED:		    LDA #$20                ; \ time to disable sprite's contact with Mario
		    STA $154C,x             ; /

		    LDA $D8,x               ; \ set y position (low byte)
		    SEC                     ;  |
		    SBC #$0F                ;  |
		    STA $D8,x               ; /
		    LDA $14D4,x             ; \ set y position (high byte)
		    SBC #$00                ;  |
		    STA $14D4,x             ; /

		    LDA #$00                ; \ set sprite DIRECTION depending on Mario's x speed
		    LDY $7B                 ;  | 
		    BPL DIRECTION           ;  |
		    INC A                   ;  |
DIRECTION:	    STA $157C,x             ; /

		    LDA #$C0                ; \ set y speed
		    STA $AA,x               ; /

		    LDA #!SOUND_TO_GEN3      ; \ sound effect
		    STA $1DFC               ; /
		    RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; spawn block routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SPAWN_BLOCK:         LDA #!SOUND_TO_GEN4      ; \ sound effect
                    STA $1DF9               ; /

                    LDA !EXTRA_BITS,x        ; \ generate other block if the extra bit is set
                    AND #$04                ;  |
                    BNE BLOCK2              ; /

                    LDA $E4,x               ; \ setup block PROPERTIES
                    STA $9A                 ;  |
                    LDA $14E0,x             ;  |
                    STA $9B                 ;  |
                    LDA $D8,x               ;  |
		    SEC                     ;  |
		    SBC #$30                ;  | set higher y position
                    STA $98                 ;  |
                    LDA $14D4,x             ;  |
                    STA $99                 ; /
                  
BLOCK1:              PHP
                    REP #$30                ; \ change sprite to block 
                    LDA.w #!BLOCK_TILE1      ;  |
                    STA $03                 ;  |
                    JSR SUBL_SET_MAP16      ;  |
                    PLP                     ; / 
                    BRA END_BLOCK

BLOCK2:              LDA $E4,x               ; \ setup block PROPERTIES
                    STA $9A                 ;  |
                    LDA $14E0,x             ;  |
                    STA $9B                 ;  |
                    LDA $D8,x               ;  |
		    SEC                     ;  |
		    SBC #$30                ;  | set higher y position
                    STA $98                 ;  |
                    LDA $14D4,x             ;  |
                    STA $99                 ; /

                    PHP
                    REP #$30                ; \ change sprite to block 
                    LDA.w #!BLOCK_TILE2      ;  |
                    STA $03                 ;  |
                    JSR SUBL_SET_MAP16      ;  |
                    PLP                     ; / 
END_BLOCK:           RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; map16 subroutine
; doesn't work with mario allstars
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


SUBL_SET_MAP16:      PHP                     ;A:0266 X:0007 Y:0001 D:0000 DB:01 S:01EE P:envmxdizcHC:1306 VC:149 00 FL:1681
                    REP #$30                ;A:0266 X:0007 Y:0001 D:0000 DB:01 S:01ED P:envmxdizcHC:1328 VC:149 00 FL:1681
                    PHY                     ;A:0266 X:0007 Y:0001 D:0000 DB:01 S:01ED P:envmxdizcHC:1350 VC:149 00 FL:1681
                    PHX                     ;A:0266 X:0007 Y:0001 D:0000 DB:01 S:01EB P:envmxdizcHC:0012 VC:150 00 FL:1681
                    TAX                     ;A:0266 X:0007 Y:0001 D:0000 DB:01 S:01E9 P:envmxdizcHC:0042 VC:150 00 FL:1681
                    LDA $03                 ;A:0266 X:0266 Y:0001 D:0000 DB:01 S:01E9 P:envmxdizcHC:0056 VC:150 00 FL:1681
                    PHA                     ;A:0266 X:0266 Y:0001 D:0000 DB:01 S:01E9 P:envmxdizcHC:0088 VC:150 00 FL:1681
                    JSR SUB_8034            ;A:0266 X:0266 Y:0001 D:0000 DB:01 S:01E7 P:envmxdizcHC:0118 VC:150 00 FL:1681
                    PLA                     ;A:0010 X:0000 Y:0006 D:0000 DB:00 S:01DC P:envmxdizCHC:0726 VC:032 00 FL:11805
                    STA $03                 ;A:02A8 X:0000 Y:0006 D:0000 DB:00 S:01DE P:envmxdizCHC:0762 VC:032 00 FL:11805
                    PLX                     ;A:02A8 X:0000 Y:0006 D:0000 DB:00 S:01DE P:envmxdizCHC:0794 VC:032 00 FL:11805
                    PLY                     ;A:02A8 X:0020 Y:0006 D:0000 DB:00 S:01E0 P:envmxdizCHC:0830 VC:032 00 FL:11805
                    PLP                     ;A:02A8 X:0020 Y:0001 D:0000 DB:00 S:01E2 P:envmxdizCHC:0866 VC:032 00 FL:11805
                    RTS                     ;A:02A8 X:0020 Y:0001 D:0000 DB:00 S:01E3 P:envmxdizCHC:0894 VC:032 00 FL:11805

                    JMP $FEA301
RETURN18:            PLX
                    PLB
                    PLP
                    RTS

SUB_8034:            PHP                     ;A:0266 X:0266 Y:0001 D:0000 DB:01 S:01E5 P:envmxdizcHC:0164 VC:150 00 FL:1682
                    SEP #$20                ;A:0266 X:0266 Y:0001 D:0000 DB:01 S:01E4 P:envmxdizcHC:0186 VC:150 00 FL:1682
                    PHB                     ;A:0266 X:0266 Y:0001 D:0000 DB:01 S:01E4 P:envMxdizcHC:0208 VC:150 00 FL:1682
                    LDA #$00                ;A:0200 X:0266 Y:0001 D:0000 DB:01 S:01E3 P:envMxdiZcHC:0286 VC:150 00 FL:1682
                    PHA                     ;A:0200 X:0266 Y:0001 D:0000 DB:01 S:01E3 P:envMxdiZcHC:0324 VC:150 00 FL:1682
                    PLB                     ;A:0200 X:0266 Y:0001 D:0000 DB:01 S:01E2 P:envMxdiZcHC:0346 VC:150 00 FL:1682
                    REP #$30                ;A:0200 X:0266 Y:0001 D:0000 DB:00 S:01E3 P:envMxdiZcHC:0374 VC:150 00 FL:1682
                    PHX                     ;A:0200 X:0266 Y:0001 D:0000 DB:00 S:01E3 P:envmxdiZcHC:0396 VC:150 00 FL:1682
                    LDA $9A                 ;A:0200 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envmxdiZcHC:0426 VC:150 00 FL:1682
                    STA $0C                 ;A:0070 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envmxdizcHC:0458 VC:150 00 FL:1682
                    LDA $98                 ;A:0070 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envmxdizcHC:0490 VC:150 00 FL:1682
                    STA $0E                 ;A:0130 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envmxdizcHC:0522 VC:150 00 FL:1682
                    LDA.w #$0000            ;A:0130 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envmxdizcHC:0554 VC:150 00 FL:1682
                    SEP #$20                ;A:0000 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envmxdiZcHC:0578 VC:150 00 FL:1682
                    LDA $5B                 ;A:0000 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0600 VC:150 00 FL:1682
                    STA $09                 ;A:0000 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0624 VC:150 00 FL:1682
                    LDA $1933               ;A:0000 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0648 VC:150 00 FL:1682
                    BEQ NO_SHIFT            ;A:0000 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0680 VC:150 00 FL:1682
                    LSR $09
NO_SHIFT:            LDY $0E                 ;A:0000 X:0266 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0702 VC:150 00 FL:1682
                    LDA $09                 ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:envMxdizcHC:0734 VC:150 00 FL:1682
                    AND #$01                ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0758 VC:150 00 FL:1682
                    BEQ HORIZ               ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0774 VC:150 00 FL:1682
                    LDA $9B
                    STA $00
                    LDA $99
                    STA $9B
                    LDA $00  
                    STA $99
                    LDY $0C
HORIZ:               CPY.w #$0200            ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0796 VC:150 00 FL:1682
                    BCS RETURN18            ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0820 VC:150 00 FL:1682
                    LDA $1933               ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0836 VC:150 00 FL:1682
                    ASL A                   ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0868 VC:150 00 FL:1682
                    TAX                     ;A:0000 X:0266 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0882 VC:150 00 FL:1682
                    LDA $BEA8,x ;[$00:BEA8] ;A:0000 X:0000 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0896 VC:150 00 FL:1682
                    STA $65                 ;A:00A8 X:0000 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0928 VC:150 00 FL:1682
                    LDA $BEA9,x ;[$00:BEA9] ;A:00A8 X:0000 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0952 VC:150 00 FL:1682
                    STA $66                 ;A:00BD X:0000 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0984 VC:150 00 FL:1682
                    STZ $67                 ;A:00BD X:0000 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1008 VC:150 00 FL:1682
                    LDA $1925               ;A:00BD X:0000 Y:0130 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1032 VC:150 00 FL:1682
                    ASL A                   ;A:0000 X:0000 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1064 VC:150 00 FL:1682
                    TAY                     ;A:0000 X:0000 Y:0130 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1078 VC:150 00 FL:1682
                    LDA ($65),y ;[$00:BDA8] ;A:0000 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1092 VC:150 00 FL:1682
                    STA $04                 ;A:00D8 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1132 VC:150 00 FL:1682
                    INY                     ;A:00D8 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1156 VC:150 00 FL:1682
                    LDA ($65),y ;[$00:BDA9] ;A:00D8 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:1170 VC:150 00 FL:1682
                    STA $05                 ;A:00BA X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1210 VC:150 00 FL:1682
                    STZ $06                 ;A:00BA X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1234 VC:150 00 FL:1682
                    LDA $9B                 ;A:00BA X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:1258 VC:150 00 FL:1682
                    STA $07                 ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1282 VC:150 00 FL:1682
                    ASL A                   ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1306 VC:150 00 FL:1682
                    CLC                     ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1320 VC:150 00 FL:1682
                    ADC $07                 ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1334 VC:150 00 FL:1682
                    TAY                     ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:1358 VC:150 00 FL:1682
                    LDA ($04),y ;[$00:BAD8] ;A:0000 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0004 VC:151 00 FL:1682
                    STA $6B                 ;A:0000 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0044 VC:151 00 FL:1682
                    STA $6E                 ;A:0000 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0068 VC:151 00 FL:1682
                    INY                     ;A:0000 X:0000 Y:0000 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0092 VC:151 00 FL:1682
                    LDA ($04),y ;[$00:BAD9] ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0106 VC:151 00 FL:1682
                    STA $6C                 ;A:00C8 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0146 VC:151 00 FL:1682
                    STA $6F                 ;A:00C8 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0170 VC:151 00 FL:1682
                    LDA #$7E                ;A:00C8 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0194 VC:151 00 FL:1682
                    STA $6D                 ;A:007E X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0210 VC:151 00 FL:1682
                    INC A                   ;A:007E X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0234 VC:151 00 FL:1682
                    STA $70                 ;A:007F X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0248 VC:151 00 FL:1682
                    LDA $09                 ;A:007F X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0272 VC:151 00 FL:1682
                    AND #$01                ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0296 VC:151 00 FL:1682
                    BEQ NO_AND              ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0312 VC:151 00 FL:1682
                    LDA $99
                    LSR A
                    LDA $9B 
                    AND #$01
                    BRA LABEL52
NO_AND:              LDA $9B                 ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0334 VC:151 00 FL:1682
                    LSR A                   ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0358 VC:151 00 FL:1682
                    LDA $99                 ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZcHC:0372 VC:151 00 FL:1682
LABEL52:             ROL A                   ;A:0001 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0396 VC:151 00 FL:1682
                    ASL A                   ;A:0002 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0410 VC:151 00 FL:1682
                    ASL A                   ;A:0004 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0424 VC:151 00 FL:1682
                    ORA #$20                ;A:0008 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0438 VC:151 00 FL:1682
                    STA $04                 ;A:0028 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0454 VC:151 00 FL:1682
                    CPX.w #$0000            ;A:0028 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0478 VC:151 00 FL:1682
                    BEQ NO_ADD              ;A:0028 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZCHC:0502 VC:151 00 FL:1682
                    CLC
                    ADC #$10 
                    STA $04
NO_ADD:              LDA $98                 ;A:0028 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZCHC:0524 VC:151 00 FL:1682
                    AND #$F0                ;A:0030 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizCHC:0548 VC:151 00 FL:1682
                    CLC                     ;A:0030 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizCHC:0564 VC:151 00 FL:1682
                    ASL A                   ;A:0030 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0578 VC:151 00 FL:1682
                    ROL A                   ;A:0060 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0592 VC:151 00 FL:1682
                    STA $05                 ;A:00C0 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0606 VC:151 00 FL:1682
                    ROL A                   ;A:00C0 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0630 VC:151 00 FL:1682
                    AND #$03                ;A:0080 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizCHC:0644 VC:151 00 FL:1682
                    ORA $04                 ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdiZCHC:0660 VC:151 00 FL:1682
                    STA $06                 ;A:0028 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizCHC:0684 VC:151 00 FL:1682
                    LDA $9A                 ;A:0028 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizCHC:0708 VC:151 00 FL:1682
                    AND #$F0                ;A:0070 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizCHC:0732 VC:151 00 FL:1682
                    LSR A                   ;A:0070 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizCHC:0748 VC:151 00 FL:1682
                    LSR A                   ;A:0038 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0762 VC:151 00 FL:1682
                    LSR A                   ;A:001C X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0776 VC:151 00 FL:1682
                    STA $04                 ;A:000E X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0790 VC:151 00 FL:1682
                    LDA $05                 ;A:000E X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envMxdizcHC:0814 VC:151 00 FL:1682
                    AND #$C0                ;A:00C0 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0838 VC:151 00 FL:1682
                    ORA $04                 ;A:00C0 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0854 VC:151 00 FL:1682
                    STA $07                 ;A:00CE X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0878 VC:151 00 FL:1682
                    REP #$20                ;A:00CE X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvMxdizcHC:0902 VC:151 00 FL:1682
                    LDA $09                 ;A:00CE X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvmxdizcHC:0924 VC:151 00 FL:1682
                    AND.w #$0001            ;A:0100 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envmxdizcHC:0956 VC:151 00 FL:1682
                    BNE LABEL51             ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envmxdiZcHC:0980 VC:151 00 FL:1682
                    LDA $1A                 ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envmxdiZcHC:0996 VC:151 00 FL:1682
                    SEC                     ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envmxdiZcHC:1028 VC:151 00 FL:1682
                    SBC.w #$0080            ;A:0000 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:envmxdiZCHC:1042 VC:151 00 FL:1682
                    TAX                     ;A:FF80 X:0000 Y:0001 D:0000 DB:00 S:01E1 P:eNvmxdizcHC:1066 VC:151 00 FL:1682
                    LDY $1C                 ;A:FF80 X:FF80 Y:0001 D:0000 DB:00 S:01E1 P:eNvmxdizcHC:1080 VC:151 00 FL:1682
                    LDA $1933               ;A:FF80 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:1112 VC:151 00 FL:1682
                    BEQ LABEL50             ;A:0000 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdiZcHC:1152 VC:151 00 FL:1682
                    LDX $1E
                    LDA $20
                    SEC
                    SBC.w #$0080
                    TAY
                    BRA LABEL50
LABEL51:             LDX $1A
                    LDA $1C
                    SEC
                    SBC.w #$0080
                    TAY
                    LDA $1933
                    BEQ LABEL50
                    LDA $1E
                    SEC
                    SBC.w #$0080
                    TAX  
                    LDY $20
LABEL50:             STX $08                 ;A:0000 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdiZcHC:1174 VC:151 00 FL:1682
                    STY $0A                 ;A:0000 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdiZcHC:1206 VC:151 00 FL:1682
                    LDA $98                 ;A:0000 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdiZcHC:1238 VC:151 00 FL:1682
                    AND.w #$01F0            ;A:0130 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:1270 VC:151 00 FL:1682
                    STA $04                 ;A:0130 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:1294 VC:151 00 FL:1682
                    LDA $9A                 ;A:0130 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:1326 VC:151 00 FL:1682
                    LSR A                   ;A:0070 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:1358 VC:151 00 FL:1682
                    LSR A                   ;A:0038 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:0004 VC:152 00 FL:1682
                    LSR A                   ;A:001C X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:0018 VC:152 00 FL:1682
                    LSR A                   ;A:000E X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:0032 VC:152 00 FL:1682
                    AND.w #$000F            ;A:0007 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:0046 VC:152 00 FL:1682
                    ORA $04                 ;A:0007 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:0070 VC:152 00 FL:1682
                    TAY                     ;A:0137 X:FF80 Y:00C0 D:0000 DB:00 S:01E1 P:envmxdizcHC:0102 VC:152 00 FL:1682
                    PLA                     ;A:0137 X:FF80 Y:0137 D:0000 DB:00 S:01E1 P:envmxdizcHC:0116 VC:152 00 FL:1682
                    SEP #$20                ;A:0266 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envmxdizcHC:0152 VC:152 00 FL:1682
                    STA [$6B],y ;[$7E:C937] ;A:0266 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envMxdizcHC:0174 VC:152 00 FL:1682
                    XBA                     ;A:0266 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envMxdizcHC:0222 VC:152 00 FL:1682
                    STA [$6E],y ;[$7F:C937] ;A:6602 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envMxdizcHC:0242 VC:152 00 FL:1682
                    XBA                     ;A:6602 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envMxdizcHC:0290 VC:152 00 FL:1682
                    REP #$20                ;A:0266 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envMxdizcHC:0310 VC:152 00 FL:1682
                    ASL A                   ;A:0266 X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envmxdizcHC:0332 VC:152 00 FL:1682
                    TAY                     ;A:04CC X:FF80 Y:0137 D:0000 DB:00 S:01E3 P:envmxdizcHC:0346 VC:152 00 FL:1682
                    PHK                     ;A:04CC X:FF80 Y:04CC D:0000 DB:00 S:01E3 P:envmxdizcHC:0360 VC:152 00 FL:1682
                    PER.w $0006            ; NOTE: this relative counter must always point to MAP16_RETURN. 
                    PEA $804C
                    JMP.l $00C0FB
MAP16_RETURN:        PLB
                    PLP
                    RTS

                    
                    NOP
                    

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;frame 1, squashed (2 bytes each)
TILE_SIZE:           db $02,$02,$00,$00
HORZ_DISP:           db $00,$00,$00,$08
VERT_DISP:           db $00,$00,$08,$08
TILEMAP:             db $42,$42,$FE,$FE
PROPERTIES:          db $40,$00,$00,$40


SUB_GFX:             JSL $20CA4F      ; after: Y = index to sprite OAM ($300)      
                    LDA $151C,x             ; \
                    ASL A                   ;  | $03 = index to frame start (frame to show * 2 tile per frame)
                    STA $03                 ; /
                    LDA $157C,x             ; \ $02 = sprite DIRECTION
                    STA $02                 ; /
                    PHX                     ; push x

                    LDA $14C8,x             ; \ if the sprite is defeated...
                    CMP #$02                ;  |
                    BNE NO_STAR             ; /
                    LDA #$02                ; \ set squashed image
                    STA $03                 ; /

NO_STAR:             LDA $1558,x             ; \ if time to show sprite remains > 0...
                    BEQ NOT_DEAD            ; /
                    LDA #$02                ; \ set squashed image
                    STA $03                 ; /
                    STX $15E9

NOT_DEAD:            LDX #$01                ; run loop 2 times, cuz 2 tiles per frame
LOOP_START:          PHX                     ; push, current tile

                    TXA                     ; \ X = index of frame start + current tile
                    CLC                     ;  |
                    ADC $03                 ;  |
                    TAX                     ; /

                    PHY                     ; set tile to be 8x8 or 16x16
                    TYA                     ;
                    LSR A                   ;
                    LSR A                   ;
                    TAY                     ;
                    LDA TILE_SIZE,x         ; 
                    STA $0460,y             ;
                    PLY                     ;
                    
                    LDA $00                 ; \ tile x position = sprite x location ($00) + tile displacement
                    CLC                     ;  | 
                    ADC HORZ_DISP,x         ;  |
                    STA $0300,y             ; /
                    
                    LDA $01                 ; \ tile y position = sprite y location ($01) + tile displacement
                    CLC                     ;  | 
                    ADC VERT_DISP,x         ;  |
                    STA $0301,y             ; /

                    LDA TILEMAP,x           ; \ store tile
                    STA $0302,y             ; /

                    PHX                     ;
                    LDX $15E9               ;
                    LDA $15F6,x             ; get palette info
                    PLX                     ;
                    ORA PROPERTIES,x        ; flip tile if necessary
                    ORA $64                 ; add in tile priority of level
                    STA $0303,y             ; store tile PROPERTIES

                    PLX                     ; \ pull, current tile
                    INY                     ;  | increase index to sprite tile map ($300)...
                    INY                     ;  |    ...we wrote 1 16x16 tile...
                    INY                     ;  |    ...sprite OAM is 8x8...
                    INY                     ;  |    ...so increment 4 times
                    DEX                     ;  | go to next tile of frame and loop
                    BPL LOOP_START          ; /

                    PLX                     ; pull, X = sprite index
                    LDY #$FF                ; \ we've already set 460 so use FF
                    LDA #$01                ;  | A = number of tiles drawn - 1
                    JSL $01B7B3             ; / don't draw if offscreen
                    RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


HORZ_DISP_TWO:           db $00,$08,$00,$08
VERT_DISP_TWO:           db $00,$00,$08,$08
TILEMAP_TWO:             db $42,$43,$52,$53


CARRIED_GFX:         JSL $20CA4F      ; after: Y = index to sprite OAM ($300)
                    LDA $151C,x             ; \
                    ASL A                   ;  | $03 = index to frame start (frame to show * 2 tile per frame)
                    STA $03                 ; /
                    LDA $157C,x             ; \ $02 = DIRECTION
                    STA $02                 ; / 
                    PHX                     ; push x

                    LDX #$03                ; run loop 4 times, cuz 4 tiles per frame
LOOP_START_2:        PHX                     ; push, current tile

                    TXA                     ; \ X = index of frame start + current tile
                    CLC                     ;  |
                    ADC $03                 ;  |
                    TAX                     ; /

                    LDA $00                 ; \ tile x position = sprite x location ($00) + tile displacement
                    CLC                     ;  |
                    ADC HORZ_DISP,x         ;  |
                    STA $0300,y             ; /
                    
                    LDA $01                 ; \
                    CLC                     ;  | tile y position = sprite y location ($01) + tile displacement
                    ADC VERT_DISP,x         ;  |
                    STA $0301,y             ; /

                    LDA TILEMAP,x           ; \ store tile
                    STA $0302,y             ; /

                    PHX                     ;
                    LDX $15E9               ;
                    LDA $15F6,x             ; get palette info
                    PLX                     ;
                    ORA $64                 ; add in tile priority of level
                    STA $0303,y             ; store tile PROPERTIES

                    PLX                     ; \ pull, current tile
                    INY                     ;  | increase index to sprite tile map ($300)...
                    INY                     ;  |    ...we wrote 1 16x16 tile...
                    INY                     ;  |    ...sprite OAM is 8x8...
                    INY                     ;  |    ...so increment 4 times
                    DEX                     ;  | go to next tile of frame and loop
                    BPL LOOP_START_2        ; /

                    PLX                     ; pull, X = sprite index
                    LDY #$00                ; \ we've already set 460 so use FF
                    LDA #$03                ;  | A = number of tiles drawn - 1
                    JSL $01B7B3             ; / don't draw if offscreen
                    RTS                     ; RETURN 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; routines below can be SHARED by all sprites.  they are ripped from original
; SMW and poorly documented
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B829 - vertical mario/sprite position check - SHARED
; Y = 1 if mario below sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B829 

SUB_VERT_POS:        LDY #$00               ;A:25A1 X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizCHC:0130 VC:085 00 FL:924
                    LDA $96                ;A:25A1 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdiZCHC:0146 VC:085 00 FL:924
                    SEC                    ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0170 VC:085 00 FL:924
                    SBC $D8,x              ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0184 VC:085 00 FL:924
                    STA $0F                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0214 VC:085 00 FL:924
                    LDA $97                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0238 VC:085 00 FL:924
                    SBC $14D4,x            ;A:2501 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizcHC:0262 VC:085 00 FL:924
                    BPL LABEL11            ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0294 VC:085 00 FL:924
                    INY                    ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0310 VC:085 00 FL:924
LABEL11:             RTS                    ;A:25FF X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizcHC:0324 VC:085 00 FL:924


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/sprite check - SHARED
; Y = 1 if mario left of sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B817        

SUB_HORZ_POS:        LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0F                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B85D - off screen processing code - SHARED
; sprites enter at different points
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B83B             

TABLE3:              db $40,$B0
TABLE6:              db $01,$FF 
TABLE4:              db $30,$C0,$A0,$80,$A0,$40,$60,$B0 
TABLE5:              db $01,$FF,$01,$FF,$01,$00,$01,$FF

SUB_OFF_SCREEN_X0:   LDA #$06                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | 
SUB_OFF_SCREEN_X1:   LDA #$04                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X2:   LDA #$02                ;  |
STORE_03:            STA $03                 ;  |
                    BRA START_SUB           ;  |
SUB_OFF_SCREEN_X3:   STZ $03                 ; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, RETURN
                    BEQ RETURN_2            ; /    
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ;  |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ;  | 
                    ADC #$50                ;  | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ;  | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ;  | 
                    CMP #$02                ;  | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
                    AND #$04                ;  |
                    BNE RETURN_2            ; /
                    LDA $13                 ; \ 
                    AND #$01                ;  | 
                    ORA $03                 ;  | 
                    STA $01                 ;  |
                    TAY                     ; /
                    LDA $1A                 ;x boundry ;A:0101 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0256 VC:090 00 FL:16953
                    CLC                     ;A:0100 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdiZcHC:0280 VC:090 00 FL:16953
                    ADC TABLE4,y            ;A:0100 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdiZcHC:0294 VC:090 00 FL:16953
                    ROL $00                 ;A:01C0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizcHC:0326 VC:090 00 FL:16953
                    CMP $E4,x               ;x pos ;A:01C0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizcHC:0364 VC:090 00 FL:16953
                    PHP                     ;A:01C0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:0394 VC:090 00 FL:16953
                    LDA $1B                 ;x boundry hi ;A:01C0 X:0006 Y:0001 D:0000 DB:03 S:01EC P:eNvMXdizCHC:0416 VC:090 00 FL:16953
                    LSR $00                 ;A:0100 X:0006 Y:0001 D:0000 DB:03 S:01EC P:envMXdiZCHC:0440 VC:090 00 FL:16953
                    ADC TABLE5,y            ;A:0100 X:0006 Y:0001 D:0000 DB:03 S:01EC P:envMXdizcHC:0478 VC:090 00 FL:16953
                    PLP                     ;A:01FF X:0006 Y:0001 D:0000 DB:03 S:01EC P:eNvMXdizcHC:0510 VC:090 00 FL:16953
                    SBC $14E0,x             ;x pos high ;A:01FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:0538 VC:090 00 FL:16953
                    STA $00                 ;A:01FE X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:0570 VC:090 00 FL:16953
                    LSR $01                 ;A:01FE X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:0594 VC:090 00 FL:16953
                    BCC LABEL20             ;A:01FE X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdiZCHC:0632 VC:090 00 FL:16953
                    EOR #$80                ;A:01FE X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdiZCHC:0648 VC:090 00 FL:16953
                    STA $00                 ;A:017E X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizCHC:0664 VC:090 00 FL:16953
LABEL20:             LDA $00                 ;A:017E X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizCHC:0688 VC:090 00 FL:16953
                    BPL RETURN_2            ;A:017E X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizCHC:0712 VC:090 00 FL:16953
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ;  |
                    BCC KILL_SPRITE         ; /
                    LDY $161A,x             ;A:FF08 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdiZCHC:0140 VC:071 00 FL:21152
                    CPY #$FF                ;A:FF08 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizCHC:0172 VC:071 00 FL:21152
                    BEQ KILL_SPRITE         ;A:FF08 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0188 VC:071 00 FL:21152
                    LDA #$00                ; \ mark sprite to come back    A:FF08 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0204 VC:071 00 FL:21152
                    STA $1938,y             ; /                             A:FF00 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdiZcHC:0220 VC:071 00 FL:21152
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_2:            RTS                     ; RETURN

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
                    AND #$04                ;  |
                    BNE RETURN_2            ; /
                    LDA $13                 ; \ only handle every other frame??
                    LSR A                   ;  | 
                    BCS RETURN_2            ; /
                    AND #$01                ;A:0227 X:0006 Y:00EC D:0000 DB:03 S:01ED P:envMXdizcHC:0228 VC:112 00 FL:1142
                    STA $01                 ;A:0201 X:0006 Y:00EC D:0000 DB:03 S:01ED P:envMXdizcHC:0244 VC:112 00 FL:1142
                    TAY                     ;A:0201 X:0006 Y:00EC D:0000 DB:03 S:01ED P:envMXdizcHC:0268 VC:112 00 FL:1142
                    LDA $1C                 ;A:0201 X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0282 VC:112 00 FL:1142
                    CLC                     ;A:02BD X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizcHC:0306 VC:112 00 FL:1142
                    ADC TABLE3,y            ;A:02BD X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizcHC:0320 VC:112 00 FL:1142
                    ROL $00                 ;A:026D X:0006 Y:0001 D:0000 DB:03 S:01ED P:enVMXdizCHC:0352 VC:112 00 FL:1142
                    CMP $D8,x               ;A:026D X:0006 Y:0001 D:0000 DB:03 S:01ED P:enVMXdizCHC:0390 VC:112 00 FL:1142
                    PHP                     ;A:026D X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNVMXdizcHC:0420 VC:112 00 FL:1142
                    LDA.w $001D             ;A:026D X:0006 Y:0001 D:0000 DB:03 S:01EC P:eNVMXdizcHC:0442 VC:112 00 FL:1142
                    LSR $00                 ;A:0200 X:0006 Y:0001 D:0000 DB:03 S:01EC P:enVMXdiZcHC:0474 VC:112 00 FL:1142
                    ADC TABLE6,y            ;A:0200 X:0006 Y:0001 D:0000 DB:03 S:01EC P:enVMXdizCHC:0512 VC:112 00 FL:1142
                    PLP                     ;A:0200 X:0006 Y:0001 D:0000 DB:03 S:01EC P:envMXdiZCHC:0544 VC:112 00 FL:1142
                    SBC $14D4,x             ;A:0200 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNVMXdizcHC:0572 VC:112 00 FL:1142
                    STA $00                 ;A:02FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizcHC:0604 VC:112 00 FL:1142
                    LDY $01                 ;A:02FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizcHC:0628 VC:112 00 FL:1142
                    BEQ LABEL22             ;A:02FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0652 VC:112 00 FL:1142
                    EOR #$80                ;A:02FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0668 VC:112 00 FL:1142
                    STA $00                 ;A:027F X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0684 VC:112 00 FL:1142
LABEL22:             LDA $00                 ;A:027F X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0708 VC:112 00 FL:1142
                    BPL RETURN_2            ;A:027F X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:0732 VC:112 00 FL:1142
                    BMI ERASE_SPRITE        ;A:0280 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:0170 VC:064 00 FL:1195

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ;  |  
                    RTS                     ; / RETURN
