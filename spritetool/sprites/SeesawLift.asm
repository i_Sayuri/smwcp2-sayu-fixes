;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; シーソーリフト
;
; SMB3のシーソーリフト
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Uses first extra bit: NO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		!JSL_i =	$2CC400
		!SubOffScreen =	!JSL_i
		!GetSin =	$87F7DB
		!GetCos =	$87F7DB
		!Torque =	$38
		!MaxSpeed =	$06
		!StopAngle =	$40

		print "INIT ",pc
InitCode:	STZ $1510,x
		LDA $E4,x
		ORA #$04
		STA $E4,x
		LDA $D8,x
		ORA #$04
		STA $D8,x
		LDA $7FAB28,x
		INC A
		ASL A
		STA $187B,x
		RTL

		print "MAIN ",pc
		PHB
		PHK
		PLB
		JSR MainCode
		PLB
		RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MainCode:	JSR GetDrawInfo		; 画面外判定で2回RTSしない設計
		LDA $9D
		BNE .Start
		LDA #$00
		JSL !SubOffScreen
		LDA $1504,x
		ORA $1570,x
		BEQ .L40
		STZ $00
		LDY #$30
		LDA $1504,x
		STA $01
		BMI .IfMinus
		LDY #$D0
		DEC $00
.IfMinus	TYA
		CLC
		ADC $1570,x
		STA $1570,x
		LDA $00
		ADC $1504,x
		STA $1504,x
		EOR $01
		BPL .L40
		STZ $1504,x
		STZ $1570,x
.L40		LDA $1570,x
		CLC
		ADC $157C,x
		STA $157C,x
		LDY #$00
		LDA $1504,x
		BPL .IfPlus
		DEY
.IfPlus		ADC $151C,x
		STA $151C,x
		TYA
		ADC $1528,x
		AND #$01
		STA $1528,x
		LSR A
		LDA $151C,x
		BCC .L30
		EOR #$FF
		INC A
.L30		CMP #!StopAngle
		BCC .Start
		LDA #!StopAngle
.L31		LDY $1528,x
		BEQ .L32
		EOR #$FF
		INC A
.L32		STA $151C,x
.Start		LDA $1528,x
		XBA
		LDA $151C,x
		REP #$31
		STA $00
		ADC.w #$0080
		AND.w #$01FF
		TAX
		AND.w #$00FF
		ASL A
		CPX.w #$0100
		TAX
		LDA.l !GetCos,x
		BCC .IfPlus0
		EOR.w #$FFFF
		INC A
		BEQ .IfPlus0
.IfPlus0	ASL #3
		STA $02
		STA $9A
		LDA $00
		TAX
		AND.w #$00FF
		ASL A
		CPX.w #$0100
		TAX
		LDA.l !GetSin,x
		BCC .IfPlus1
		EOR.w #$FFFF
		INC A
.IfPlus1	ASL #3
		STA $00
		STA $98
		SEP #$10
		LDX $15E9
		TDC
		LDA $187B,x
		LSR A
		DEC A
		TAY
		STY $0D
		TDC
.Loop0		CLC
		ADC $00
		DEY
		BPL .Loop0
		LDY #$00
		EOR.w #$FFFF
		INC A
		BPL .IfPlus2
		DEY
.IfPlus2	STA $04
		STY $06
		LDY $0D
		TDC
.Loop1		CLC
		ADC $02
		DEY
		BPL .Loop1
		LDY #$00
		EOR.w #$FFFF
		INC A
		BPL .IfPlus3
		DEY
.IfPlus3	STA $07
		STY $09
		SEP #$20
		LDA $E4,x
		STA $0A
		LDA $14E0,x
		STA $0B
		LDA $D8,x
		STA $0C
		LDA $14D4,x
		STA $0D
		LDY $187B,x
		REP #$20
		BRA .InitLoop
.Loop2		LDX #$00
		LDA $04
		CLC
		ADC $00
		STA $04
		BPL .L10
		DEX
.L10		STX $06
		LDX #$00
		LDA $07
		CLC
		ADC $02
		STA $07
		BPL .L11
		DEX
.L11		STX $09
.InitLoop	LDA $0A
		CLC
		ADC $08
		PHA
		LDA $0C
		CLC
		ADC $05
		PHA
		DEY
		BPL .Loop2
		SEP #$20
		LDX $15E9
		LDA $187B,x
		STA $0D
		JSR SubGFX

		LDA $187B,x
		STA $0D
		STZ $0E

		LDA $71
		ORA $9D
		ORA $13F9
		ORA $1FE2,x
		BEQ .SetTorque
		JMP PullPosData

.SetTorque	LDA #!Torque
		STA $4202
		LDA $187B,x
		LSR A
		STA $4203
		REP #$20
		LDA $4216
		STA $4D
		SEP #$20

.Loop3		LDA $94
		SEC
		SBC $03,s
		CLC
		ADC #$18
		CMP #$30
		BCS .Next0
		LDA $96
		ADC #$30
		SEC
		SBC $01,s
		CMP #$60
		BCS .Next0
		PLA
		PLY
		ADC #$01
		STA $05
		BCC .L00
		INY
		CLC
.L00		STY $0B
		PLA
		PLY
		ADC #$01
		STA $04
		BCC .L01
		INY
.L01		STY $0A
		LDA #$06
		STA $06
		STA $07
		JSL $03B664
		JSL $03B72B
		BCC .Next1
		JSR SetMarioSpeed
		BRA .Next1
.Next0		PLA
		PLA
		PLA
		PLA
.Next1		LDA $4D
		SEC
		SBC #!Torque
		STA $4D
		BCS .L50
		DEC $4E
.L50		DEC $0D
		BPL .Loop3

.L52		LDA $1FD6,x
		INC A
		ORA $0E
		LSR A
		BCC .L53
		LDA $7B
		SEC
		SBC $1510,x
		STA $7B
		BRA .L51
.L53		;LDA #$04
		;STA $1FE2,x
		LDA $7D
		BMI .L51
		CLC
		ADC #$10
		STA $7D
.L51		STZ $1510,x
		LDA $0E
		STA $1FD6,x
Return:		RTS

PullPosData:	PLA
		PLA
		PLA
		PLA
		DEC $0D
		BPL PullPosData
		JMP MainCode_L52

SetMarioSpeed:	LDA $05
		SEC
		SBC $1C
		STA $00
		LDA $80
		CLC
		ADC #$18
		CMP $00
		BMI .L08
		JMP .L00
.L08		LDA $7D
		BMI Return
		LDA $77
		AND #$08
		BNE Return
		LDA #$10
		STA $7D
		LDA #$03
		STA $1471
		LDA #$1F
		LDY $187A
		BEQ .NoYoshi
		LDA #$2F
.NoYoshi	STA $01
		LDA $05
		SEC
		SBC $01
		STA $96
		LDA $0B
		SBC #$00
		STA $97
		LDA $0E
		BNE .Return
		INC $0E
		;LDA $FFFFFF
		LDA $7B
		ASL A
		TDC
		ROL A
		INC A
		AND $77
		BNE .SetAngle
		LDA $9B
		ASL A
		REP #$20
		LDA $98
		BCC .IfPlus
		EOR.w #$FFFF
		INC A
.IfPlus		STA $00
		SEP #$20
		LDA $01
		EOR #$80
		ASL A
		TDC
		ROL A
		INC A
		STA $00
		LDA $76
		EOR #$01
		INC A
		AND $00
		AND $15
		STA $0660,x
		CMP #$01
		LDA $01
		BCC .Skip
		STA $00
		ASL A
		ADC $00
.Skip		STA $00
		ADC $7B
		STA $7B

		LDA $00
		ASL $00
		ROR A
		STA $1510,x
.SetAngle	LDA $1570,x
		CLC
		ADC $4D
		STA $1570,x
		LDA $1504,x
		ADC $4E
		STA $1504,x
		STA $00
		BPL .IfPlus0
		EOR #$FF
		INC A
.IfPlus0	CMP #!MaxSpeed
		BCC .Return
		LDA #!MaxSpeed
		ASL $00
		BCC .Store
		EOR #$FF
		INC A
.Store		STA $1504,x
.Return		RTS

.L00		LDA $190F,x
		LSR A
		BCS .Return
		LDA #$00
		LDY $73
		BNE .Ducking
		LDY $19
		BNE .PowerUp
.Ducking	LDA #$10
.PowerUp	LDY $187A
		BEQ .NoYoshi0
		ADC #$08
.NoYoshi0	CLC
		ADC $80
		CMP $00
		BMI .Return0
		LDA $7D
		BPL .Return0
		LDA #$10
		STA $7D
		LDA #$01
		STA $1DF9
		LDA $4D
		ORA $4E
		BEQ .Return0
		LDA $0E
		BNE .Return0
		INC $0E
		REP #$20
		LDA $4D
		ASL A
		ASL A
		STA $00
		SEP #$20
		LDA $1570,x
		SEC
		SBC $00
		STA $1570,x
		LDA $1504,x
		SBC $01
		STA $1504,x
		STA $00
		BPL .IfPlus1
		EOR #$FF
		INC A
.IfPlus1	CMP #!MaxSpeed
		BCC .Return0
		LDA #!MaxSpeed
		ASL $00
		BCC .Store0
		EOR #$FF
		INC A
.Store0		STA $1504,x
.Return0	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SubGFX:		LDA $15C4,x
		BNE .Return
		LDA #$03
		XBA
		LDA $0D
		ASL A
		ASL A
		ORA #$03
		ADC $15EA,x
		REP #$30
		TSX
		STX $010E
		TCS
		SEP #$20
		LDY.w #$283D
.Loop		PHY
		LDA $03,x
		INX #2
		SEC
		SBC $1C
		PHA
		LDA $03,x
		INX #2
		SEC
		SBC $1A
		PHA
		DEC $0D
		BPL .Loop
		LDX $010E
		TXS
		SEP #$10
		LDX $15E9
		LDA $187B,x
		LSR A
		ASL A
		ASL A
		ADC $15EA,x
		TAY
		LDA #$2A
		STA $0303,y
		LDY #$00
		LDA $187B,x
		JSL $01B7B3
.Return		RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TABLE1:		db $0C,$1C
TABLE2:		db $01,$02
RangeTable:	dw $0040,$0080
RangeTable2:	dw $0180,$0200

GetDrawInfo:	STZ $186C,x
		STZ $15A0,x
		LDA $E4,x
		CMP $1A
		LDA $14E0,x
		SBC $1B
		BEQ ON_SCREEN_X
		INC $15A0,x

ON_SCREEN_X:	LDY #$00
		LDA $187B,x
		CMP #$04
		BCC DrawLabel0
		LDY #$02
DrawLabel0:	LDA $14E0,x
		XBA
		LDA $E4,x
		REP #$21
		ADC RangeTable,y
		SEC
		SBC $1A
		CMP RangeTable2,y
		SEP #$20
		ROL A
		AND #$01
		STA $15C4,x
		BNE INVALID

		LDY #$00
		LDA $1662,x
		AND #$20
		BEQ ON_SCREEN_LOOP
		INY
ON_SCREEN_LOOP:	LDA $D8,x
		CLC
		ADC TABLE1,y
		PHP
		CMP $1C
		ROL $00
		PLP
		LDA $14D4,x
		ADC #$00
		LSR $00
		SBC $1D
		BEQ ON_SCREEN_Y
		LDA $186C,x
		ORA TABLE2,y
		STA $186C,x
ON_SCREEN_Y:	DEY
		BPL ON_SCREEN_LOOP

INVALID:	RTS