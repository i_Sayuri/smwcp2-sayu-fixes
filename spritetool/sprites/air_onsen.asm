;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!RAM_AirMeter = $7F8827

	;; Where to place the meter relative to the screen boarder
	!MeterPositionX = $E8
	!MeterPositionY = $1E
	
	;; Possible values for the below variables:
	;;   (fast) $00, $01, $03, $07, $0f, $1f, $3f, $7f, $ff (slow)
	!CountDownFrequency = $00
	!CountUpFrequency = $01

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;; INIT and MAIN routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		PRINT "INIT ",pc
PHB
PHK
PLB
LDY #$09				; setup loop

LOOPSTART:
CPY $15E9				;\ if sprite being checked is this one,
BEQ NEXTCYCLE				;/ branch
LDA $14C8,y				;\ if sprite being checked is non-existant,
BEQ NEXTCYCLE				;/ branch
PHX
TYX
LDA $7FAB9E,x				;\  if sprite being checked isn't
PLX					; | the same custom sprite as this one,
CMP $7FAB9E,x				; |
BNE NEXTCYCLE				;/  branch
STZ $14C8,x				; if code gets here, there is another instance of
BRA LOOPDONE				; this sprite active, so this one is destroyed

NEXTCYCLE:
DEY					; decrease loop counter
BPL LOOPSTART				; if sprites left to check, branch

LOOPDONE:
PLB
        RTL

        PRINT "MAIN ",pc
        PHB
        PHK
        PLB
        JSR MAINSUB
        PLB
        RTL
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sprite Main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MAINSUB:
	LDA !RAM_AirMeter	; Play warning sounds

	CMP #$1C
	BEQ SOUND
	CMP #$14
	BEQ SOUND
	CMP #$0C
	BNE NOSOUND
SOUND:	
	LDA #$2a
	STA $1DFC
NOSOUND:
	
	;; 	LDA !RAM_AirMeter	; Change Mario's face blue
	;; 	CMP #$30
	;; 	BCS NoChangePal
	;; 	
	;;      LDY #$86              
	;;  	STY $2121             
	;; 	
	;; 	LDA #$31
	;; 	STA $2122
	;; 	LDA #$7e
	;; 	STA $2122	
	;; 	
	;; 	LDY #$8e              
	;;  	STY $2121             
	;; 	
	;; 	LDA #$ad
	;; 	STA $2122
	;; 	LDA #$6d
	;; 	STA $2122
	;; NoPalChange:
	
	LDA !RAM_AirMeter	; If the meter is full...
	CMP #$FF		;  
	BNE Gfx			; ...and it has been for some time...
	LDA $1540,x		;
	BEQ NOGFX		; ...then skip the graphics routine
Gfx:	
	JSR SUBGFX		; Draw the sprite
NOGFX:	

	LDA $9D			; RETURN if sprites are locked
	BNE RETURN0

	LDA $75
	BEQ COUNTUP
COUNTDOWN:
	LDA $13
	AND #!CountDownFrequency
	BNE SKIPMETER

	LDA !RAM_AirMeter
	DEC A
	BNE SETMETER
	BRA KILLMARIO
COUNTUP:
	LDA $13
	AND #!CountUpFrequency
	BNE SKIPMETER
	
	LDA !RAM_AirMeter
	CMP #$FF
	BEQ SKIPMETER

	LDA #$48
	STA $1540,x
	
	LDA !RAM_AirMeter
	INC A
	;; 	LDA #$FF		; NOTE: Reinstate to make the meter instantly reset when not in water
SETMETER:	
	STA !RAM_AirMeter
	
SKIPMETER:		
	LDA $1a			; Position = Screen boundary
	CLC
	ADC #$80
	STA $E4,x		; This is done so the sprite stays on screen
	LDA $1b
	ADC #$00
	STA $14E0,x
	LDA $1c
	STA $D8,x
	LDA $1d
	STA $14D4,x
RETURN0:	
	RTS

KILLMARIO:
	LDA #$90
	STA $7D
	LDA #$09		; Set death animation
	STA $71
	STA $1DFB		; Set death music
	LDA #$FF
	STA $0DDA
	LDA #$30		; Set time until we RETURN to the OW
	STA $1496
	STA $9D
	STZ $140D
	STZ $1407
	STZ $188A
	RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!NumTiles = $05
	
SPRITETILEDISPX:    db $00,$08,$10,$18,$20,$27
SPRITETILEDISPY:    db $28,$20,$18,$10,$08,$01
SPRITEGFXPROP:	    db $80,$00,$00,$00,$00,$00

PALETTES:           db $09,$05,$0B
	
SUBGFX:
	JSR GETPALETTE
	;; 	LDA $15F6,x		; NOTE: Reinstate to use the palette from the cfg file
	STA $02
	
	LDA !RAM_AirMeter	; Load address to be timed
	LSR
	LSR
	LSR
	STA $03
	
        LDY $15EA,x             ; get offset to sprite OAM                           

        PHX
        LDX #!NumTiles
GFXLOOPSTART:

        LDA #!MeterPositionX
        STA $0300,Y
	
	LDA #!MeterPositionY
	CLC
	ADC SPRITETILEDISPY,x
        STA $0301,Y

	LDA $03
	JSR CALCULATETILE
        STA $0302,Y
	
        LDA #%00110111
        ORA SPRITEGFXPROP,x
	ORA #$20
        STA $0303,Y
	
        INY    
        INY                     
        INY                     
        INY                     

	DEX
        BPL GFXLOOPSTART
	PLX

        LDY #$00
        LDA #!NumTiles
        JSL $01B7B3
        RTS                       

GETPALETTE:
	LDY #$00
	LDA !RAM_AirMeter
	CMP #$55
	BCC LOADPALETTE
	INY
	CMP #$AA
	BCC LOADPALETTE
	INY
LOADPALETTE:
	LDA PALETTES,y
	RTS	
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
CALCULATETILE:
	CPX #$00
	BEQ ENDPIECE
	CPX #!NumTiles
	BEQ ENDPIECE

	PHA
	TXA
	DEC A
	ASL
	ASL
	ASL
	STA $00
	PLA

	SEC
	SBC $00
	BMI EMPTYTILE
	
	CMP #$08 	
	BCS FULLTILE

	AND #$07
	CLC
	ADC #$47
	CMP #$4B
	BCC RETURN
	CLC
	ADC #$0C
RETURN:	
	RTS

FULLTILE:
	LDA #$46
	RTS
EMPTYTILE:
	LDA #$47
	RTS
ENDPIECE:
	LDA #$56
	RTS
	