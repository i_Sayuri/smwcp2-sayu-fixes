;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Laser Orb Shooter/Door, by imamelia
;;
;; This sprite performs various functions related to laser orb puzzles.
;;
;; Uses first extra bit: YES
;;
;; If the first extra bit is clear, the sprite will be a laser orb shooter ($C2,x = 0).
;; If the first extra bit is set, the sprite will be a laser orb-actived door ($C2,x = 1).
;;
;; Sprite table info:
;;
;; - $1510,x = sprite state (for the door)
;; - $1570,x = frame counter (for the shooter)
;; - $1602,x = animation frame (for the door)
;; - $160E,x = tile offset (for the door)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

TweakerValues:
db $00,$10,$FF,$A2,$B9,$46
db $00,$2B,$FF,$A2,$B9,$46
;db $00,$00,$FF,$02,$B1,$46

StartingIndexes:
db $00,$06;,$0C

!LaserOrbSprNum = $09
!ShootSFX = $231DF9
!DoorSFX1 = $221DFC
!DoorSFX2 = $0F1DFC

ShooterTilemap:
db $E2,$E4

DoorTilemap:
db $8C,$AC,$8E,$AE

SolidOffsetsLo:
db $01,$FF
SolidOffsetsHi:
db $00,$FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
PHB
PHK
PLB
JSR LaserOrbSprInit
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbSprInit:

LDA $D8,x			;
SEC					;
SBC #$01				;
STA $D8,x			;
LDA $14D4,x			;
SBC #$00				;
STA $14D4,x			;

LDA $7FAB10,x		;
AND #$04			;
LSR #2				;
STA $C2,x			;
TAY					;
LDA StartingIndexes,y	;
TAY					;
LDA TweakerValues,y	;
STA $1656,x			;
LDA TweakerValues+1,y	;
STA $1662,x			;
LDA TweakerValues+2,y	;
STA $166E,x			;
LDA TweakerValues+3,y	;
STA $167A,x			;
LDA TweakerValues+4,y	;
STA $1686,x			;
LDA TweakerValues+5,y	;
STA $190F,x			;
LDA $C2,x			;
CMP #$01			;
BNE .End				;
LDA $D8,x			;
CLC					;
ADC #$08			;
STA $D8,x			;
LDA $14D4,x			;
ADC #$00			;
STA $14D4,x			;
.End					;
RTS					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR LaserOrbSprMain
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbSprMain:

LDA $C2,x				;
BEQ LaserOrbShooterMain	;
JMP LaserOrbDoorMain		;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; shooter main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbShooterMain:

JSR LaserOrbShooterGFX

LDA $14C8,x
CMP #$08
BNE .Return
LDA $9D
BNE .Return

JSR SubOffscreenX0

INC $1570,x			;
LDA $1570,x			;
CMP #$40			;
BCC .NoShoot			;
JSR SpawnLaserOrb		;
STZ $1570,x			;
.NoShoot				;

JSL $81B44F			; make the shooter solid

.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; door main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbDoorMain:

JSR LaserOrbDoorGFX

LDA $14C8,x
CMP #$08
BNE .Return
LDA $9D
BNE .Return

JSR SubOffscreenX0

LDA $1510,x			;
JSL $8086DF			;

dw S00_Waiting		;
dw S01_OpenInit		;
dw S02_Opening		;

.Return				;
RTS

S00_Waiting:			;

LDA $D8,x			;
PHA					;
SEC					;
SBC #$10				;
STA $D8,x			;
LDA $14D4,x			;
PHA					;
SBC #$00				;
STA $14D4,x			;

JSL $81A7DC			;
BCC .Return			;

JSR SubHorizPos		;
LDA $0F				;
CLC					;
ADC #$04			;
CMP #$08			;
BCC .Return			;

LDA $94				;
CLC					;
ADC SolidOffsetsLo,y	;
STA $94				;
LDA $95				;
ADC SolidOffsetsHi,y	;
STA $95				;
STZ $7B				;

.Return				;
PLA					;
STA $14D4,x			;
PLA					;
STA $D8,x			;
RTS					;

S01_OpenInit:			;

LDA #$0F				;
STA $1DFC			;
INC $1510,x			;
INC $1602,x			;
RTS					;

S02_Opening:			;

INC $160E,x			;
LDA $160E,x			;
CMP #$20			;
BCC .Return			;
STZ $14C8,x			;
.Return				;
RTS					;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; shooter graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbShooterGFX:

JSR GetDrawInfo			;

LDA $00					;
STA $0300,y				;
STA $0304,y				;

LDA $01					;
STA $0301,y				;
CLC						;
ADC #$10				;
STA $0305,y				;

LDA.w ShooterTilemap		;
STA $0302,y				;
LDA.w ShooterTilemap+1	;
STA $0306,y				;

LDA #$3D				;
STA $0303,y				;
STA $0307,y				;

LDY #$02					;
LDA #$01				;
JSL $81B7B3				;
RTS						;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; door graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

LaserOrbDoorGFX:

JSR GetDrawInfo			;

LDA $00					;
STA $0300,y				;
STA $0304,y				;
STA $0308,y				;
STA $030C,y				;

LDA $01					;
SEC						;
SBC #$18					;
SEC						;
SBC $160E,x				;
STA $0301,y				;
CLC						;
ADC #$10				;
STA $0305,y				;
LDA $01					;
CLC						;
ADC #$08				;
CLC						;
ADC $160E,x				;
STA $0309,y				;
CLC						;
ADC #$10				;
STA $030D,y				;

LDA $1602,x				;
ASL						;
TAX						;
LDA.w DoorTilemap,x		;
STA $0302,y				;
STA $030E,y				;
LDA.w DoorTilemap+1,x		;
STA $0306,y				;
STA $030A,y				;

LDA #$30				;
STA $02					;
LDA $C2,x				;
BEQ .KeepPriority			;
LDA #$10				;
STA $02					;
.KeepPriority				;
LDA #$0D				;
ORA $02					;
STA $0303,y				;
STA $0307,y				;
ORA #$80				;
STA $030B,y				;
STA $030F,y				;

LDX $15E9				;
LDY #$02					;
LDA #$03				;
JSL $81B7B3				;
RTS						;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; other subroutines
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;------------------------------------------------
; laser orb spawning routine
;------------------------------------------------

SpawnLaserOrb:

LDA $18A0,x			;
ORA $186C,x			;
BNE .Return			;

JSR FindFreeC			;
BMI .Return			;

LDA #!LaserOrbSprNum	;
STA $1892,y			; cluster sprite number

LDA $E4,x			;
STA $1E16,y			; X position low byte
LDA $14E0,x			;
STA $1E3E,y			; X position high byte
LDA $D8,x			;
CLC					;
ADC #$1C			;
STA $1E02,y			; Y position low byte
LDA $14D4,x			;
ADC #$00			;
STA $1E2A,y			; Y position high byte

LDA #$06			; make the sprite start out moving down
STA $0F4A,y			; here, $0F4A is a direction table

LDA #$01			;
STA $18B8			; enable cluster sprite codes to run

LDA.b #!ShootSFX>>16	;
STA.w !ShootSFX		; play a sound effect

.Return				;
RTS					;

;------------------------------------------------
; cluster sprite slot-finding routine
;------------------------------------------------

FindFreeC:

LDY #$13
.Loop
LDA $1892,y
BEQ .FoundSlot
DEY
BPL .Loop
.FoundSlot
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; miscellaneous standard subroutines
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Table1:              db $0C,$1C
Table2:              db $01,$02
Table3:              db $40,$B0
Table6:              db $01,$FF
Table4:              db $30,$C0,$A0,$C0,$A0,$F0,$60,$90,$30,$C0,$A0,$80,$A0,$40,$60,$B0
Table5:              db $01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$FF,$01,$00,$01,$FF

SubOffscreenX0:
LDA #$00
;BRA SubOffscreenMain
;SubOffscreenX1:
;LDA #$02
;BRA SubOffscreenMain
;SubOffscreenX2:
;LDA #$04
;BRA SubOffscreenMain
;SubOffscreenX3:
;LDA #$06
;BRA SubOffscreenMain
;SubOffscreenX4:
;LDA #$08
;BRA SubOffscreenMain
;SubOffscreenX5:
;LDA #$0A
;BRA SubOffscreenMain
;SubOffscreenX6:
;LDA #$0C
;BRA SubOffscreenMain
;SubOffscreenX7:
;LDA #$0E

SubOffscreenMain:

STA $03

JSR SubIsOffscreen
BEQ Return2

LDA $5B
LSR
BCS VerticalLevel
LDA $D8,x
CLC
ADC #$50
LDA $14D4,x
ADC #$00
CMP #$02
BPL EraseSprite
LDA $167A,x
AND #$04
BNE Return2
LDA $13
AND #$01
ORA $03
STA $01
TAY
LDA $1A
CLC
ADC Table4,y
ROL $00
CMP $E4,x
PHP
LDA $1B
LSR $00
ADC Table5,y
PLP
SBC $14E0,x
STA $00
LSR $01
BCC Label20
EOR #$80
STA $00
Label20:
LDA $00
BPL Return2

EraseSprite:
LDA $14C8,x
CMP #$08
BCC KillSprite
LDY $161A,x
CPY #$FF
BEQ KillSprite
LDA #$00
STA $1938,y
KillSprite:
STZ $14C8,x
Return2:
RTS

VerticalLevel:

LDA $167A,x
AND #$04
BNE Return2
LDA $13
LSR
BCS Return2
AND #$01
STA $01
TAY
LDA $1C
CLC
ADC Table3,y
ROL $00
CMP $D8,x
PHP
LDA $1D
LSR $00
ADC Table6,y
PLP
SBC $14D4,x
STA $00
LDY $02
BEQ Label22
EOR #$80
STA $00
Label22:
LDA $00
BPL Return2
BMI EraseSprite

SubIsOffscreen:
LDA $15A0,x
ORA $186C,x
RTS

GetDrawInfo:

STZ $186C,x
STZ $15A0,x
LDA $E4,x
CMP $1A
LDA $14E0,x
SBC $1B
BEQ OnscreenX
INC $15A0,x
OnscreenX:
LDA $14E0,x
XBA
LDA $E4,x
REP #$20
SEC
SBC $1A
CLC
ADC.w #$0040
CMP #$0180
SEP #$20
ROL A
AND #$01
STA $15C4,x
BNE Invalid

LDY #$00
LDA $1662,x
AND #$20
BEQ OnscreenLoop
INY
OnscreenLoop:
LDA $D8,x
CLC
ADC Table1,y
PHP
CMP $1C
ROL $00
PLP
LDA $14D4,x
ADC #$00
LSR $00
SBC $1D
BEQ OnscreenY
LDA $186C,x
ORA Table2,y
STA $186C,x
OnscreenY:
DEY
BPL OnscreenLoop
LDY $15EA,x
LDA $E4,x
SEC
SBC $1A
STA $00
LDA $D8,x
SEC
SBC $1C
STA $01
RTS

Invalid:
PLA
PLA
RTS

SubHorizPos:

LDY #$00
LDA $94
SEC
SBC $E4,x
STA $0F
LDA $95
SBC $14E0,x
BPL $01
INY
RTS


