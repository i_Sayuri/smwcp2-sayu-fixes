;Horizontal Screen Wrap
;connects the top and bottom of the screen, meaning that if you fall down a hole, you
;will see MARIO fall from the top of the screen, and if a SPRITE falls down, it will also
;appear at the top of the screen. Make sure whatever level you use this on has no vertical
;scrolling enabled, and that MARIO starts on the lower half of the screen
;This is the same as the patch, but it's in a SPRITE format so it's easier to add toonly specific levels
;Also fixes a bug noticed by some people where MARIO would act kinda choppy when big
;insert as a generator
;erm... I don't think it matters which SPRITE tool you use to assemble it but I used Romi's when I tested it
;so I know it will work with that
;made by: HuFlungDu
;No credit neccesary
;Feel free to PM me if you have any problems

PRINT "INIT ",pc
RTL
PRINT "MAIN ",pc
JSR MARIO	;check for MARIO
JSR SPRITE	;check for sprites
RTL

SPRITE:
LDY #$00	;innitiate the accumulator
LOOP:
LDA $14C8,y	;\check if the SPRITE is dead
CMP #$02	;|
BEQ ENDS	;/
LDA $14D4,y	;\Check which (vertical) subscreen the SPRITE is on
CMP #$01	;|
BNE UpS		;/
LDA $00D8,y	;\Check the sprites position on the subscreen
CMP #$A0	;|
BCC UpS		;/
LDA #$00	;\Move to subscreen 0
STA $14D4,y	;/
UpS:
LDA $14D4,y	;\Check which (vertical) subscreen the SPRITE is on
BNE ENDS	;|
LDA $00D8,y	;\Check the sprites position on the subscreen
CMP #$A0	;|
BCS ENDS	;/
LDA #$01	;\Move to subscreen 1
STA $14D4,y	;/
ENDS:
INY			;\increase the Accumulator
CPY #$0C	;|and see if it's checked all sprites
BCC LOOP	;/ if not, check the next one
RTS			;return

MARIO:
LDA $0100	;\make sure the level is going
CMP #$14	;|
BNE ENDM	;/
LDA $13E0	;\don't do it if MARIO is dead (this checks his pose)
CMP #$3E	;|
BEQ ENDM	;/
LDA $D4		;\Check which (vertical) subscreen MARIO is on
CMP #$01	;|
BNE UpM		;/
LDA $D3		;\Check MARIO's position on the subscreen
CMP #$A0	;|
BCC UpM		;/
LDA #$00	;\move to subscreen 0
STA $97		;/
UpM:
LDA $D4		;\Check which (vertical) subscreen MARIO is on
BNE ENDM	;/
LDA $D3		;\Check MARIO's position on the subscreen
CMP #$A0	;|
BCS ENDM	;/
LDA #$01	;\move to screen 0
STA $97		;/
ENDM:
RTS			;return

