
incsrc subroutinedefs_xkas.asm

;incsrc owcounter.asm		; Coin defines
incsrc CoinDefines.asm		; Coin defines
table table.txt

!FreeRAM 	= $7FA000		; 0x400 bytes needed in total
!L3TileMap	= !FreeRAM+$20		; 0x2E0 bytes long buffer
!L3Props1	= !FreeRAM+$300		; 0xA0 bytes long buffer
!L3Props2	= !FreeRAM+$3A0		; 0x60 bytes long buffer
!level_names_start = !L3TileMap+$21
!world_name_start = !L3TileMap+$2C3

!world_num = $13E6
!cursor_pos = $146C

!level_name_location = $D791A5 ;read3($83bb57), This will likely need to be updated manually
!level_name_size = 19
!world_name_size = 25
!max_levels = 15

!world_count = 9

print "MAIN ", pc
	PHB
	PHK
	PLB
	JSR SpriteMain
	LDX $15E9
	PLB
	RTL

world_name_pointers:
	dw world_1_name
	dw world_2_name
	dw world_3_name
	dw world_4A_name
	dw world_4B_name
	dw world_5_name
	dw world_6_name
	dw world_7_name
	dw world_8_name
	dw world_9_name

world_1_name:
	db "           WORLD 1        ", $FF
world_2_name:
	db "           WORLD 2        ", $FF
world_3_name:
	db "           WORLD 3        ", $FF
world_4A_name:
	db "           WORLD 4A        ", $FF
world_4B_name:
	db "           WORLD 4B        ", $FF
world_5_name:
	db "           WORLD 5        ", $FF
world_6_name:
	db "           WORLD 6        ", $FF
world_7_name:
	db "           WORLD 7        ", $FF
world_8_name:
	db "           WORLD 8        ", $FF
world_9_name:
	db "           WORLD 9        ", $FF

unknown_level_name:
	db "???????????????????", $FF

world_level_pointers:
	dw world_1_levels
	dw world_2_levels
	dw world_3_levels
	dw world_4A_levels
	dw world_4B_levels
	dw world_5_levels
	dw world_6_levels
	dw world_7_levels
	dw world_8_levels
	dw world_9_levels

; High bit is used to mark as having a secret exit
world_1_levels:
	db $02,$03|$80,$04,$05,$06,$07|$80,$08,$09|$80,$FF
world_2_levels:
	db $0A,$0B|$80,$0C,$0D,$0E,$0F|$80,$10,$11,$FF
world_3_levels:
	db $12|$80,$13,$14,$15|$80,$16,$17,$18,$FF
world_4A_levels:
	db $19,$1A|$80,$1B,$1C,$1D,$1E,$1F|$80,$FF
world_4B_levels:
	db $20|$80,$21,$22,$23,$24,$25,$26|$80,$27,$28,$FF
world_5_levels:
	db $33|$80,$34,$35,$36,$37|$80,$38|$80,$39,$FF
world_6_levels:
	db $3A,$3B,$3C|$80,$3D|$80,$3F,$40|$80,$41,$42,$43|$80,$44,$45,$FF
world_7_levels:
	db $29,$46,$47,$48,$49,$4A|$80,$4B|$80,$4C,$4D,$4E,$FF
world_8_levels:
	db $4F,$50,$51|$80,$52|$80,$53,$54,$55,$56,$5E,$FF
world_9_levels:
	db $57,$58,$59,$5A,$5B,$5C,$5D,$5F,$FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Init routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
print "INIT ", pc
SpriteInit:
	lda #$00
	sta !world_num
	sta !cursor_pos
	rtl
SpriteMain:
	LDA #$FF			; hide and stun mario
	STA $18BD
	STA $78
	LDA #$0A			; disable pausing
	STA $13D3
	
	LDA $13				; do some bg scroll
	LSR A
	BCC +
	REP #$20
	INC $1466
	INC $1468
	SEP #$20
	+
	jsr handle_world
	jsr handle_levels
	LDA $16				; check if it's time to warp
	AND #$10			; (when you press start)
	BEQ .nowarp
	LDA #$06
	STA $71
	STZ $89
	STZ $88
	.nowarp
	rts

handle_levels:
	jsr draw_levels
	rts

!world_level_pointer = $00
!level_tile_offset = $02
draw_levels:
	rep #$20
	stz !level_tile_offset
	sep #$20
	lda !world_num
	asl
	tax
	rep #$20
	lda world_level_pointers,x
	sta !world_level_pointer
	sep #$20
	ldy #$00
	-
	lda (!world_level_pointer),y
	cmp.b #$FF
	beq .done
	jsr draw_level
	rep #$20
	lda !level_tile_offset
	clc
	adc.w #$0020
	sta !level_tile_offset
	sep #$20
	iny
	bra -
	.done
	sty $00
	lda #!max_levels
	sec
	sbc $00
	tay
	-
	jsr draw_blank_level
	rep #$20
	lda !level_tile_offset
	clc
	adc.w #$0020
	sta !level_tile_offset
	sep #$20
	dey
	bpl -
	rts

draw_blank_level:
	phy
	rep #$10
	ldy.w #$0000
	ldx !level_tile_offset
	lda #$FC
	-
	sta !level_names_start,x
	inx
	iny
	cpy.w #!level_name_size+11
	bcc -
	sep #$10
	ply
	rts

draw_unseen_level:
	phy
	rep #$10
	ldy.w #$0000
	ldx !level_tile_offset
	lda.b #$89
	-
	sta !level_names_start,x
	inx
	iny
	cpy.w #!level_name_size
	bcc -
	inx
	sta !level_names_start,x
	sta !level_names_start+2,x
	lda.b #$8B
	sta !level_names_start+1,x

	inx #3
	iny #4
	lda.b #$FC
	-
	sta !level_names_start,x
	inx
	iny
	cpy.w #!level_name_size+11
	bcc -

	sep #$10
	ply
	rts

!exit_count = $04
!found_exits = $05
!level_num_temp = $08
!coins_found_temp = $0A
!scratch = $0B
draw_level:
	phy
	pha
	stz !exit_count
	stz !found_exits
	ldx.b #$01
	and.b #$80
	beq +
	ldx #$02
	+
	stx !exit_count
	pla
	and.b #$7F
	sta !level_num_temp
	tax
	ldy.b #$00
	jsr .get_level_event_passed
	cmp.b #$FF
	bne +
		jsr check_level_beaten
		beq +
			inc !found_exits
		bra .no_secret_exit
	+
	cmp.b #$00
	beq +
		inc !found_exits
	+
	lda !exit_count
	cmp.b #$01
	beq .no_secret_exit
		ldy.b #$01
		jsr .get_level_event_passed
		cmp.b #$00
		beq +
			inc !found_exits
		+
	.no_secret_exit
	lda !found_exits
	bne +
		jsr draw_unseen_level
		ply
		rts
	+

	lda.b #!level_name_size
	sta $4202
	txa
	sta $4203
	nop #4
	rep #$20
	lda $4216
	rep #$10
	tax
	ldy.w #$0000
	sep #$20
	-
	lda.l !level_name_location,x
	jsr convert_level_character
	phx
	pha
	rep #$20
	tya
	clc
	adc !level_tile_offset
	tax
	sep #$20
	pla
	sta !level_names_start,x
	plx
	inx
	iny
	cpy.w #!level_name_size
	bcc -
	tya
	clc
	adc !level_tile_offset
	tax
	; Draw exit count
	inx
	lda !found_exits
	sta !level_names_start,x
	inx
	lda #$8B
	sta !level_names_start,x
	inx
	lda !exit_count
	sta !level_names_start,x

	sta $7FA000
	; Draw coins
	inx
	phb
	lda.b #levelbittable>>16
	pha
	plb
	rep #$20
	lda.w #$0000
	sep #$20
	lda !level_num_temp
	lsr
	lsr
	lsr
	tay
	lda !level_num_temp
	and.b #$07
	tax
	lda leveltable,y
	and levelbittable,x
	cmp #$00
	beq .end_coins
	rep #$20
	tya
	txy
	tax
	sep #$20
	stz !coins_found_temp
	lda !ramspace,x
	and levelbittable,y
	beq +
	inc !coins_found_temp
	+
	lda !ramspace+11,x
	and levelbittable,y
	beq +
	lda #$02
	tsb !coins_found_temp
	+
	lda !ramspace+22,x
	and levelbittable,y
	beq +
	lda #$04
	tsb !coins_found_temp
	+
	plb
	lda.b #$04
	sta !scratch
	lda !level_tile_offset
	clc
	adc #28
	tax
	-
	ldy.w #$00A9
	lda !coins_found_temp
	and !scratch
	beq +
	iny
	+
	tya
	sta !level_names_start,x
	dex
	lsr !scratch
	bne -
	.end_coins
	sep #$10
	ply
	rts


!event_byte_temp = $06
!special_level = $127-$100+$24
!special_event = $4F
; x: level number
; y: Secret exit. 0=no, anything else=yes
; return:
;	a: 0=not triggered, not 0=triggered, FF=special case, no events. Check normal beaten.
.get_level_event_passed
	phx
	lda.l $05D608,x
	cmp.b #$FF
	bne +
		plx
		rts
	+
	cpy.b #$00
	beq +
		inc
		cpx.b #!special_level
		bne +
			lda.b #!special_event
	+
	pha
	lsr #3
	tax
	lda $1F02,x
	sta !event_byte_temp
	pla
	and.b #$07
	tax
	lda.b #$80
	-
	cpx.b #$00
	beq +
		lsr
		dex
	bra -
	+
	and !event_byte_temp
	plx
	rts

convert_level_character:
	cmp #$64
	bcc +
	cmp #$6C
	bcc .fix_number
	+
	cmp #$1A
	bcs +
	bra .fix_upper
	+
	cmp #$40
	bcc +
	cmp #$5A
	bcs +
	bra .fix_lower
	+
	cmp #$1F
	beq .fix_space
	cmp.b #$1E
	beq .fix_questionmark
	cmp.b #$32
	bcc +
	cmp.b #$35
	bcs +
	bra .fix_inci
	+
	cmp.b #$5D
	beq .fix_apostrophe

	.return
	rts

	.fix_upper
	clc
	adc #$0A
	bra .return
	
	.fix_lower
	sec
	sbc #$36
	bra .return
	
	.fix_space
	lda #$FC
	bra .return

	.fix_apostrophe
	lda.b #$85
	bra .return

	.fix_inci
	clc
	adc.b #$67
	bra .return

	.fix_questionmark
	lda.b #$89
	bra .return
	
	.fix_number
	cmp #$6B
	bne +
	lda #$00
	bra .return
	+
	sec
	sbc #$63
	bra .return

handle_world:
	jsr draw_world
	-
		jsr handle_world_change
		jsr check_world_seen
		beq -
	rts

!world_name_pointer = $00
draw_world:
	lda !world_num
	asl
	tax
	rep #$20
	lda world_name_pointers,x
	sta !world_name_pointer
	sep #$20
	ldy #!world_name_size
	.draw_loop
	tyx
	lda (!world_name_pointer),y
	sta !world_name_start,x
	dey
	bpl .draw_loop
	rts

handle_world_change:
	lda $16
	bit #$02
	bne .do_left
	bit #$01
	bne .do_right
	lda $18
	bit #$20
	bne .do_left
	bit #$10
	bne .do_right
	bra .return
	.do_left
	lda !world_num
	cmp.b #$00
	bne +
		lda #!world_count+1
	+
	dec
	bra .done
	.do_right
	lda !world_num
	cmp.b #!world_count
	bne +
		lda #$FF
	+
	inc
	.done
	sta !world_num
	.return
	lda !world_num
	rts


; Input:
;	a: world number
; Output:
;	a: 0=not seen, else=seen
check_world_seen:
	cmp.b #$00
	bne +
		lda #$01 ; Always show world 1
		rts
	+
	asl
	tax
	rep #$20
	lda world_level_pointers,x
	sta !world_level_pointer
	sep #$20
	ldy #$00
	-
	lda (!world_level_pointer),y
	cmp.b #$FF
	beq .done
		and #$7F
		tax
		jsr check_level_beaten
		bne .return
	iny
	bra -
	.done
	lda #$00
	.return
	rts

; Input:
;	x: translevel number
; Output:
;	a: 0=not beaten, 80=beaten
check_level_beaten:
	lda $1EA2,x
	and.b #$80
	rts

levelbittable:
	db $80,$40,$20,$10,$08,$04,$02,$01

; This table is mirrored in asm/owcounter.asm. If you change it here, change it there!
leveltable:
	db %00111111	; Level 000 - 007
	db %11111111	; Level 008 - 00F
	db %11111111	; Level 010 - 017
	db %11111111	; Level 018 - 01F
	db %11111111	; Level 020 - 024, Level 101 - 103
	db %11111111	; Level 104 - 10B
	db %11111111	; Level 10C - 113
	db %11111111	; Level 114 - 11B
	db %11111111	; Level 11C - 123
	db %11111111	; Level 124 - 12B
	db %11111100	; Level 12C - 133
	db %00000000	; Level 134 - 13B