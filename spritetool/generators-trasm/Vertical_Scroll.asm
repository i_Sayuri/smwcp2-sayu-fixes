;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; "Vertical Auto Scroll" generator
; By Jimmy52905
; Fixed by yoshicookiezeus
; Uses first extra bit: YES
; If extra bit is set, the generator makes the screen scroll downwards instead of upwards
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

STOP:		db $10		; screen number the sprite should STOP at when scrolling downwards

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	PRINT "INIT ",pc
	PRINT "MAIN ",pc                                    
	PHB                     
	PHK                     
	PLB                     
	JSR MAIN 
	PLB                     
	RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAIN sprite code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MAIN:
	LDA $9D		;\ If sprites are locked...
	BNE RETURN	;/ Go to "RETURN".

	LDA $13		;\
	LSR A		; |
	BCC RETURN	;/ Only run code every other frame.

	STZ $1412	; Disable Vertical Scroll so that the code can run properly.

	LDA $18B9	;\ If the first extra bit is set...
	AND #$40	; |
	BNE DOWN	;/ Go to "DOWN".

	STZ $55		; Make sure that new tiles are uploaded from the top of the screen

	LDA $1464	;\
	ORA $1465	; |If the screen position is at the top of the level...
	BEQ RETURN	;/ Go to "RETURN".

	REP #$20	; 16-bit (accumulator).
	DEC $1464	; Make the screen scroll upwards automatically.
	SEP #$20	; 8-bit (accumulator).

RETURN:
	RTS		;RETURN from Subroutine.

DOWN:
	LDA #$02	;\ Make sure that new tiles are uploaded from the bottom of the screen
	STA $55		;/

	LDA $1465	;\ If the screen position is at screen 10 (last screen of a vertical layer 2 level)...
	CMP STOP	; |
	BEQ RETURN	;/ Go to "RETURN".

	REP #$20	; 16-bit (accumulator).
	INC $1464	; Make the screen scroll downwards automatically.
	SEP #$20	; 8-bit (accumulator).

	RTS		;RETURN from Subroutine.