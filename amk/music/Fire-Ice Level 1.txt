#SPC
{
	#author "Camerin"
	#comment "Fire/Ice theme 1"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
	"CCymbal.brr"
}

#0 w160 t41

$EF $FF $46 $46
$F1 $02 $32 $01

#0 @14 v240
o2[a16  a16a16r8^16a16a16a16r8^16a+16a16g16a+16]8
[r1]2
[a16a16a16r8^16a16a16>c16r8^16c16c16c16c16
<a16a16a16r8^16a16a16e16r8^16e16e16e16e16
a16a16a16r8^16a16a16>c16r8^16c16c16c16c16
<a16a16a16r8^16a16a16e16e16e16r16a16a16a16a16]2
>d16d16d16r8^16d16c16<b16a16g16a16b16>c16d16<b16
>e16e16e16r8^16e16d16c16<b16>c16d16e16d16c16e16
d16d16d16r8^16d16c16<b16a16g+16f16e16g+16b16>d16
c16c16c16r8^16c16<b16a16b16>c16d16e16e16e16e16
d16d16d16r8^16d16f16a16f16d16a16g16e16c+16e16
<a16a16a16r8^16a16>c16e16c16<a16>c16e16c16<a16>c16
e16e16e16r8^16e16g+16b16g+16e16b16a16f+16d+16f+16
e16e16e16r8^16e16e16e16r8^16e16f16e16f16
;
#1 @3 v240
[r1]4
o4[a16a16a16a16>a16a16<a16a16a16a16a16a16>a16a16a16a16]4
[r1]2
@2 v200
a8^16g16a8e8g8f+8e8d8
c16<b16a16b16>c8d8e4<g+4
>a8^16g16a8e8g8f+8e8d8
c8^16d16e8c8e16d16c16<b16a4
[r1]4
>f8e16f32e32d8c8<b8a8g8>f8
e8d16e32d32c8<b8>c8d8e4
d8c16d32c32<b8a8g+8b8>f8e8
<[a16]4>[c16]4[e16]4[a16]4
[r1]4
;
#6 @13 v235 p40,30 $ED $70 $88
o4[r1]2
o3a1^1
o5a1
g1
f1
d+1
[r1]2
a2g2
a2<g+2
>a2g2
a1
[r1]4
f2d2
e2c2
d2<b2
a1
[r1]4
;
#2 @11 v200 $ED $1C $AA p20,40
[r1]8
o3a16a16a16r8^16a16a16a16r8^16a+16a16g16a+16
a1
<a2g2
a2>e2
<a2g2
a2e4a4
o4c16r8c16c8<a8a8a8a8g8
e16r16a16e16a8>c8e2
c16r8c16c8<a8a8a8a8g8
a4g4e4a4
o5f8d8<b8>d8<g8b8>d8f8
e8c8<g8>c8<e8g8>c8e8
d8<b8g+8b8e8g+8b8>d8
c4d4e2
<d16r8d16d8c+8d8c+8<a4
a16r8a16a8e8a8>c8e4
e16r8e16e8<b8>e4d+4
e1
;
#3 @11 v200 $ED $1C $AA p20,40
[r1]8
o4c16c16c16r8^16c16c16c16r8^16c+16c16<a+16>c+16
c1
v200>a8e8c8e8g8e8c8e8
a8e8c8e8g+8e8d8e8
a8e8c8e8g8e8c8e8
a8e8c8e8g8e8a4
v240<e16r8e16e8c8e8d8c8<b8
a16r16>c16<a16>c8e8a4g+4
e16r8e16e8c8e8d8c8<b8
>c4<b4a4>e4
<f2d2
e2c2
d2<b2
>c2<a2
o4f16r8f16f8e8f8e8d8c+8
c16r8c16c8<a8>c8e8a4
g+16r8g+16g+8e8g+4f+4
a2g+2
;
#5 @11 v200 $ED $1C $AA p20,40
[r1]8
o4e16e16e16r8^16e16e16e16r8^16f16e16d16f16
e1
[r1]4
a16r8g16a8e8g8f+8e8d8
c16r16e16c16e8a8>c4<b4
a16r8g16a8e8g8f+8e8d8
e4d4c4a4
[r1]4
a16r8a16a8g8a8g8f8e8
e16r8e16e8c8e8a8>c4
<b16r8b16b8g+8b4a4
b1
;
#4 @10 v240
o3[@10c4>@29e8^16<@10c8^16@10c8>@29e4]26
;

                

#amk=1
