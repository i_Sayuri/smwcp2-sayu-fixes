#SPC
{
	#author "Kipernal"
	#comment "Sky castle theme"
	#game "SMW Central Production 2"
}

#samples
{
	#optimized
	#AMM
	#sky_and_dream
	"CTrumpet.brr"
	"CCymbal.brr"
	"CTimpani.brr"
	"CStrings.brr"
	"CBass.brr"
	"CChoir.brr"
}

#0 w160
$EF $A1 $33 $33
$F1 $05 $44 $01

$F4 $02

@15 ("CTimpani.brr", $04) $ED $7F $E0  t50 o3v255 g8^16  g16g8g4g4g8g1
@0 ("CCymbal.brr", $04) $ED $6F $AF  b+1^1
^1^1

@15 ("CTrumpet.brr", $09) $ED $6A $C7 $DE $30 $08 $30  o4  ;$DE $60 $22 $30

d4^8c8d4^8e8d2^4^8<b8
/>c2^4^8d8c1
d4^8c8d4^8e8d2^4^8e8
f2f8^16f8^32g8^32>c1

<e4^8d8e4f+4g4^8f+4^8e4
e4^8d8e4f+4g2^4a4
>c2<b4a4b2^4g4
a2^4b4e1

e4^8d4^8<b4>e2^4e8f+8
g4^8a4^8b4a1
e4^8d4^8<b4>e2^4e8f+8
g4^8a4^8b4>d4<b8a4f+8d4

d4^8e4^8<b4a2^4f+4
g4^8a4^8b4g1
>d4^8e4^8<b4>f+4^8e4^8f+4
g4^8a4^8b4>e1

<d4^8c8d4^8e8d2^4^8<b8

#1
r1^1
^1^1
^1^1 o3v255 
 (2)[@23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16]
/(3)[@23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23  c16]
 (2)
 (3)4
 @23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @0 ("CCymbal.brr", $04) $ED $02 $E0  v255 b+2
$ED $6F $AF b+1^1
^1^2 $ED $03 $1F b+1
^2^1
^1^2 @23   v162 c4 @23  c4
[ @23  c2 @23  c2 @23  c2 @23  c4 @23  c8 @23  c8]2
 @23  c4@10 >v255 c4 @23   v162 <c4@10 >v255 c8@10  v162 c8 @23  <c4@10 >v255 c4 @23   v162 <c4@10 >v255 c8@10  v162 c8
 @23  <c2 @23  c2 @23  c4 @23  c8 @23  c8@10 >v255 c32@10  v157 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10 c32@10  v200 c32
@0 ("CCymbal.brr", $04) $ED $6F $AF  <v255 b+2 @23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23  c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16 @23   v157 c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  c16 @23  v255 c16


#2
@10 o4v255 c8 c16 c16 c8 c8 c8 c16 c16 c8 c8 @27   v243 o2c32 @27   v255 c32 @27   v76 c32 @27   v82 c32 @27 v87 c32 @27  v96 c32 @27  v102 c32 @27  v108 c32 @27  v113 c32 @27  v119 c32 @27  v125 c32 @27  v131 c32 @27  v136 c32 @27  v145 c32 @27  v151 c32 @27  v157 c32 @27  v162 c32 @27  v168 c32 @27  v174 c32 @27  v180 c32 @27  v189 c32 @27  v194 c32 @27  v200 c32 @27  v206 c32 @27  v212 c32 @27  v217 c32 @27  v223 c32 @27  v229 c32 @27  v238 c32 @27  v243 c32 @27  v249 c32 @27  v255 c32
@10 o4
c8 c16 c16 c8 c8 c16 c16 c8 c32  v157 c32 c32 c32 c32 c32 c32  v200 c32 v255 c8 c16 c16 c8 c8 c16 c16 c8 c4
(1)[c8 c16 c16 c8 c8 c16 c16 c8 c32  v157 c32 c32 c32 c32 c32 c32  v200 c32 v255 c8 c16 c16 c8 c8 c16 c16 c8 c8 c8
c8 c16 c16 c8 c8 c16 c16 c8 c32  v157 c32 c32 c32 c32 c32 c32  v200 c32 v255 c8 c16 c16 c8 c8 c16 c16 c8 c4]
/
(1)
(1)
(1)

 c8 c16 c16 c8 c8 c16 c16 c8 c8 c8 c8 c16 c16 c8 c8 c32  v157 c32 c32 c32 c32 c32 c32 c32 c32 c32 c32 c32 c32 c32 c32  v200 c32
 v255 c1^1
^1^1
^1^1
^1^2 @23  <c8 @23  c8 @23  c8 @23  c8
 (4)[@23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8]
 (4)
 (4)
 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @23  c8 @0 ("CCymbal.brr", $04) $ED $02 $E0   b+2
@10 >
c8 c16 c16 c8 c8 c16 c16 c8 c32  v157 c32 c32 c32 c32 c32 c32  v200 c32 v255 c8 c16 c16 c8 c8 c16 c16 c8 c4

#3
@0 ("CBass.brr", $04) $ED $7F $95 o3v255 $EE $FF
c4^8d4d+8^16f8^16g1
c4^8c4^8<g4>c4^8c4^8<g8a+8
>c4^8c4^8<g4>c4^8c4^8<g4
>c4^8c4<g16a+16>d4c4^8c4<g8>d8<a+8
/a+4^8a+4f16g+16>c4<a+4^8a+8^16f16>f16c+16d+16c16c+16<g+16
>c4^8c4<g16a+16>d4c4^8c4<g8>d8<a+8
a+4^8a+4f16g+16>c4<a+4^8a+8^16f16>f16c+16d+16c16c+16<g+16
>d4^8d4^8c4f4^8f4^8g4
d4^8d4^8c4f4^8f4^8e4
d4^8d4^8c4f4^8f4^8g4
d4^8d4^8c4d4^8d4^8d4
^1^1
^1^1
^1^1
^1^1
^1^2^8g4c8
^1^2^8<g4a8
^1^2^8>g4a8
^4
$EE $00
@10 >c2 c8  v162 c8 v255 c2^8 $EE $FF @0 ("CBass.brr", $04) $ED $7F $95 <d4d8
c4^8c4<g16a+16>d4c4^8c4<g8>d8<a+8


#7
[r1]8
/[r1]12
^1^2 @23  o3v255 c16   v157 @23c16  @23c16  @23c16  @23c16  @23c16  @23c16  v255 @23c16
[r1]12
^1^2^4   v162 @23c8  @23c8
^1^2 v255 @23c8  @23c8   @23c8  @23c8
  @23c16   v157 @23c16   @23c16  @23c16  @23c16  @23c16  @23c16  @23c1^2^16


#5
@15 ("CTrumpet.brr", $09) $ED $6A $C0  v255 o3

d1a1
>d1^1 
$E8 $FF $00 ^1^1
y8
@15 ("CStrings.brr", $04) $ED $45 $A9 $EE $FF o3 v225
d1c1
/ @15 ("CStrings.brr", $04) $ED $57 $E5

c1<a+2>d2
d1c1
c1<a+2>d2





@11 ("CChoir.brr", $03) $ED $57 $E5 >  e1a1
e1a1
>e1f+2g+2
d+2^4e4c+1
<f+1g+2e2
f+1g+2f+2
f+1g+2e2
f+1g+2>c+4<f+4
>e2^4c+1^4
d+2^4<b4>c+1
e2^4c+1^4
<b1>c+1
@15 ("CStrings.brr", $02) $ED $45 $A5 <
d1c1



#4
@15 ("CStrings.brr", $04) $ED $45 $A5 o2v225 $EE $FF 
g2a+4>c4d1
y12
$ED $45 $A0 g1^1
$E8 $FF $00 ^1^1 v255
$ED $45 $A5 g1g1
/f1f1
g1g1
f1f1

@11 ("CChoir.brr", $03) $ED $48 $A5 >>c+1e2^4d+4
c+1e2^4f+4
a2^4b4g+2^4e4
f+2^4<b4>c+1
c+1<b2>c+2
c+1f+2<b2
>c+1<b2>c+2
c+1f+2g+4<b4
>a2^4g+8f+8g+1
f+2^4g+4e1
a2^4g+8f+8g+1
d+2^4g+4>c+1
@15 ("CStrings.brr", $04) $ED $45 $A5 o3 $EE $FF g1g1
                

#amk=1
