#SPC
{
	#author "Moose"
	#comment "Desert ruins level 2"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

"FluteInstr=@4 (\"CPanFlute.brr\", $04) $ed $77 $c2"
#0 t48 w235
$ef $83 $68 $68
$f1 $08 $36 $01

#0 @8 $ed $7d $ea v255 l1 q7e
o2
(1)[c^2^4r4c^2^4r4<b-^2^4r4b-^2^4r4]
/
(1)6

#1 @14 v255 l16 q7f
o3
(2)[ccg8f8c8>c4<fffgf8c2^8r4ccg8f8c8>c4<fffgf8c2^8r4<b-b->f8e-8<b-8>b-4e-e-e-fe-8<b-2^8r4b-b->f8e-8<b-8>b-4 e-e-e-fe-8<b-2^8r4>]
/
(2)3
[cc2^4f8^f2^4r4cc2^4f8^f2^4r4<b-b-2^4>e-8^e-2^4r4<b-b-2^4>e-8^e-2^4r4]2

#2 l16 v255 q7f
o3
(5)[r1]8
/
[y10 @21c8 y8 @22c y12@22c y10 @29g8 y8 @22c y10 @21c y12 @22c y8 @22c y10 @21c y12 @22c y10@29g8 y8 @22c y12 @22c]36
[r8 y10 @22c@22cr8@22cr@22c@22cr@22c@22cr8@22c@22c]8

#3 l16 v255 q7e
o3
(5)8
/
$f4 $03
(5)8
@1 $ed $77 $c2
$de $30 $10 $90 v200 y10
q7f o4 c1<a+2.g4a2.a+4a2.a+4>
c2.<a+4>d+2.<a+4>c2<a+4f4c2.r4
a2.a+4>c2.<a+4>c2d2f2g2
$df @13 f1c1<g1^1 $f4 $03 FluteInstr v180 y12 o4
[q7d c1^1c1^1<b-1^1b-1^1]2

#5 l16 v200 @14 q7d
o3
(5)8
/
(5)24
FluteInstr y8 v180
[q7d f1^1f1^1e-1^1e-1^1]2

#4 l16 v200
o3
(5)8
/
$f4 $03
(5)24 v255
$f4 $03
@13 $ed $77 $c0 v200
$de $30 $10 $90
[q7c o4 c1^1c1^1<a+1^1a+1^1]4                

#amk=1
