#SPC
{
	#author "Bloop"
	#comment "Desert ruins theme 4"
	#game "SMWCP2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

;s02

#0
t50 w160 l16
[r1]9
/
[r1]8
@14
;$de $00 $01 $32
v250
o5c4^8<b-8>c8<b-8>c8<b-8>
e-4e-8dcd2
o4>c4^8<b-8>c8e-8g8e-8
a-2f2
o4>c4^8<b-8>c8<b-8>c8<b-8>
e-4e-8dcd2
o4>c4^8<b-8>c8e-8g8e-8
a-4^8ce-f4d8<b-8>
c1
[r1]8
c8d8e-2f8g8
a-4^8gfg8f8e-8d8
c8d8e-2f8g8
a-8g8f8e-8f8e-8d8e-8
cde-de-de-fgfe-fe-de-f
ga-b-a-gfe-fgfe-fe-dc<b->
c8de-f8e-de-8fga-8gf
e-8cde-cde-f8de-fde-f
g2^4g8g8
a-4^8e-fg8a-8b-8a-8
g4e-8f8g8a-8g8f8
g4f8e-8f8e-8d8e-8
c2^4de-fg
a-4^8a-b->c8<b-8a-8e-f
g8a-8g8f8g8f8e-8e-f
g8f8e-8f8e-8d8e-8<b-8>
c1

#1
@5 $ed $4e $71 v230

o3
(2)[g4e-8c8g8fe-f4
a-4e-8c8a-8gfg4
g4e-8c8g8fe-f4
ce-a-ce-a-c8
cfa-cfa-c8]2
r1
/
v240
(2)3
g4e-8c8g8fe-f4
a-4e-8c8a-8gfg4
g4e-8c8g8fe-f4
ce-a-ce-a-c8
dfb-dfb-d8
[e-g>c<e-g>c<e-8]2
[o4g8c8e-8c8g8fe-f8c8
a-8c8e-8c8a-8gfg8c8
g8c8e-8c8g8fe-f8c8
ce-a-ce-a-ce-
cfa-cfa-cf]3
g8c8e-8c8g8fe-f8c8
a-8c8e-8c8a-8gfg8c8
g8c8e-8c8g8fe-f8c8
ce-a-ce-a-ce-
dfb-dfb-df
[e-g>c<e-g>c<e-g]2
[o3a-4f8c8a-8gfg4
g4e-8c8g8fe-f4
g4d8<b8>g8fe-f4
ce-gce-gc8
ce-gce-gc8]2

#2 v230
@8 $ed $4b $71

[r1]9
/
v240
o2(3)[e-8g8>c2^4<
a-8>c8e-2c8<a-8
e-8g8>c2^4<
a-8>c8e-4<a-8>c8f4<]3
e-8g8>c2^4<
a-8>c8e-2c8<a-8
e-8g8>c2^4<
a-8>c8e-4<b-8>d8f4
c8e-8g2^4<
(3)3
e-8g8>c2^4<
a-8>c8e-2c8<a-8
e-8g8>c2^4<
a-8>c8e-4<b-8>d8f4
c8e-8g2^4<
[f8a-8>c2^4<
e-8g8>c2^4<
d8g8b2^4
e-8g8>c2^4<]2

#3 v180
@13 $ed $5b $c3
$de $10 $0c $2f
o4(4)[g1a-1g1a-2f2]
g1a-1g1a-2f1^2
/
v190
(4)3
g1a-1g1a-2b-2
>c1<
(4)3
g1a-1g1a-2b-2
>c1<
a-1g1b1>c1<
a-1g1<b1>c1

#5 v220
@1 $ed $38 $60
$de $10 $0a $3f
[r1]4
e-1c1e-1c2f2^1
/
v220
(5)[e-1c1e-1c2f2]4
g1
(5)4
g1
[f1e-1d1e-1]2

#4
[r1]8
v232[@21c]4
v234[@21c]4
v236[@21c]4
v238[@21c]4
/
v240
(6)[@21c4r8@21c8@29c4r8@29c@29c
@21c4r8@21c8@29c4r8@29c@29c
@21c4r8@21c8@29c4r8@29c@29c
@21c4@21c8@29c@29c@21c4@21c8@29c@29c]4
[@21c4@29c8@21c8]2
(6)4
[@21c4@29c8@21c8]2
(6)2

#6 v238
;hi-hat & crash cymbal
[r1]8
r2^4@29[c]4
/
v240
(7)[@23c8@22c8c8c8c8c8c8c8c8c8c8c8c8c8c8c8]8
[@23c8@22c8c8c8]2
(7)6
[@23c8@22c8c8c8]2
(7)6                

#amk=1
