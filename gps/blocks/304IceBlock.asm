db $42
JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP FIAR : JMP Return : JMP Return : JMP Return

FIAR:		STZ $170B,x		; \ Erase fireball
		JSR SUB_SMOKE		; / Show smoke effect

		LDA #$02		;\
		STA $9C			; | Generate blank block
		JSL $00BEB0		;/

Return:		RTL

SUB_SMOKE:	LDY #$03		; \ find a free slot to display effect
FINDFREE:	LDA $17C0,y		;  |
		BEQ FOUNDONE		;  |
		DEY			;  |
		BPL FINDFREE		;  |
		RTS			; / return if no slots open

FOUNDONE:	LDA #$01		; \ set effect to smoke
		STA $17C0,y		; /

		LDA $1715,x		; \ set y pos of smoke
		STA $17C4,y		; /

		LDA $171F,x		; \ set x pos of smoke
		STA $17C8,y		; /

		LDA #$18		; \ set smoke duration
		STA $17CC,y		; /
		RTS