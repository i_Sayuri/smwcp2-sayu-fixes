db $42
JMP Return : JMP MarioAbove : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP MarioAbove : JMP Return : JMP Return

MarioAbove:
	LDA $60
	ASL A
	TAX

	STZ $7B
	STZ $7D		; Mario is not allowed to move until block  finishes shattering.

	LDA $16
	AND #$7F
	STA $16
	LDA $18
	AND #$7F
	STA $18		; Disable jumping

	REP #$20
	LDA $98
	PHA
	LDA $9A
	PHA
	LDA $98
	CLC
	ADC YTable,x
	STA $98
	LDA $9A
	SEC
	SBC XTable,x
	STA $9A
	SEP #$20
	JSR Shatter
	REP #$20
	PLA
	STA $9A
	PLA
	STA $98
	SEP #$20

	INC $60
	LDA $60
	AND #$03
	STA $60

	LDA $60
	BNE Return

	LDA #$C0
	STA $7D		; Bounce up a bit

Return:
	RTL

XTable:
dw $0010,$0000,$0010,$0000
YTable:
dw $0010,$0010,$0000,$0000

Shatter:
	PHY		;preserve map16 high

	PHB		;need to change bank
	LDA #$02
	PHA
	PLB		;to 02
	LDA #$00	;default shatter
	JSL $028663	;shatter block
	PLB		;restore bank

	LDA #$02
	STA $9C
	JSL $00BEB0

	PLY
	RTS