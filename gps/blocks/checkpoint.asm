incsrc CoinDefines.asm

db $42

JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball : JMP TopCorner : JMP HeadInside : JMP BodyInside

!OWLevelNum = $13BF
!CheckpointRAM = $7FA040
!CheckpointNumber = $01

MarioAbove:
MarioBelow:
MarioSide:
TopCorner:
HeadInside:
BodyInside:

;---------
;;;; Code used by the smwc coins
;---------

LDA.l !TempCounter+1
ASL #3
ORA.l !TempCounter+1
AND #$3F
STA.l !TempCounter+1

;---------
;;;;
;---------

LDX !OWLevelNum
LDA #!CheckpointNumber
STA !CheckpointRAM,x

LDA #$05
STA $1DF9

lda $19
bne +
	inc $19
+

LDA #$02
STA $9C
PHY
JSL $80BEB0
PLY

SpriteV:
SpriteH:
MarioCape:
MarioFireball:
RTL