;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Quicksand (Pit)
; By Sonikku
; Description: 
; Almost exactly like Magus' quicksand block, except this is more customizable.
; This version makes it very hard for Mario to move left and right. 
; Customization:
; !Power is how hard it is to move while under the effect of the block. Change it 
; lower to make Mario's jump less effective, set it higher for an easier escape.
; !SpriteSink is how fast sprites go in the block. Set it lower for a slower fall, or set it higher
; so sprites fall faster. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
db $42
JMP Main : JMP Main : JMP Main : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

!Power = $00
!Sink = $09

Main:
	STZ $15		;Stop controller movements

	LDA $13EC
	ORA #$01
	STA $13EC

	LDA #$02	;
	STA $1471	;What type of platform Mario is on.

	LDA $1F3B	;
	AND #$01	; 
	BNE NoSound	;If sound already played, branch.

	LDA #$25	;Play sinking sound.
	STA $1DF9	;Store to bank.
NoSound:
	LDA $1F3B	;
	ORA #$01	;
	STA $1F3B	;Set bit so sound doesn't replay.

	LDA #$09	;Load horizontal speed.
	STA $7B		;store.
	RTL
Sprite:
	LDA $7FAB9E,x		
	CMP #$61		;\ If the sprite is the right one...
	BEQ Return		;/ branch

	LDA #!Sink
	STA $AA,x
	STA $BB,x
Return:
	RTL		;Return