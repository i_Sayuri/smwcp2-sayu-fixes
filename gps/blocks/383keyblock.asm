; This is similar to the Key Block given in SMWC.
; However this one does not destroy the key when it's used.
; cstutor89
db $42
JMP Return : JMP Return : JMP MarioSide : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

!SpriteState = $00	;sprite status after touching the block
!SpriteNum = $80	;Key sprite
!AcceptSFX = $10	;Sound to play when you touch block with key

MarioSide:	PHY
		PHA
		PHX
		LDX #$00
Loop1:		LDA $9E,x
		CMP #!SpriteNum
		BNE NoMatch
		LDA $14C8,x
		CMP #$0B
		BNE NoMatch
		BRA Unlock
NoMatch:	INX
		CPX #$0C
		BEQ Return1
		BRA Loop1

Unlock:		REP #$30
		LDA $03
		CLC
		ADC #$0001
		STA $03
		JSL SUBL_SET_MAP16
		SEP #$30
		JSR SUB_SMOKE
		LDA #!AcceptSFX
		STA $1DF9
		
Return1:	PLX
		PLA
		PLY
Return:		RTL


SUBL_SET_MAP16:		PHP
			REP #$30
			PHY
			PHX
			TAX
			LDA $03
			PHA
			JSR SUB_8034
			PLA
			STA $03
			PLX
			PLY
			PLP
			RTL

RETURN18:		PLX
			PLB
			PLP
			RTS

SUB_8034:		PHP
			SEP #$20
			PHB
			LDA #$00
			PHA
			PLB
			REP #$30
			PHX
			LDA $9A
			STA $0C
			LDA $98
			STA $0E
			LDA #$0000
			SEP #$20
			LDA $5B
			STA $09
			LDA $1933
			BEQ NO_SHIFT
			LSR $09
NO_SHIFT:		LDY $0E
			LDA $09
			AND #$01
			BEQ HORIZ
			LDA $9B
			STA $00
			LDA $99
			STA $9B
			LDA $00
			STA $99
			LDY $0C
HORIZ:			CPY #$0200
			BCS RETURN18
			LDA $1933
			ASL A
			TAX
			LDA $BEA8,x
			STA $65
			LDA $BEA9,x
			STA $66
			STZ $67
			LDA $1925
			ASL A
			TAY
			LDA ($65),y
			STA $04
			INY
			LDA ($65),y
			STA $05
			STZ $06
			LDA $9B
			STA $07
			ASL A
			CLC
			ADC $07
			TAY
			LDA ($04),y
			STA $6B
			STA $6E
			INY
			LDA ($04),y
			STA $6C
			STA $6F
			LDA #$7E
			STA $6D
			INC A
			STA $70
			LDA $09
			AND #$01
			BEQ NO_AND
			LDA $99
			LSR A
			LDA $9B
			AND #$01
			BRA LABEL52
NO_AND:			LDA $9B
			LSR A
			LDA $99
LABEL52:		ROL A
			ASL A
			ASL A
			ORA #$20
			STA $04
			CPX #$0000
			BEQ NO_ADD
			CLC
			ADC #$10
			STA $04
NO_ADD:			LDA $98
			AND #$F0
			CLC
			ASL A
			ROL A
			STA $05
			ROL A
			AND #$03
			ORA $04
			STA $06
			LDA $9A
			AND #$F0
			LSR A
			LSR A
			LSR A
			STA $04
			LDA $05
			AND #$C0
			ORA $04
			STA $07
			REP #$20
			LDA $09
			AND #$0001
			BNE LABEL51
			LDA $1A
			SEC
			SBC #$0080
			TAX
			LDY $1C
			LDA $1933
			BEQ LABEL50
			LDX $1E
			LDA $20
			SEC
			SBC #$0080
			TAY
			BRA LABEL50
LABEL51:		LDX $1A
			LDA $1C
			SEC
			SBC #$0080
			TAY
			LDA $1933
			BEQ LABEL50
			LDA $1E
			SEC
			SBC #$0080
			TAX
			LDY $20
LABEL50:		STX $08
			STY $0A
			LDA $98
			AND #$01F0
			STA $04
			LDA $9A
			LSR A
			LSR A
			LSR A
			LSR A
			AND #$000F
			ORA $04
			TAY
			PLA
			SEP #$20
			STA [$6B],y
			XBA
			STA [$6E],y
			XBA
			REP #$20
			ASL A
			TAY
			PHK
			PER MAP16_RETURN-$01
			PEA $804C
			JML $00C0FB
MAP16_RETURN:		PLB
			PLP
			RTS

SUB_SMOKE:          LDY #$03                ; \ find a free slot to display effect
FINDFREE:           LDA $17C0,y             ;  |
                    BEQ FOUNDONE            ;  |
                    DEY                     ;  |
                    BPL FINDFREE            ;  |
                    RTS                     ; / return if no slots open

FOUNDONE:           LDA #$01                ; \ set effect graphic to smoke graphic
                    STA $17C0,y             ; /
                    LDA #$1B                ; \ set time to show smoke
                    STA $17CC,y             ; /
                    LDA $D8,x               ; \ smoke y position = generator y position
                    STA $17C4,y             ; /
                    LDA $E4,x               ; \ load generator x position and store it for later
                    STA $17C8,y             ; /
                    RTS