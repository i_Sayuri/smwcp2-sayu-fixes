;Sprite kill block, courtesy of Kipernal.
;Will kill all sprites that are not 5b, 5C, 5D, and 18, which are the the three floating platforms, and the surface jumping fish/pirhangler.
;Acts like tile 0.
db $42
JMP Return : JMP Return : JMP Return : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

Sprite:
LDA $9E,x
CMP #$5B
BEQ Return
CMP #$5C
BEQ Return
CMP #$5D
BEQ Return
CMP #$18
BEQ Return
LDA #$05
STA $14C8,x

Return:
RTL