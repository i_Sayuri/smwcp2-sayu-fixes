incsrc CoinDefines.asm

db $42

JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireball : JMP TopCorner : JMP HeadInside : JMP BodyInside

!OWLevelNum = $13BF
!CheckpointNumber = $00

MarioAbove:
MarioBelow:
MarioSide:
TopCorner:
HeadInside:
BodyInside:

;---------
;;;; Code used by the smwc coins
;---------

LDA.l !TempCounter+1
ASL #3
ORA.l !TempCounter+1
AND #$3F
STA.l !TempCounter+1
LDA !OWLevelNum
STA.l !TempCounter
;---------
;;;;
;---------

LDX !OWLevelNum
LDA #!CheckpointNumber
STA !CheckpointRAM,x

LDX $13BF
LDA $1EA2,x
ORA #$40
STA $1EA2,x

LDA #$05
STA $1DF9

lda $19
bne +
	inc $19
+

LDA #$02
STA $9C
PHY
JSL $80BEB0
PLY

REP #$20
LDA $9A
SEC
SBC #$0010
STA $9A

REP #$10
LDX #$0030
%change_map16()
SEP #$30

SpriteV:
SpriteH:
MarioCape:
MarioFireball:
RTL