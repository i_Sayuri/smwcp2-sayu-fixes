;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Yellow Gem: This switch will activate Bit 2 of the Custom Trigger
;found at $7FC0FC. Acts like 130.
;Uses Edit's/Roy's Bounce Block Unrestrictor Patch.
;                                                     -Milk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP Switch : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Defines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!BouncePalette = $0E	; 0-7/8-F
	!BounceTile = $0080	; 0000-01FF (top-left 8x8)
	!ShatterType = $00	; change this to $01 for throw-block-like shatter effect

	!RAM_M16Tbl = $7EC275	; same as the other patch, to avoid using more RAM
	!RAM_BounceTileTbl = $7EC271

	!BPal = !BouncePalette&$07
	!BTile = !BounceTile&$1FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Switch:
		LDA $7FC0FC
		EOR #$04
		STA $7FC0FC
		LDA #$0B
		STA $1DF9

Bounce:		LDX.b #$03
Loop:		LDA.w $1699,x
		BEQ FoundSlot
		DEX
		BPL Loop
		DEC.w $18CD
		LDA.b #%11111100
		TRB.w $18CD
		LDX $18CD
		LDA $1699,x
		CMP #$07
		BNE FoundSlot
		JSR ResetTurnblock
FoundSlot:	LDA.b #%00001111
		TRB.b $98
		TRB.b $9A
		LDA.b #$01		; acts-like setting (not sure if this matters)
		STA.w $1699,x
		LDA.b #$00
		STA.w $169D,x
		LDA.b $98
		STA.w $16A1,x
		LDA.b $99
		STA.w $16A9,x
		LDA.b $9A
		STA.w $16A5,x
		LDA.b $9B
		STA.w $16AD,x
		LDA.b #$C0		; Y speed (C0=up, 40=down, 00=left/right)
		STA.w $16B1,x
		LDA.b #$00		; X speed (C0=left, 40=right, 00=up/down)
		STA.w $16B5,x
		LDA.b #$FF		; use FD, FE, or FF **
		STA.w $16C1,x
		LDA.b #$08
		STA.w $16C5,x
		LDA.w $1933
		LSR
		ROR
		;ORA #$00		; 00 = up, 01 = right, 02 = left, 03 = down (uncomment if needed)
		STA.w $16C9,x		; \
		LDA.b #!BounceTile	;  | remove if FE**
		STA !RAM_BounceTileTbl,x;  | (and change how $1901
		LDA.b #!BPal<<1
                ORA.b #!BTile>>8; | is written to)
		STA.w $1901,x		; /
		;PHX			; \
		TXA			;  | remove if FD**
		ASL			;  |
		TAX			;  |
		REP #%00100000		;  |
		LDA.b $03		;  |
		STA.l !RAM_M16Tbl,x	;  |
		SEP #%00100000		;  |
		;PLX			; /
Return:
		RTL

ResetTurnblock:	LDA $9A
		PHA
		LDA $9B
		PHA
		LDA $98
		PHA
		LDA $99	
		PHA
		LDA $16A5,Y
		STA $9A
		LDA $16AD,Y
		STA $9B
		LDA $16A1,Y
		CLC
		ADC #$0C
		AND #$F0
		STA $98
		LDA $16A9,Y
		ADC #$00
		STA $99
		LDA $16C1,Y
		STA $9C
		LDA $04
		PHA
		LDA $05
		PHA
		LDA $06
		PHA
		LDA $07
		PHA
		JSL $00BEB0
		PLA 
		STA $07
		PLA 
		STA $06
		PLA
		STA $05
		PLA 
		STA $04
		PLA 
		STA $99
		PLA 
		STA $98
		PLA 
		STA $9B
		PLA 
		STA $9A
		RTS