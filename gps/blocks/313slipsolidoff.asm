db $42

JMP Main : JMP MainAbove : JMP Main : JMP Main : JMP Main : JMP Main : JMP Main
JMP NoSlip : JMP Main : JMP Main


Main:
	LDA $14AF
	BEQ Blank	; Change to BEQ for it to be reversed
	LDY #$01	;\
	LDA #$30	;| Act Like Tile 130
	STA $1693	;/
Blank:
	RTL		; Return


MainAbove:
	JSL Main
	LDA $14AF
	BEQ Blank	; Change to BEQ for it to be reversed
	LDA $16		;\
	ORA $18		;| If pressing A/B, Set Slippery flag to none
	BMI NoSlip	;/
	LDA #$80	;\ Set Slippery Flag to #$80
	STA $86		;/
	RTL
NoSlip:
	STZ $86
	RTL