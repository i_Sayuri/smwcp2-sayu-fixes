db $42
JMP below : JMP return : JMP return
JMP return : JMP sprite_handle : JMP return : JMP return
JMP return : JMP return : JMP return

sprite_handle:
	LDA $9E,X
	CMP #$53
	BNE return
	LDA $14C8,x
	CMP #$0B
	BEQ return
	PHX
	JSR BRING_ON_THE_FIRE
	PLX
	LDA $E4,x
	STA $00E4,y
	LDA $14E0,x
	STA $14E0,y
	LDA $D8,x
	STA $00D8,y
	LDA $14D4,x
	STA $14D4,y
return:
	RTL

below:
	LDA #$20
	STA $7D
	PHX
	JSR BRING_ON_THE_FIRE
	LDA $94
	SEC
	SBC #$10
	STA $E4,x
	LDA $95
	STA $14E0,x
	LDA $96
	STA $D8,x
	LDA $97
	STA $14D4,x
	PLX
	RTL

BRING_ON_THE_FIRE:
	LDA #$06
	STA $0E
	JSL $02A9E6
	BMI NO_FIRE
	LDA #$17
	STA $1DFC
	TYX
	LDA #$01
	STA $14C8,x
	LDA #$0A
	STA $7FAB9E,x
	JSL $07F7D2
	JSL $0187A7
	LDA #$08
	STA $7FAB10,x
	INC $1528,x	
	RTS
NO_FIRE:
	PLA
	PLA
	PLX
	RTL